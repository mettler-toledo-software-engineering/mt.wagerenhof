﻿// <auto-generated/>

using System;
using System.Collections.Generic;
using MT.Singularity.Collections;
using MT.Singularity.Localization;

namespace IND930YMLUI 
{
	[System.CodeDom.Compiler.GeneratedCodeAttribute("MT.Singularity.Environment.Yml.Localization.LanguageXmlTemplate", "2.1.0.0")]
	[TranslationModule("en-US", "de-DE", "fr-Fr", Strings = 75)]
	public class Localization
	{	
		internal static readonly TranslationModule Module = new ModuleClass();

		public enum Key
		{
			/// <summary>
			/// Abbrechen
			/// </summary>
			Abbrechen,
			/// <summary>
			/// Admin Name:
			/// </summary>
			AdminName,
			/// <summary>
			/// Admin Passwort:
			/// </summary>
			AdminPassword,
			/// <summary>
			/// Artikel:
			/// </summary>
			Article,
			/// <summary>
			/// Barocde Reader Com Port:
			/// </summary>
			BarocdeReaderComPort,
			/// <summary>
			/// Brutto
			/// </summary>
			Brutto,
			/// <summary>
			/// Betriebswäsche Tagesübersicht
			/// </summary>
			CompanyLaundrySummary,
			/// <summary>
			/// Container Bezeichnung:
			/// </summary>
			ContainerName,
			/// <summary>
			/// CSV-Name Artikel:
			/// </summary>
			CSVArticle,
			/// <summary>
			/// CSV-Name Debitor:
			/// </summary>
			CSVDebitor,
			/// <summary>
			/// CSV-Name Ergebnisse:
			/// </summary>
			CSVMeasurements,
			/// <summary>
			/// Anwendung
			/// </summary>
			CustomApplication,
			/// <summary>
			/// Kunde:
			/// </summary>
			Customer,
			/// <summary>
			/// Datenquelle:
			/// </summary>
			DataSource,
			/// <summary>
			/// Datenbank Server:
			/// </summary>
			DBServer,
			/// <summary>
			/// Datenbank Username:
			/// </summary>
			DBUser,
			/// <summary>
			/// Drucken...
			/// </summary>
			Drucken,
			/// <summary>
			/// Drucker Config
			/// </summary>
			DruckerConfig,
			/// <summary>
			/// Einstellungen
			/// </summary>
			Einstellungen,
			/// <summary>
			/// Error
			/// </summary>
			Error,
			/// <summary>
			/// Extern?
			/// </summary>
			Extern,
			/// <summary>
			/// Favorit Kunde
			/// </summary>
			FavoriteCustomerNumber,
			/// <summary>
			/// Grundzustand
			/// </summary>
			Grundzustand,
			/// <summary>
			/// Intern?
			/// </summary>
			Intern,
			/// <summary>
			/// Intern / Extern:
			/// </summary>
			InternExtern,
			/// <summary>
			/// Ja
			/// </summary>
			Ja,
			/// <summary>
			/// Kein USB-Stick erkannt.
			/// </summary>
			KeinUSBStickErkannt,
			/// <summary>
			/// Löschen
			/// </summary>
			Loeschen,
			/// <summary>
			/// Wagerenhof Setup
			/// </summary>
			MainSetupNode,
			/// <summary>
			/// Artikel Verwalten
			/// </summary>
			ManageArticles,
			/// <summary>
			/// Container Verwalten
			/// </summary>
			ManageContainer,
			/// <summary>
			/// Kunden Verwalten
			/// </summary>
			ManageDebitor,
			/// <summary>
			/// Name
			/// </summary>
			Name,
			/// <summary>
			/// Nein
			/// </summary>
			Nein,
			/// <summary>
			/// Netto
			/// </summary>
			Netto,
			/// <summary>
			/// NETTO
			/// </summary>
			NETTOCAPS,
			/// <summary>
			/// Netto Gewicht:
			/// </summary>
			NettoWeight,
			/// <summary>
			/// Nullieren
			/// </summary>
			Nullieren,
			/// <summary>
			/// Ok
			/// </summary>
			Ok,
			/// <summary>
			/// Werk:
			/// </summary>
			Plant,
			/// <summary>
			/// Vortara
			/// </summary>
			PreTare,
			/// <summary>
			/// Druckername
			/// </summary>
			PrinterName,
			/// <summary>
			/// Stückzahl:
			/// </summary>
			Quantity,
			/// <summary>
			/// PC / Domain:
			/// </summary>
			RemoteHost,
			/// <summary>
			/// Reset Intervall in h:
			/// </summary>
			ResetIntervall,
			/// <summary>
			/// SAP API Password
			/// </summary>
			SapApiPassword,
			/// <summary>
			/// SAP API URL
			/// </summary>
			SapApiUrl,
			/// <summary>
			/// SAP API UserName
			/// </summary>
			SapApiUser,
			/// <summary>
			/// SAP Status Code
			/// </summary>
			SAPResponseCode,
			/// <summary>
			/// SAP Status Message
			/// </summary>
			SAPResponseMessage,
			/// <summary>
			/// Suchen / Filtern:
			/// </summary>
			SearchFilter,
			/// <summary>
			/// Artikelauswahl
			/// </summary>
			SelectArticle,
			/// <summary>
			/// Containerauswahl
			/// </summary>
			SelectContainer,
			/// <summary>
			/// Kundenauswahl
			/// </summary>
			SelectDebitor,
			/// <summary>
			/// Team ID:
			/// </summary>
			SelectedTeamID,
			/// <summary>
			/// Einstellungen
			/// </summary>
			Settings,
			/// <summary>
			/// Setup
			/// </summary>
			Setup,
			/// <summary>
			/// Terminalbezeichnung
			/// </summary>
			StationName,
			/// <summary>
			/// Zusammenfassung
			/// </summary>
			Summary,
			/// <summary>
			/// Tara
			/// </summary>
			Tara,
			/// <summary>
			/// Tara löschen
			/// </summary>
			TaraLoeschen,
			/// <summary>
			/// Tarieren
			/// </summary>
			Tarieren,
			/// <summary>
			/// Terminal config
			/// </summary>
			TerminalSubNode,
			/// <summary>
			/// Test
			/// </summary>
			Test,
			/// <summary>
			/// Test Result
			/// </summary>
			TestResult,
			/// <summary>
			/// System Update
			/// </summary>
			Update,
			/// <summary>
			/// Update Status
			/// </summary>
			UpdateStatus,
			/// <summary>
			/// Waage
			/// </summary>
			Waage,
			/// <summary>
			/// Waage auswählen
			/// </summary>
			WaageAuswaehlen,
			/// <summary>
			/// Lagerhaus:
			/// </summary>
			Warehouse,
			/// <summary>
			/// Container mit Wäsche wiegen
			/// </summary>
			WeighLaundry,
			/// <summary>
			/// Gewicht:
			/// </summary>
			Weight,
			/// <summary>
			/// Arbeitsumgebung:
			/// </summary>
			WorkingEnvironment,
			/// <summary>
			/// Application
			/// </summary>
			WorkPlaceType,
			/// <summary>
			/// Ausgabeverzeichnis:
			/// </summary>
			WriteDirectory,
		}

		public static string Get(Key key)
		{
			return LocalizationManager.GetTranslation(Module, (int)key);
		}

		public static string Format(Key key, string arg0)
		{
			return string.Format(LocalizationManager.GetTranslation(Module, (int)key), arg0);
		}

		public static string Format(Key key, string arg0, string arg1)
		{
			return string.Format(LocalizationManager.GetTranslation(Module, (int)key), arg0, arg1);
		}

		public static string Format(Key key, params string[] args)
		{
			return string.Format(LocalizationManager.GetTranslation(Module, (int)key), args);
		}

		public static TranslationModule GetTranslationModule()
		{
			return Module;
		}

		private class ModuleClass : TranslationModule
		{
			private readonly IImmutableIndexable<string> languages = Indexable.ImmutableValues("en-US", "de-DE", "fr-Fr");

			public override string PrimaryLanguage
			{
				get { return "de-DE"; }
			}

			public override IEnumerable<string> SupportedLanguages
			{
				get { return languages; }
			}

			public override string Name 
			{
				get { return "IND930YMLUI.Localization"; }
			}

			public override ITranslationProvider GetTranslation(string language)
			{
				if (language == "en-US")
					return new TranslationEN_US();
				if (language == "de-DE")
					return new TranslationDE_DE();
				if (language == "fr-Fr")
					return new TranslationFR_FR();
				return null;
			}

			public override Dictionary<string, int> GetKeyNames()
			{
                Dictionary<string, int> keyNames = new Dictionary<string, int>();
				for (int i = 0; i < 75; i++)
				{
					keyNames.Add(((Key)i).ToString(), i);
				}
                return keyNames;
			}
		}

		private class TranslationEN_US : ITranslationProvider
		{
			public string Get(int key) 
			{
				switch((Key)key)
				{
					case Key.Abbrechen:
						return "Cancel";
					case Key.AdminName:
						return "Admin Name:";
					case Key.AdminPassword:
						return "Admin Password:";
					case Key.Article:
						return "Artikel:";
					case Key.BarocdeReaderComPort:
						return "Barocde Reader Com Port:";
					case Key.Brutto:
						return "Gross";
					case Key.CompanyLaundrySummary:
						return "Betriebswäsche Tagesübersicht";
					case Key.ContainerName:
						return "Container Bezeichnung:";
					case Key.CustomApplication:
						return "Application";
					case Key.Customer:
						return "Kunde:";
					case Key.DataSource:
						return "Data Source:";
					case Key.DBServer:
						return "Database Server:";
					case Key.DBUser:
						return "Database Username:";
					case Key.CSVDebitor:
						return "CSV-Name Debitor:";
					case Key.CSVArticle:
						return "CSV-Name Article:";
					case Key.CSVMeasurements:
						return "CSV-Name Ergebnisse:";
					case Key.Drucken:
						return "Printing...";
					case Key.DruckerConfig:
						return "Printer Config";
					case Key.Einstellungen:
						return "Settings";
					case Key.Error:
						return "Error";
					case Key.Extern:
						return "Extern?";
					case Key.FavoriteCustomerNumber:
						return "Favorite Customer";
					case Key.Grundzustand:
						return "Basic state";
					case Key.Intern:
						return "Intern?";
					case Key.InternExtern:
						return "Intern / Extern:";
					case Key.Ja:
						return "Yes";
					case Key.KeinUSBStickErkannt:
						return "No USB drive found";
					case Key.Loeschen:
						return "Delete";
					case Key.MainSetupNode:
						return "Wagerenhof Setup";
					case Key.ManageArticles:
						return "Artikel Verwalten";
					case Key.ManageContainer:
						return "Container Verwalten";
					case Key.ManageDebitor:
						return "Kunden Verwalten";
					case Key.Name:
						return "Name";
					case Key.Nein:
						return "No";
					case Key.Netto:
						return "Net";
					case Key.NettoWeight:
						return "Netto Gewicht:";
					case Key.NETTOCAPS:
						return "NET";
					case Key.Nullieren:
						return "Zero";
					case Key.Ok:
						return "Ok";
					case Key.Plant:
						return "Plant:";
					case Key.PreTare:
						return "Pretare";
					case Key.PrinterName:
						return "Printer Name:";
					case Key.Quantity:
						return "Stückzahl:";
					case Key.RemoteHost:
						return "PC / Domain:";
					case Key.ResetIntervall:
						return "Reset Intervall in h:";
					case Key.SapApiUrl:
						return "SAP API URL";
					case Key.SapApiUser:
						return "SAP API UserName";
					case Key.SapApiPassword:
						return "SAP API Password";
					case Key.SAPResponseCode:
						return "SAP Status Code";
					case Key.SAPResponseMessage:
						return "SAP Status Message";
					case Key.SelectedTeamID:
						return "Team ID:";
					case Key.SelectContainer:
						return "Containerauswahl";
					case Key.SelectDebitor:
						return "Kundenauswahl";
					case Key.SelectArticle:
						return "Artikelauswahl";
					case Key.Settings:
						return "Settings";
					case Key.Setup:
						return "Setup";
					case Key.StationName:
						return "Station name";
					case Key.SearchFilter:
						return "Suchen / Filtern:";
					case Key.Summary:
						return "Zusammenfassung";
					case Key.Tara:
						return "(EN)Tara";
					case Key.TaraLoeschen:
						return "(EN)Tara löschen";
					case Key.Tarieren:
						return "Tare";
					case Key.TerminalSubNode:
						return "Station config";
					case Key.Test:
						return "Test";
					case Key.TestResult:
						return "Test Result";
					case Key.UpdateStatus:
						return "Update Status";
					case Key.Update:
						return "System Update";
					case Key.Waage:
						return "Scale";
					case Key.Warehouse:
						return "Warehouse:";
					case Key.WaageAuswaehlen:
						return "(EN)Waage auswählen";
					case Key.WeighLaundry:
						return "Container mit Wäsche wiegen";
					case Key.Weight:
						return "Gewicht:";
					case Key.WorkingEnvironment:
						return "Arbeitsumgebung:";
					case Key.WorkPlaceType:
						return "Anwendung";
					case Key.WriteDirectory:
						return "Ausgabeverzeichnis:";
					default:
						return string.Empty;
				}
			}

			public string Language
			{
				get { return "en-US"; }
			}
		}
		private class TranslationDE_DE : ITranslationProvider
		{
			public string Get(int key) 
			{
				switch((Key)key)
				{
					case Key.Abbrechen:
						return "Abbrechen";
					case Key.AdminName:
						return "Admin Name:";
					case Key.AdminPassword:
						return "Admin Passwort:";
					case Key.Article:
						return "Artikel:";
					case Key.BarocdeReaderComPort:
						return "Barocde Reader Com Port:";
					case Key.Brutto:
						return "Brutto";
					case Key.CompanyLaundrySummary:
						return "Betriebswäsche Tagesübersicht";
					case Key.ContainerName:
						return "Container Bezeichnung:";
					case Key.CustomApplication:
						return "Anwendung";
					case Key.Customer:
						return "Kunde:";
					case Key.DataSource:
						return "Datenquelle:";
					case Key.DBServer:
						return "Datenbank Server:";
					case Key.DBUser:
						return "Datenbank Username:";
					case Key.CSVDebitor:
						return "CSV-Name Debitor:";
					case Key.CSVArticle:
						return "CSV-Name Artikel:";
					case Key.CSVMeasurements:
						return "CSV-Name Ergebnisse:";
					case Key.Drucken:
						return "Drucken...";
					case Key.DruckerConfig:
						return "Drucker Config";
					case Key.Einstellungen:
						return "Einstellungen";
					case Key.Error:
						return "Error";
					case Key.Extern:
						return "Extern?";
					case Key.FavoriteCustomerNumber:
						return "Favorit Kunde";
					case Key.Grundzustand:
						return "Grundzustand";
					case Key.Intern:
						return "Intern?";
					case Key.InternExtern:
						return "Intern / Extern:";
					case Key.Ja:
						return "Ja";
					case Key.KeinUSBStickErkannt:
						return "Kein USB-Stick erkannt.";
					case Key.Loeschen:
						return "Löschen";
					case Key.MainSetupNode:
						return "Wagerenhof Setup";
					case Key.ManageArticles:
						return "Artikel Verwalten";
					case Key.ManageContainer:
						return "Container Verwalten";
					case Key.ManageDebitor:
						return "Kunden Verwalten";
					case Key.Name:
						return "Name";
					case Key.Nein:
						return "Nein";
					case Key.Netto:
						return "Netto";
					case Key.NettoWeight:
						return "Netto Gewicht:";
					case Key.NETTOCAPS:
						return "NETTO";
					case Key.Nullieren:
						return "Nullieren";
					case Key.Ok:
						return "Ok";
					case Key.Plant:
						return "Werk:";
					case Key.PreTare:
						return "Vortara";
					case Key.PrinterName:
						return "Druckername";
					case Key.Quantity:
						return "Stückzahl:";
					case Key.RemoteHost:
						return "PC / Domain:";
					case Key.ResetIntervall:
						return "Reset Intervall in h:";
					case Key.SapApiUrl:
						return "SAP API URL";
					case Key.SapApiUser:
						return "SAP API UserName";
					case Key.SapApiPassword:
						return "SAP API Password";
					case Key.SAPResponseCode:
						return "SAP Status Code";
					case Key.SAPResponseMessage:
						return "SAP Status Message";
					case Key.SelectedTeamID:
						return "Team ID:";
					case Key.SelectContainer:
						return "Containerauswahl";
					case Key.SelectDebitor:
						return "Kundenauswahl";
					case Key.SelectArticle:
						return "Artikelauswahl";
					case Key.Settings:
						return "Einstellungen";
					case Key.Setup:
						return "Setup";
					case Key.StationName:
						return "Terminalbezeichnung";
					case Key.SearchFilter:
						return "Suchen / Filtern:";
					case Key.Summary:
						return "Zusammenfassung";
					case Key.Tara:
						return "Tara";
					case Key.TaraLoeschen:
						return "Tara löschen";
					case Key.Tarieren:
						return "Tarieren";
					case Key.TerminalSubNode:
						return "Terminal config";
					case Key.Test:
						return "Test";
					case Key.TestResult:
						return "Test Result";
					case Key.UpdateStatus:
						return "Update Status";
					case Key.Update:
						return "System Update";
					case Key.Waage:
						return "Waage";
					case Key.Warehouse:
						return "Lagerhaus:";
					case Key.WaageAuswaehlen:
						return "Waage auswählen";
					case Key.WeighLaundry:
						return "Container mit Wäsche wiegen";
					case Key.Weight:
						return "Gewicht:";
					case Key.WorkingEnvironment:
						return "Arbeitsumgebung:";
					case Key.WorkPlaceType:
						return "Application";
					case Key.WriteDirectory:
						return "Ausgabeverzeichnis:";
					default:
						return string.Empty;
				}
			}

			public string Language
			{
				get { return "de-DE"; }
			}
		}
		private class TranslationFR_FR : ITranslationProvider
		{
			public string Get(int key) 
			{
				switch((Key)key)
				{
					case Key.Abbrechen:
						return "Quitter";
					case Key.AdminName:
						return "";
					case Key.AdminPassword:
						return "";
					case Key.BarocdeReaderComPort:
						return "Barocde Reader Com Port:";
					case Key.Brutto:
						return "Brut";
					case Key.DataSource:
						return "";
					case Key.DBServer:
						return "";
					case Key.DBUser:
						return "";
					case Key.CSVDebitor:
						return "";
					case Key.CSVArticle:
						return "";
					case Key.CSVMeasurements:
						return "";
					case Key.Drucken:
						return "Imprimer…";
					case Key.DruckerConfig:
						return "Imprimante Config";
					case Key.Einstellungen:
						return "Réglages";
					case Key.Error:
						return "Erreur";
					case Key.Extern:
						return "Extern?";
					case Key.FavoriteCustomerNumber:
						return "Favorit Kunde";
					case Key.Grundzustand:
						return "Grundzustand (de)";
					case Key.Intern:
						return "Intern?";
					case Key.InternExtern:
						return "Intern / Extern:";
					case Key.Ja:
						return "Oui";
					case Key.KeinUSBStickErkannt:
						return "Pas de clé USB disponible";
					case Key.Loeschen:
						return "Effacer";
					case Key.MainSetupNode:
						return "Wagerenhof Setup";
					case Key.ManageArticles:
						return "Artikel Verwalten";
					case Key.ManageContainer:
						return "Container Verwalten";
					case Key.ManageDebitor:
						return "Kunden Verwalten";
					case Key.Name:
						return "Nom";
					case Key.Nein:
						return "Non";
					case Key.Netto:
						return "NET";
					case Key.NettoWeight:
						return "Netto Gewicht:";
					case Key.NETTOCAPS:
						return "NET";
					case Key.Nullieren:
						return "Remise à zéro";
					case Key.Ok:
						return "Ok";
					case Key.Plant:
						return "";
					case Key.PreTare:
						return "Vortara (de)";
					case Key.PrinterName:
						return "";
					case Key.Quantity:
						return "";
					case Key.RemoteHost:
						return "";
					case Key.ResetIntervall:
						return "Reset Intervall in h:";
					case Key.SapApiUrl:
						return "SAP API URL";
					case Key.SapApiUser:
						return "SAP API UserName";
					case Key.SapApiPassword:
						return "SAP API Password";
					case Key.SAPResponseCode:
						return "SAP Status Code";
					case Key.SAPResponseMessage:
						return "SAP Status Message";
					case Key.SelectedTeamID:
						return "Team ID:";
					case Key.SelectContainer:
						return "Containerauswahl";
					case Key.SelectDebitor:
						return "Kundenauswahl";
					case Key.SelectArticle:
						return "Artikelauswahl";
					case Key.Settings:
						return "Settings";
					case Key.Setup:
						return "Setup";
					case Key.StationName:
						return "Terminalbezeichnung (de)";
					case Key.SearchFilter:
						return "Suchen / Filtern:";
					case Key.Tara:
						return "Tare";
					case Key.TaraLoeschen:
						return "Effacer la tare";
					case Key.Tarieren:
						return "Tarer";
					case Key.TerminalSubNode:
						return "Terminal config";
					case Key.Test:
						return "Test";
					case Key.TestResult:
						return "Test Result";
					case Key.UpdateStatus:
						return "Update Status";
					case Key.Update:
						return "System Update";
					case Key.Waage:
						return "Bal";
					case Key.Warehouse:
						return "";
					case Key.WaageAuswaehlen:
						return "Commuter balance";
					case Key.WeighLaundry:
						return "Container mit Wäsche wiegen";
					case Key.Weight:
						return "Gewicht:";
					case Key.WriteDirectory:
						return "";
					default:
						return string.Empty;
				}
			}

			public string Language
			{
				get { return "fr-Fr"; }
			}
		}
	}
}