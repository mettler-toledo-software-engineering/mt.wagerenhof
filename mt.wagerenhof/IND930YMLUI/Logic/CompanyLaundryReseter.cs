﻿using IND930UI.Library.Models;
using IND930YMLUI.Config;
using System;
using MT.Singularity.Composition;
using MT.Singularity.Platform.Infrastructure;
using System.Timers;
using System.ComponentModel;

namespace IND930YMLUI.Logic
{
    public class CompanyLaundryReseter
    {
        private DateTime _lastCapturedTime;
        private Timer _resetProcessTimer;
        private CompanyLaundryProcessModel _companyLaundryProcess;
        private int _intervallInHours;
        private IComponents _components = ApplicationBootstrapperBase.CompositionContainer.Resolve<IComponents>();
        private Configuration _configuration;
        public CompanyLaundryReseter()
        {
            _components.GetConfigurationAsync().ContinueWith( task => {
                _configuration = task.Result;
                _intervallInHours = _configuration.ResetIntervall;

                _configuration.PropertyChanged += OnConfigurationChanged;
            });
        }

        private void OnConfigurationChanged(object sender, PropertyChangedEventArgs e)
        {
            if(e.PropertyName == "ResetIntervall")
            {
                _lastCapturedTime = DateTime.Now;
                _intervallInHours = _configuration.ResetIntervall;
                _resetProcessTimer.Stop();
                _resetProcessTimer.Start();
                //todo timer neu starten wenn config ändert
            }
        }

        public void Start(CompanyLaundryProcessModel companyLaundryProcess)

        {
           
            _companyLaundryProcess = companyLaundryProcess;
            _resetProcessTimer = new Timer();
            _resetProcessTimer.AutoReset = true;
            // 30 minutes interval = 1800000
            _resetProcessTimer.Interval = 18000;
            _resetProcessTimer.Enabled = true;
            // 
            _resetProcessTimer.Elapsed += OnTimerElapsed;
            _resetProcessTimer.Start();
        }
        public event EventHandler CompanyLaundryResetedEvent;

        private void OnTimerElapsed(object sender, ElapsedEventArgs e)
        {
            if (_lastCapturedTime != null)
            {
                // 13.45 Uhr + 1h <  13.48
                if (_lastCapturedTime.AddHours(_intervallInHours) < DateTime.Now)
                {
                    _companyLaundryProcess.ResetCompanyLaundryProcess();
                    CompanyLaundryResetedEvent?.Invoke(this, EventArgs.Empty);
                    _lastCapturedTime = DateTime.Now;
                }
            }
            else
            {
                //lauf nur 1. -> _lastCapturedTime = null, <-- DateTime.Now = 01.01.2021 10:35:00
                _lastCapturedTime = DateTime.Now;
            }

        }

    }
}
