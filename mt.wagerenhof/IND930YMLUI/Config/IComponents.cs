﻿using MT.Singularity.Platform.Configuration;

namespace IND930YMLUI.Config
{
    interface IComponents : IConfigurable<Configuration>
    {
    }
}