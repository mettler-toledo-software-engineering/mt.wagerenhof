﻿using System.ComponentModel;
using IND930UI.Library.Enum;
using MT.Singularity.Collections;
using MT.Singularity.Components;
using MT.Singularity.Platform.Configuration;

namespace IND930YMLUI.Config
{
    [Component]
    public class Configuration : ComponentConfiguration
    {


        public Configuration()
        {
            WorkingEnviroments = workingEnvironments;
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue(24)]
        public virtual int ResetIntervall
        {
            get { return (int)GetLocal(); }
            set { SetLocal(value); }
        }
        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue("20657")]
        public virtual string FavoriteCustomerNumber
        {
            //Todo noch anpassen
            get { return (string)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue(@"C:\Packages\mt.wagerenhof\Data")]
        public virtual string DataSource
        {
            get { return (string)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue("waageadmin")]
        public virtual string AdminName
        {
            get { return (string)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue("Gulila08beki!")]
        public virtual string AdminPassword
        {
            get { return (string)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue("debitoren.csv")]
        public virtual string CsvDebitor
        {
            get { return (string)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue("artikel.csv")]
        public virtual string CsvArticle
        {
            get { return (string)GetLocal(); }
            set { SetLocal(value); }
        }


        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue("container.csv")]
        public virtual string WriteDirectory
        {
            get { return (string)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue("measurements.csv")]
        public virtual string CsvMeasurements
        {
            get { return (string)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue("PCDEVSCHOLZW10")]
        public virtual string RemoteHost
        {
            get { return (string)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
       // [DefaultValue("COM5")]
        public virtual string BarcodeReaderComPort
        {
            get { return (string)GetLocal(); }
            set { SetLocal(value); }
        }

       [Setting(WritePermissions = Permissions.UntilSupervisorId)]
       [Component]
       [DefaultValue("")]
       public virtual string UsbUpdateResult
       {
           get { return (string)GetLocal(); }
           set { SetLocal(value); }
       }


       [Setting(WritePermissions = Permissions.UntilSupervisorId)]
       [Component]
       [DefaultValue("")]
       public virtual string SelectedWorkingEnvironment
       {
           get { return (string) GetLocal(); }
           set { SetLocal(value); }
        }

        private IImmutableIndexable<string> _workingEnviroments;

        public IImmutableIndexable<string> WorkingEnviroments
        {
            get { return _workingEnviroments; }
            set
            {
                if (Equals(_workingEnviroments, value) == false)
                {
                    _workingEnviroments = value;
                    NotifyPropertyChanged();
                }
            }
        }


        private static readonly IImmutableIndexable<string> workingEnvironments =
           Indexable.ImmutableValues(
               WorkingEnvironment.CleanLaundry.ToFriendlyName(),
               WorkingEnvironment.DirtyLaundry.ToFriendlyName()
           );
    }
}
