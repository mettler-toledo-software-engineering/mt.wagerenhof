﻿using System;
using System.Threading.Tasks;
using MT.Singularity.Collections;
using MT.Singularity.Logging;
using MT.Singularity.Platform.CommonUX.Setup.Impl;
using MT.Singularity.Platform.CommonUX.Setup.ViewModels;
using MT.Singularity.Presentation;

namespace IND930YMLUI.Config
{
    class SettingsSubNode : GroupSetupMenuItem
    {
        private readonly Configuration _configuration;
        private readonly IComponents _customerComponent;

        /// <summary>
        /// Initializes a new instance of the <see cref="MySubNodeOne"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="customerComponent">The customer component.</param>
        /// <param name="configuration">The configuration.</param>
        public SettingsSubNode(SetupMenuContext context, IComponents customerComponent, Configuration configuration)
            : base(context, new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.Setup), configuration, customerComponent)
        {
            _customerComponent = customerComponent;
            _configuration = configuration;
        }

        /// <summary>
        /// Show the children of this group. This will create the children.
        /// </summary>
        /// <returns></returns>
        public override Task ShowChildrenAsync()
        {
            try
            {
                // Titles for the my Setup parameters


                var dataSourceTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.DataSource);
                var dataSourceTarget = new TextSetupMenuItem(_context, dataSourceTitle, _configuration, "DataSource");

                var adminNameTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.AdminName);
                var adminNameTarget = new TextSetupMenuItem(_context, adminNameTitle, _configuration, "AdminName");

                var adminPasswordTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.AdminPassword);
                var adminPasswordTarget = new TextSetupMenuItem(_context, adminPasswordTitle, _configuration, "AdminPassword");
                adminPasswordTarget.PasswordChar = char.Parse("*");

                var remoteHostTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.RemoteHost);
                var remoteHostTarget = new TextSetupMenuItem(_context, remoteHostTitle, _configuration, "RemoteHost");

                var debitorCsvFileNameTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.CSVDebitor);
                var debitorCsvFileNameTarget = new TextSetupMenuItem(_context, debitorCsvFileNameTitle, _configuration, "CsvDebitor");

                var articleCsvFileNameTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.CSVArticle);
                var articleCsvFileNameTarget = new TextSetupMenuItem(_context, articleCsvFileNameTitle, _configuration, "CsvArticle");

                var writeDirectoryTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.WriteDirectory);
                var writeDirectoryTarget = new TextSetupMenuItem(_context, writeDirectoryTitle, _configuration, "WriteDirectory");

                var measurementsCsvFileNameTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.CSVMeasurements);
                var measurementsCsvFileNameTarget = new TextSetupMenuItem(_context, measurementsCsvFileNameTitle, _configuration, "CsvMeasurements");

                var barcodeReaderComPortTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.BarocdeReaderComPort);
                var barcodeReaderComPortTarget = new TextSetupMenuItem(_context, barcodeReaderComPortTitle, _configuration, "BarcodeReaderComPort");

                var usbUpdateResultTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.UpdateStatus);
                var usbUpdateResultTarget = new TextBlockSetupMenuItem(_context, usbUpdateResultTitle, _configuration, "UsbUpdateResult");

                var favoriteCustomerNumberTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.FavoriteCustomerNumber);
                var favoriteCustomerNumberTarget = new TextSetupMenuItem(_context, favoriteCustomerNumberTitle, _configuration, "FavoriteCustomerNumber");

                var workingEnvironmentTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.WorkingEnvironment);
                var workingEnvironmentTarget = new DropDownSetupMenuItem(_context, workingEnvironmentTitle, _configuration, "WorkingEnviroments", "SelectedWorkingEnvironment");

                
                //buttons können nicht in gruppen gelegt werden sondern müssen direkt an das child
                var button = new ImageButtonSetupMenuItem(_context, "embedded://IND930YMLUI/IND930YMLUI.images.FlashDisk.al8", new Color(0xFF, 0x00, 0x00, 0x00), Localization.GetTranslationModule(), (int)Localization.Key.Update, 20, SystemUpdate);


                var rangeGroup1 = new GroupedSetupMenuItems(_context, adminNameTarget, adminPasswordTarget, dataSourceTarget, writeDirectoryTarget);
                var rangeGroup2 = new GroupedSetupMenuItems(_context, remoteHostTarget, debitorCsvFileNameTarget, articleCsvFileNameTarget, measurementsCsvFileNameTarget);
                var rangeGroup3 = new GroupedSetupMenuItems(_context, workingEnvironmentTarget, barcodeReaderComPortTarget, favoriteCustomerNumberTarget, usbUpdateResultTarget);
                Children = Indexable.ImmutableValues<SetupMenuItem>(rangeGroup1, rangeGroup2, rangeGroup3, button);
                
                //Children = Indexable.ImmutableValues<SetupMenuItem>(rangeGroup1, rangeGroup2, button, rangeGroup3);
            }
            catch (Exception ex)
            {
                Log4NetManager.ApplicationLogger.Error("Error Setup Node.ShowChildrenAsync", ex);
            }

            return TaskEx.CompletedTask;
        }
        private void SystemUpdate()
        {
            bool success;
            success = IND930UI.Library.Update.SystemUpdate.ExecuteUpdate();

            if (success == true)
            {
                _configuration.UsbUpdateResult = "Update copied.";
            }
            else
            {
                _configuration.UsbUpdateResult = "Update failed!";
            }
        }
    }
}
