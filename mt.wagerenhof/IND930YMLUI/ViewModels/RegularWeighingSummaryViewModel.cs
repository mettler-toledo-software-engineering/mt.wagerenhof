﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;
using IND930UI.Library.Enum;
using IND930UI.Library.Models;

using IND930YMLUI.Infrastructure;
using IND930YMLUI.View;
using MT.Singularity.Composition;
using MT.Singularity.Data;
using MT.Singularity.Platform.Devices.Scale;
using MT.Singularity.Platform.Infrastructure;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Drawing;
using MT.Singularity.Presentation.Model;
using IND930UI.Library.EventManager;
using System.Threading.Tasks;
using DataAccess;
using IND930YMLUI.Config;
using MT.Singularity.Platform.Translation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;

namespace IND930YMLUI.ViewModels
{
    public class RegularWeighingSummaryViewModel : ObservableObject
    {
        private RegularLaundryModel _regularLaundry;
        private IScaleService _scaleService = ApplicationBootstrapperBase.CompositionContainer.Resolve<IScaleService>();
        private IWeighingEventProvider _weighingEventProvider = ApplicationBootstrapperBase.CompositionContainer.Resolve<IWeighingEventProvider>();
        private static string _settingsImage = @"embedded://IND930YMLUI/IND930YMLUI.images.Settings.al8";
        private static string _articleImagePath = $@"{Globals.GetDataDirectory()}\Images\article_small.png";
        private static string _containerImagePath = $@"{Globals.GetDataDirectory()}\Images\basket_small.png";
        private static string _debitorImagePath = $@"{Globals.GetDataDirectory()}\Images\debitor_small.png";
        private static string _okImage = @"embedded://IND930YMLUI/IND930YMLUI.images.ok.al8";
        private IComponents _components = ApplicationBootstrapperBase.CompositionContainer.Resolve<IComponents>();
        private Configuration _configuration;
        private double _measuredNetWeight = 0;
        private string _favorit = "";

        public RegularWeighingSummaryViewModel(RegularLaundryModel regularLaundry)
        {
            _regularLaundry = regularLaundry;

            SelectDebitorView = new SelectDebitorView(_regularLaundry);
            SelectArticleView = new SelectArticleView(_regularLaundry);
            SelectWagerenhofArticleView = new SelectWagerenhofArticleView(_regularLaundry);
            SelectContainerView = new SelectContainerView(_regularLaundry);
//            WeighLaundryView = new WeighLaundryView(_regularLaundry);
            _components.GetConfigurationAsync().ContinueWith(RegisterEvnts);
            _scaleService.SelectedScale.ClearTareAsync();
            UpdateButtonImages();

        }

        public INavigationPage Parent { get; set; }

        private void RegisterEvnts(Task<Configuration> obj)
        {
            if (obj.IsCompleted)
            {
                _configuration = obj.Result;
                _favorit = _configuration.FavoriteCustomerNumber;
                _configuration.PropertyChanged += ConfigurationOnPropertyChanged;

            }
        }
        private void ConfigurationOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "FavoriteCustomerNumber")
            {
                _favorit = _configuration.FavoriteCustomerNumber;
            }
        }
        public void PopulateProperties()
        {
            

            SelectedDebitor = _regularLaundry.SelectedDebitor?.DebitorName;
            SelectedContainer = _regularLaundry.SelectedContainer?.NameInGrid;
            SelectedArticle = _regularLaundry.SelectedArticle?.Quantity > 0 ? $"{_regularLaundry.SelectedArticle?.ArticleName} ({_regularLaundry.SelectedArticle?.Quantity} Stk)" : _regularLaundry.SelectedArticle?.ArticleName;
            SelectedNetWeight = string.IsNullOrEmpty(_regularLaundry.Unit) ? null : $"{_regularLaundry.MeasuredNetWeight:F2} {_regularLaundry.Unit}";
            

            UpdateButtonImages();
            TareScale();
        }

        private async Task TareScale()
        {
            if(_regularLaundry.SelectedContainer != null)
            {
                await _scaleService.SelectedScale.PresetTareAsync(_regularLaundry.SelectedContainer.TareWeightAsString, MT.Singularity.WellknownWeightUnit.Kilogram);
            }
        }

        private void UpdateButtonImages()
        {
            DebitorImage = SelectedDebitor != null ? _okImage : _debitorImagePath;
            ArticleImage = SelectedArticle != null ? _okImage : _articleImagePath;
            ContainerImage = SelectedContainer != null ? _okImage : _containerImagePath;
            GoToDebitorButtonBackgroundColor = SelectedDebitor != null ? Colors.GreenBrush : Globals.TransparantBlueBrush;
            GoToArticleButtonBackgroundColor = SelectedArticle != null ? Colors.GreenBrush : Globals.TransparantBlueBrush;
            GoToContainerButtonBackgroundColor = SelectedContainer != null ? Colors.GreenBrush : Globals.TransparantBlueBrush;
            
           
        }
        public void SetProcessStep()
        {
            _regularLaundry.CurrentProcessStep = RegularLaundryProcessStep.Idle;
            

        }

        public void RegisterEventsForViewModel()
        {
            _scaleService.SelectedScale.NewChangedWeightInDisplayUnit += SelectedScaleOnNewChangedWeightInDisplayUnit;
        }

        private void OnNewWeighingEvent(object sender, WeighingEventArgs e)
        {
            ResetProcess();
            Parent.NavigationService.Back();
        }

        public void UnregisterEventsForViewModel()
        {
            _scaleService.SelectedScale.NewChangedWeightInDisplayUnit -= SelectedScaleOnNewChangedWeightInDisplayUnit;

        }
        public void ClearTare()
        {
            _scaleService.SelectedScale.ClearTareAsync();
        }
        

        private SolidColorBrush _weighingButtonBackgroundColor;

        public SolidColorBrush WeighingButtonBackgroundColor
        {
            get { return _weighingButtonBackgroundColor; }
            set
            {
                if (_weighingButtonBackgroundColor != value)
                {
                    _weighingButtonBackgroundColor = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private SolidColorBrush _goToDebitorButtonBackgroundColor;

        public SolidColorBrush GoToDebitorButtonBackgroundColor
        {
            get { return _goToDebitorButtonBackgroundColor; }
            set
            {
                if (_goToDebitorButtonBackgroundColor != value)
                {
                    _goToDebitorButtonBackgroundColor = value;
                    NotifyPropertyChanged();
                }
            }
        }


        private SolidColorBrush _goToArticleButtonBackgroundColor;

        public SolidColorBrush GoToArticleButtonBackgroundColor
        {
            get { return _goToArticleButtonBackgroundColor; }
            set
            {
                if (_goToArticleButtonBackgroundColor != value)
                {
                    _goToArticleButtonBackgroundColor = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private SolidColorBrush _goToContainerButtonBackgroundColor;

        public SolidColorBrush GoToContainerButtonBackgroundColor
        {
            get { return _goToContainerButtonBackgroundColor; }
            set
            {
                if (_goToContainerButtonBackgroundColor != value)
                {
                    _goToContainerButtonBackgroundColor = value;
                    NotifyPropertyChanged();
                }
            }
        }


        private string _debitorImage;

        public string DebitorImage
        {
            get { return _debitorImage; }
            set
            {
                if (_debitorImage != value)
                {
                    _debitorImage = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private string _articleImage;

        public string ArticleImage
        {
            get { return _articleImage; }
            set
            {
                if (_articleImage != value)
                {
                    _articleImage = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private string _containerImage;

        public string ContainerImage
        {
            get { return _containerImage; }
            set
            {
                if (_containerImage != value)
                {
                    _containerImage = value;
                    NotifyPropertyChanged();
                }
            }
        }



        private string _selectedDebitor;

        public string SelectedDebitor
        {
            get { return _selectedDebitor; }
            set
            {
                if (_selectedDebitor != value)
                {
                    _selectedDebitor = value;
                    NotifyPropertyChanged();
                }


            }
        }


        private string _selectedContainer;

        public string SelectedContainer
        {
            get { return _selectedContainer; }
            set
            {
                if (_selectedContainer != value)
                {
                    _selectedContainer = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private string _selectedArticle;

        public string SelectedArticle
        {
            get { return _selectedArticle; }
            set
            {
                if (_selectedArticle != value)
                {
                    _selectedArticle = value;
                    NotifyPropertyChanged();
                }
            }
        }



        private string _selectedNetWeight;

        public string SelectedNetWeight
        {
            get { return _selectedNetWeight; }
            set
            {
                if (_selectedNetWeight != value)
                {
                    _selectedNetWeight = value;
                    NotifyPropertyChanged();
                }
            }
        }


        private SelectContainerView _selectContainerView;

        public SelectContainerView SelectContainerView
        {
            get { return _selectContainerView; }
            set
            {
                if (_selectContainerView != value)
                {
                    _selectContainerView = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public ICommand GoToSelectContainerCommand => new DelegateCommand(GoToSelectContainerCommandExecute);

        private void GoToSelectContainerCommandExecute()
        {
            //SelectContainerView = new SelectContainerView(_regularLaundryProcess);
        }

        private SelectArticleView _selectArticleView;

        public SelectArticleView SelectArticleView
        {
            get { return _selectArticleView; }
            set
            {
                if (_selectArticleView != value)
                {
                    _selectArticleView = value;
                    NotifyPropertyChanged();
                }
            }
        }
        private SelectWagerenhofArticleView _selectWagerenhofArticleView;

        public SelectWagerenhofArticleView SelectWagerenhofArticleView
        {
            get { return _selectWagerenhofArticleView; }
            set
            {
                if (_selectWagerenhofArticleView != value)
                {
                    _selectWagerenhofArticleView = value;
                    NotifyPropertyChanged();
                }
            }
        }


        public ICommand GoToSelectArticleCommand => new DelegateCommand(GoToSelectArticleCommandExecute);

        private void GoToSelectArticleCommandExecute()
        {
            if (_regularLaundry?.SelectedDebitor?.DebitorNumber == _favorit)
            {
                Parent.NavigationService.NavigateTo(SelectWagerenhofArticleView);
            }
            else
            {
                Parent.NavigationService.NavigateTo(SelectArticleView);
            }
            
        }

        public ICommand GoToSelectDebitorCommand => new DelegateCommand(GoToSelectDebitorCommandExecute);



        private void GoToSelectDebitorCommandExecute()
        {
            
        }

        private SelectDebitorView _selectDebitorView;

        public SelectDebitorView SelectDebitorView
        {
            get { return _selectDebitorView; }
            set
            {
                if (_selectDebitorView != value)
                {
                    _selectDebitorView = value;
                    NotifyPropertyChanged();
                }
            }
        }



  
        private void ResetProcess()
        {
            _regularLaundry.SelectedArticle = null;
            _regularLaundry.SelectedContainer = null;
            _regularLaundry.SelectedDebitor = null;
            _regularLaundry.Unit = null;
            PopulateProperties();
        }

        private void SelectedScaleOnNewChangedWeightInDisplayUnit(WeightInformation weight)
        {
            _measuredNetWeight = weight.NetWeight;
            _doWeighingCommand?.NotifyCanExecuteChanged();

        }

        private DelegateCommand _doWeighingCommand;
        public ICommand DoWeighingCommand => _doWeighingCommand = new DelegateCommand(DoWeighingCommandExecute, CanDoWeighingCommandExecute);

        private bool CanDoWeighingCommandExecute()
        {
            bool canExecute = _measuredNetWeight > 0 && SelectedContainer != null && SelectedArticle != null && SelectedDebitor != null;
            WeighingButtonBackgroundColor = canExecute ? Colors.LightBlueBrush : Colors.RedBrush;
            return canExecute;
        }

        private async void DoWeighingCommandExecute()
        {

            var weight = await _scaleService.SelectedScale.GetStableWeightAsync(UnitType.Display);
            _regularLaundry.MeasuredNetWeight = weight.NetWeight;
            _regularLaundry.Unit = weight.Unit.ToTranslatedAbbreviation();
            var success = SaveMeasureToFile();

            if (success)
            {
                MessageBox.Show((Visual)Parent, "Speichern Erfolgreich", "", MessageBoxButtons.OK, MessageBoxIcon.Information, Callback);
            }

        }

        private void Callback(DialogResult obj)
        {
            if (obj == DialogResult.OK)
            {

                ResetProcess();

                Parent.NavigationService.Back();
            }
        }

        private bool SaveMeasureToFile()
        {
            var measurement = CreateMeasurementModel();

            var dataaccess = ApplicationBootstrapperBase.CompositionContainer.Resolve<IGenericCsvAccess<MeasurementModel>>();
            dataaccess.FileMode = FileMode.Append;
            dataaccess.HasHeaderRecord = false;
            var result = dataaccess.SaveDataEntry(measurement);

            return result;
        }

        private MeasurementModel CreateMeasurementModel()
        {
            var measurement = new MeasurementModel();
            measurement.NetWeight = $"{_regularLaundry.MeasuredNetWeight:F2}";
            measurement.DebitorNumber = _regularLaundry.SelectedDebitor.DebitorNumber;
            measurement.ArticleNumber = _regularLaundry.SelectedArticle.ArticleNumber;
            measurement.Date = DateTime.Now.ToShortDateString();
            //measurement.GrossWeight = _regularLaundry.MeasuredNetWeight + _regularLaundry.SelectedContainer.TareWeight;
            measurement.Id = "";
            return measurement;
        }

    }
}
