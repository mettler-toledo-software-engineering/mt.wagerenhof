﻿using System;

using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using DataAccess;
using IND930UI.Library.Models;
using MT.Singularity.Composition;
using MT.Singularity.Data;
using MT.Singularity.Platform.Infrastructure;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Model;

namespace IND930YMLUI.ViewModels
{
    public class ManageArticlesViewModel : ObservableObject
    {
        
        private ObservableCollection<ArticleModel> _unmodifiedList;
        private Visual _parent;
        public ManageArticlesViewModel(Visual parent)
        {
            KeyboardVisibility = Visibility.Collapsed;
            
            _parent = parent;
            PopulateGrid();
        }

        public void RegisterEventsForViewModel()
        {

        }

        public void UnregisterEventsForViewModel()
        {

        }

        private void PopulateGrid()
        {
            List<ArticleModel> articleModels = null;
            try
            {
                var csvDebitorAccess = ApplicationBootstrapperBase.CompositionContainer.Resolve<IGenericCsvAccess<ArticleModel>>();

                articleModels = csvDebitorAccess.GetAllDataEntries().OrderBy(model => model.ArticleName).ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);

            }

            if (articleModels?.Count > 0)
            {
                ArticleList.Clear();
                articleModels.ForEach(model => ArticleList.Add(model));
                _unmodifiedList = new ObservableCollection<ArticleModel>(ArticleList);
                ArticleListIndex = 0;
                SelectedArticle = ArticleList[ArticleListIndex];
            }
            else
            {
                MessageBox.Show(_parent, "Es wurden keine Daten gefunden\nBitte kontaktieren Sie Ihren Administrator", "Achtung", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

        }

        private string _searchText;

        public string SearchText
        {
            get { return _searchText; }
            set
            {
                if (_searchText != value)
                {
                    _searchText = value;
                    NotifyPropertyChanged();
                    FilterList(_searchText);
                }
            }
        }

        private void FilterList(string searchText)
        {
            var resultList = _unmodifiedList.Where(model => model.NameInGrid.ToLowerInvariant().Contains(searchText.ToLowerInvariant())).ToList();
            ArticleList.Clear();
            resultList.ForEach(model => ArticleList.Add(model));

            if (resultList.Count == 1)
            {
                SelectedArticle = resultList[0];
                ArticleListIndex = 0;
            }
        }

        private Visibility _keyboardVisibility;

        public Visibility KeyboardVisibility
        {
            get { return _keyboardVisibility; }
            set
            {
                if (_keyboardVisibility != value)
                {
                    _keyboardVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }



        private object _selectedArticle;

        public object SelectedArticle
        {
            get { return _selectedArticle; }
            set
            {
                if (_selectedArticle != value)
                {
                    _selectedArticle = value;

                    NotifyPropertyChanged();
                }
            }
        }

        private int _articleListIndex;

        public int ArticleListIndex
        {
            get { return _articleListIndex; }
            set
            {
                if (_articleListIndex != value)
                {
                    _articleListIndex = value;
                    NotifyPropertyChanged();
                    _scrollDownCommand?.NotifyCanExecuteChanged();
                    _scrollUpCommand?.NotifyCanExecuteChanged();
                }
            }
        }

        private ObservableCollection<ArticleModel> _articleList = new ObservableCollection<ArticleModel>();

        public ObservableCollection<ArticleModel> ArticleList
        {
            get { return _articleList; }
            set
            {
                if (_articleList != value)
                {
                    _articleList = value;
                    NotifyPropertyChanged();
                }
            }
        }


        private DelegateCommand _scrollDownCommand;
        public ICommand ScrollDownCommand => _scrollDownCommand = new DelegateCommand(ScrollDownCommandExecute, CanScrollDownCommandExecute);

        private bool CanScrollDownCommandExecute()
        {
            if (ArticleList.Count < 10 || ArticleListIndex == ArticleList.Count - 1)
            {
                return false;
            }
            else return true;
        }

        private void ScrollDownCommandExecute()
        {
            if (ArticleListIndex + 5 >= ArticleList.Count)
            {
                ArticleListIndex = ArticleList.Count - 1;
            }
            else
            {
                ArticleListIndex = ArticleListIndex + 5;
            }
        }

        private DelegateCommand _scrollUpCommand;

        public ICommand ScrollUpCommand => _scrollUpCommand = new DelegateCommand(ScrollUpCommandExecute, CanScrollUpCommandExecute);
        private bool CanScrollUpCommandExecute()
        {
            if (ArticleList.Count < 10 || ArticleListIndex <= 0)
            {
                return false;
            }
            else return true;
        }

        private void ScrollUpCommandExecute()
        {
            if (ArticleListIndex <= 5)
            {
                ArticleListIndex = 0;
            }
            else
            {
                ArticleListIndex = ArticleListIndex - 5;
            }
        }
    }
}
