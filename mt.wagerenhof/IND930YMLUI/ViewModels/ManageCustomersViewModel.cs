﻿using System;

using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using DataAccess;
using DevicesLibrary.BarcodeReader;
using IND930UI.Library.Models;
using MT.Singularity.Composition;
using MT.Singularity.Data;
using MT.Singularity.Platform.Devices.Scale;
using MT.Singularity.Platform.Infrastructure;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Model;

namespace IND930YMLUI.ViewModels
{

    public class ManageCustomersViewModel : ObservableObject
    {
        
        private ObservableCollection<DebitorModel> _unmodifiedList;
        private Visual _parent;

        public ManageCustomersViewModel(Visual parent)
        {
            KeyboardVisibility = Visibility.Collapsed;
            _parent = parent;
            PopulateGrid();
        }

        public void RegisterEventsForViewModel()
        {

        }

        public void UnregisterEventsForViewModel()
        {

        }

        private void PopulateGrid()
        {
            List<DebitorModel> debitorModels = null;
            try
            {
                var csvDebitorAccess = ApplicationBootstrapperBase.CompositionContainer.Resolve<IGenericCsvAccess<DebitorModel>>();
                debitorModels = csvDebitorAccess.GetAllDataEntries().OrderBy(model => model.DebitorName).ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);

            }

            if (debitorModels?.Count > 0)
            {
                DebitorList.Clear();
                debitorModels.ForEach(model => DebitorList.Add(model));
                _unmodifiedList = new ObservableCollection<DebitorModel>(DebitorList);
                DebitorListIndex = 0;
                SelectedDebitor = DebitorList[DebitorListIndex];
            }
            else
            {
                MessageBox.Show(_parent, "Es wurden keine Daten gefunden\nBitte kontaktieren Sie Ihren Administrator", "Achtung", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

        }

        private string _searchText;

        public string SearchText
        {
            get { return _searchText; }
            set
            {
                if (_searchText != value)
                {
                    _searchText = value;
                    NotifyPropertyChanged();
                    FilterList(_searchText);
                }
            }
        }

        private void FilterList(string searchText)
        {
            var resultList = _unmodifiedList.Where(model => model.NameInGrid.ToLowerInvariant().Contains(searchText.ToLowerInvariant())).ToList();
            DebitorList.Clear();
            resultList.ForEach(model => DebitorList.Add(model));

            if (resultList.Count == 1)
            {
                SelectedDebitor = resultList[0];
                DebitorListIndex = 0;
            }
        }

        private Visibility _keyboardVisibility;

        public Visibility KeyboardVisibility
        {
            get { return _keyboardVisibility; }
            set
            {
                if (_keyboardVisibility != value)
                {
                    _keyboardVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }



        private object _selectedDebitor;

        public object SelectedDebitor
        {
            get { return _selectedDebitor; }
            set
            {
                if (_selectedDebitor != value)
                {
                    _selectedDebitor = value;

                    NotifyPropertyChanged();
                }
            }
        }

        private int _debitorListIndex;

        public int DebitorListIndex
        {
            get { return _debitorListIndex; }
            set
            {
                if (_debitorListIndex != value)
                {
                    _debitorListIndex = value;
                    NotifyPropertyChanged();
                    _scrollDownCommand?.NotifyCanExecuteChanged();
                    _scrollUpCommand?.NotifyCanExecuteChanged();
                }
            }
        }

        private ObservableCollection<DebitorModel> _debitorList = new ObservableCollection<DebitorModel>();

        public ObservableCollection<DebitorModel> DebitorList
        {
            get { return _debitorList; }
            set
            {
                if (_debitorList != value)
                {
                    _debitorList = value;
                    NotifyPropertyChanged();
                }
            }
        }


        private DelegateCommand _scrollDownCommand;
        public ICommand ScrollDownCommand => _scrollDownCommand = new DelegateCommand(ScrollDownCommandExecute, CanScrollDownCommandExecute);

        private bool CanScrollDownCommandExecute()
        {
            if (DebitorList.Count < 10 || DebitorListIndex == DebitorList.Count - 1)
            {
                return false;
            }
            else return true;
        }

        private void ScrollDownCommandExecute()
        {
            if (DebitorListIndex + 5 >= DebitorList.Count)
            {
                DebitorListIndex = DebitorList.Count - 1;
            }
            else
            {
                DebitorListIndex = DebitorListIndex + 5;
            }
        }

        private DelegateCommand _scrollUpCommand;

        public ICommand ScrollUpCommand => _scrollUpCommand = new DelegateCommand(ScrollUpCommandExecute, CanScrollUpCommandExecute);
        private bool CanScrollUpCommandExecute()
        {
            if (DebitorList.Count < 10 || DebitorListIndex <= 0)
            {
                return false;
            }
            else return true;
        }

        private void ScrollUpCommandExecute()
        {
            if (DebitorListIndex <= 5)
            {
                DebitorListIndex = 0;
            }
            else
            {
                DebitorListIndex = DebitorListIndex - 5;
            }
        }
    }
}
