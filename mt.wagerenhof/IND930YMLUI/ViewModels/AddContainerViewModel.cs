﻿using System;

using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using DataAccess;
using IND930UI.Library.Models;
using MT.Singularity.Composition;
using MT.Singularity.Data;
using MT.Singularity.Platform.Infrastructure;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Model;

namespace IND930YMLUI.ViewModels
{
    public class AddContainerViewModel : ObservableObject
    {
        

        private ChildWindow _parent;
        private ContainerModel _uneditedContainer;
        public AddContainerViewModel(ChildWindow parent, string containerName, double containerWeight)
        {

            //todo nicht die liste übergeben sondern nur das selektierte objekt zusammen mit index

            _parent = parent;
            _containerName = containerName;
            _containerWeight = Math.Round(containerWeight, 2);

        }

        public AddContainerViewModel(ChildWindow parent, ContainerModel selectedContainer)
        {
            _parent = parent;
            _uneditedContainer = selectedContainer;
            _containerName = selectedContainer.Name;
            _containerWeight = selectedContainer.TareWeight;
        }



        private string _containerName;

        public string ContainerName
        {
            get { return _containerName; }
            set
            {
                if (_containerName != value)
                {
                    _containerName = value;
                    NotifyPropertyChanged();
                    _okCommand?.NotifyCanExecuteChanged();
                }
            }
        }


        private double _containerWeight;

        public double ContainerWeight
        {
            get { return _containerWeight; }
            set
            {
                if ( _containerWeight.Equals(value) == false)
                {
                    _containerWeight = value;
                    NotifyPropertyChanged();
                    _okCommand?.NotifyCanExecuteChanged();
                }
            }
        }

        private DelegateCommand _okCommand;
        public ICommand OkCommand => _okCommand = new DelegateCommand(OkCommandExecute, CanOkCommandExecute);

        private void OkCommandExecute()
        {
            var csvContainerAccess = ApplicationBootstrapperBase.CompositionContainer.Resolve<IGenericCsvAccess<ContainerModel>>();
            var container = new ContainerModel
            {

                Name = ContainerName,
                TareWeight = ContainerWeight,
                Unit = "kg"
            };

            if (_uneditedContainer != null)
            {
                csvContainerAccess.DeleteDataEntry(_uneditedContainer.Id);
                container.Id = _uneditedContainer.Id;
            }
            else
            {
                container.Id = csvContainerAccess.GenerateId();
            }

            csvContainerAccess.SaveDataEntry(container);

            _parent.DialogResult = DialogResult.OK;
            _parent.Close();
        }

        private bool CanOkCommandExecute()
        {
            bool result = string.IsNullOrWhiteSpace(ContainerName) == false && ContainerWeight > 0 && double.IsNaN(ContainerWeight) == false;
            return result;
        }

    }
}
