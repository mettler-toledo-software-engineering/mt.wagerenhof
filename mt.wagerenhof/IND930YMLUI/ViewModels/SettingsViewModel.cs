﻿using System;

using System.Collections.Generic;
using System.Text;
using IND930YMLUI.View;
using MT.Singularity.Data;

namespace IND930YMLUI.ViewModels
{
    public class SettingsViewModel : ObservableObject
    {

        public SettingsViewModel()
        {
            
        }

        public void RegisterEventsForViewModel()
        {
        }
        public void UnregisterEventsForViewModel()
        {
        }

        private ManageContainerView _manageContainerView = new ManageContainerView();

        public ManageContainerView ManageContainerView
        {
            get { return _manageContainerView; }
            set
            {
                if (_manageContainerView != value)
                {
                    _manageContainerView = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private ManageArticlesView _manageArticlesView = new ManageArticlesView();

        public ManageArticlesView ManageArticlesView
        {
            get { return _manageArticlesView; }
            set
            {
                if (_manageArticlesView != value)
                {
                    _manageArticlesView = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private ManageCustomersView _manageCustomersView = new ManageCustomersView();

        public ManageCustomersView ManageCustomersView
        {
            get { return _manageCustomersView; }
            set
            {
                if (_manageCustomersView != value)
                {
                    _manageCustomersView = value;
                    NotifyPropertyChanged();
                }
            }
        }
    }
}
