﻿using System;

using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using DataAccess;
using DevicesLibrary;
using DevicesLibrary.BarcodeReader;
using IND930UI.Library.Enum;
using IND930UI.Library.Models;
using IND930YMLUI.View;
using MT.Singularity.Composition;
using MT.Singularity.Data;
using MT.Singularity.Logging;
using MT.Singularity.Platform.Infrastructure;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Model;

namespace IND930YMLUI.ViewModels
{
    public class SelectArticleViewModel : ObservableObject
    {

        private IBarcodeReader _barcodeReader;
        private ObservableCollection<ArticleModel> _unmodifiedList;
        private Visual _parent;
        private RegularLaundryModel _regularLaundry;
        private CompanyLaundryProcessModel _companyLaundry;
        public SelectArticleViewModel(Visual parent, RegularLaundryModel regularLaundry)
        {
            _regularLaundry = regularLaundry;
            KeyboardVisibility = Visibility.Collapsed;
            _parent = parent;
            

        }

        public SelectArticleViewModel(Visual parent, CompanyLaundryProcessModel companyLaundry)
        {
            _companyLaundry = companyLaundry;
            KeyboardVisibility = Visibility.Collapsed;
            _parent = parent;

        }
        public void SetProcessStep()
        {
            PopulateGrid();
            if (_regularLaundry != null)
            {
                _regularLaundry.CurrentProcessStep = RegularLaundryProcessStep.SelectArticle;
            }
            
        }

        public void RegisterEventsForViewModel()
        {
            if (_barcodeReader != null)
            {
                _barcodeReader.DataReceivedEvent += BarcodeReaderOnDataReceivedEvent;
            }
        }
        public void UnregisterEventsForViewModel()
        {
            if (_barcodeReader != null)
            {
                _barcodeReader.DataReceivedEvent -= BarcodeReaderOnDataReceivedEvent;
            }
        }
        public void RegisterBarcodeReader()
        {
            bool success = ApplicationBootstrapperBase.CompositionContainer.TryResolve(out _barcodeReader);
            if (success)
            {
                Log4NetManager.ApplicationLogger.Info($"barcode reader on viewmodel ready");
            }
            else
            {
                Log4NetManager.ApplicationLogger.Error($"barcode reader on viewmodel NULL");
            }
        }
        private void BarcodeReaderOnDataReceivedEvent(object sender, DataReceivedEventArgs e)
        {
            Log4NetManager.ApplicationLogger.Info($"barcode reader on viewmodel data received{e.Data}");
            SearchText = e.Data;
        }

        private int _selectedQuantity;

        public int SelectedQuantity
        {
            get { return _selectedQuantity; }
            set
            {
                if (_selectedQuantity != value)
                {
                    _selectedQuantity = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private void PopulateGrid()
        {
            List<ArticleModel> articleModels = null;
            try
            {
                var csvArticleAccess = ApplicationBootstrapperBase.CompositionContainer.Resolve<IGenericCsvAccess<ArticleModel>>();
                articleModels = csvArticleAccess.GetAllDataEntries().OrderBy(model => model.ArticleName).ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);

            }

            if (articleModels?.Count > 0)
            {
                ArticleList.Clear();
                articleModels.ForEach(model => ArticleList.Add(model));
                _unmodifiedList = new ObservableCollection<ArticleModel>(ArticleList);
                ArticleListIndex = 0;
                SelectedArticle = ArticleList[ArticleListIndex];
            }
            else
            {
                MessageBox.Show(_parent, "Es wurden keine Daten gefunden\nBitte kontaktieren Sie Ihren Administrator", "Achtung", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

        }

        private string _searchText;

        public string SearchText
        {
            get { return _searchText; }
            set
            {
                if (_searchText != value)
                {
                    _searchText = value;
                    NotifyPropertyChanged();
                    FilterList(_searchText);
                }
            }
        }

        private void FilterList(string searchText)
        {
            var resultList = _unmodifiedList.Where(model => model.NameInGrid.ToLowerInvariant().Contains(searchText.ToLowerInvariant())).ToList();
            ArticleList.Clear();
            resultList.ForEach(model => ArticleList.Add(model));

            if (resultList.Count == 1)
            {
                SelectedArticle = resultList[0];
                ArticleListIndex = 0;
            }
        }

        private Visibility _keyboardVisibility;

        public Visibility KeyboardVisibility
        {
            get { return _keyboardVisibility; }
            set
            {
                if (_keyboardVisibility != value)
                {
                    _keyboardVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }



        private object _selectedArticle;

        public object SelectedArticle
        {
            get { return _selectedArticle; }
            set
            {
                if (_selectedArticle != value)
                {
                    _selectedArticle = value;
                    NotifyPropertyChanged();
                    _confirmSelectionCommand?.NotifyCanExecuteChanged();
                }
            }
        }

        private int _articleListIndex;

        public int ArticleListIndex
        {
            get { return _articleListIndex; }
            set
            {
                if (_articleListIndex != value)
                {
                    _articleListIndex = value;
                    NotifyPropertyChanged();
                    _scrollDownCommand?.NotifyCanExecuteChanged();
                    _scrollUpCommand?.NotifyCanExecuteChanged();
                }
            }
        }

        private ObservableCollection<ArticleModel> _articleList = new ObservableCollection<ArticleModel>();

        public ObservableCollection<ArticleModel> ArticleList
        {
            get { return _articleList; }
            set
            {
                if (_articleList != value)
                {
                    _articleList = value;
                    NotifyPropertyChanged();
                }
            }
        }


        private DelegateCommand _scrollDownCommand;
        public ICommand ScrollDownCommand => _scrollDownCommand = new DelegateCommand(ScrollDownCommandExecute, CanScrollDownCommandExecute);

        private bool CanScrollDownCommandExecute()
        {
            if (ArticleList.Count < 10 || ArticleListIndex == ArticleList.Count - 1)
            {
                return false;
            }
            else return true;
        }

        private void ScrollDownCommandExecute()
        {
            if (ArticleListIndex + 5 >= ArticleList.Count)
            {
                ArticleListIndex = ArticleList.Count - 1;
            }
            else
            {
                ArticleListIndex = ArticleListIndex + 5;
            }
        }

        private DelegateCommand _scrollUpCommand;

        public ICommand ScrollUpCommand => _scrollUpCommand = new DelegateCommand(ScrollUpCommandExecute, CanScrollUpCommandExecute);
        private bool CanScrollUpCommandExecute()
        {
            if (ArticleList.Count < 10 || ArticleListIndex <= 0)
            {
                return false;
            }
            else return true;
        }

        private void ScrollUpCommandExecute()
        {
            if (ArticleListIndex <= 5)
            {
                ArticleListIndex = 0;
            }
            else
            {
                ArticleListIndex = ArticleListIndex - 5;
            }
        }

        private DelegateCommand _confirmSelectionCommand;
        public ICommand ConfirmSelectionCommand => _confirmSelectionCommand = new DelegateCommand(ConfirmSelectionCommandExecute, CanConfirmSelectionCommandExecute);
        private bool CanConfirmSelectionCommandExecute()
        {
            return SelectedArticle != null;
        }

        private void ConfirmSelectionCommandExecute()
        {
            if (_regularLaundry != null)
            {
                _regularLaundry.SelectedArticle = (ArticleModel)SelectedArticle;
                _regularLaundry.SelectedArticle.Quantity = SelectedQuantity;
            }

            if (_companyLaundry != null)
            {
                _companyLaundry.SelectedCompanyLaundry.SelectedArticle = (ArticleModel)SelectedArticle;
                _companyLaundry.SelectedCompanyLaundry.SelectedArticle.Quantity = SelectedQuantity;
            }
        }

        public void ClearSearchText()
        {
            SearchText = string.Empty;
        }
    }
}
