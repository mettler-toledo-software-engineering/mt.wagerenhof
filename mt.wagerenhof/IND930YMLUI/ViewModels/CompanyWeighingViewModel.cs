﻿using System;

using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using IND930UI.Library.Extensions;
using IND930UI.Library.Models;
using MT.Singularity.Composition;
using MT.Singularity.Data;
using MT.Singularity.Platform.Devices.Scale;
using MT.Singularity.Platform.Infrastructure;
using MT.Singularity.Platform.Translation;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Presentation.Model;
using System.Threading.Tasks;
using MT.Singularity.Presentation.Drawing;
using MT.Singularity.Presentation;

namespace IND930YMLUI.ViewModels
{
    public class CompanyWeighingViewModel : ObservableObject
    {
        private INavigationPage _parent;
        private CompanyLaundryProcessModel _companyLaundryProcess;
        private IScaleService _scaleService = ApplicationBootstrapperBase.CompositionContainer.Resolve<IScaleService>();
        private double _netWeight = 0;
        public CompanyWeighingViewModel(INavigationPage parent, CompanyLaundryProcessModel companyLaundryProcess)
        {
            _parent = parent;
            _companyLaundryProcess = companyLaundryProcess;
            InitNetWeight();
        }

        public async void InitNetWeight()
        {
            _netWeight = (await _scaleService.SelectedScale.GetStableWeightAsync(UnitType.Display)).NetWeight;
            _weighContainerCommand?.NotifyCanExecuteChanged();
        }
        public void RegisterEventsForViewModel()
        {
            _scaleService.SelectedScale.NewChangedWeightInDisplayUnit += SelectedScaleOnNewChangedWeightInDisplayUnit;
            _scaleService.SelectedScale.PresetTareAsync(_companyLaundryProcess.SelectedCompanyLaundry.SelectedContainer.TareWeightAsString, _scaleService.SelectedScale.CurrentDisplayUnit);
            LastMeasuredNetWeight = $"{_companyLaundryProcess?.SelectedCompanyLaundry?.LastNetWeightAsString} {_scaleService.SelectedScale.CurrentDisplayUnit.ToTranslatedAbbreviation()}";
        }

        private void SelectedScaleOnNewChangedWeightInDisplayUnit(WeightInformation weight)
        {
            _netWeight = weight.NetWeight;
            _weighContainerCommand?.NotifyCanExecuteChanged();
        }

        public void UnregisterEventsForViewModel()
        {
            _scaleService.SelectedScale.NewChangedWeightInDisplayUnit -= SelectedScaleOnNewChangedWeightInDisplayUnit;
            _companyLaundryProcess.SelectedCompanyLaundry.CurrentNetWeight = 0;
        }

        public void PopulateProperties()
        {
            SelectedContainer = _companyLaundryProcess?.SelectedCompanyLaundry?.SelectedContainer?.NameInGrid;

            CurrentMeasuredNetWeight = $"{_companyLaundryProcess?.SelectedCompanyLaundry?.CurrentNetWeightAsString} {_scaleService.SelectedScale.CurrentDisplayUnit.ToTranslatedAbbreviation()}";
            TotalDistributedNetWeight = $"{_companyLaundryProcess?.SelectedCompanyLaundry?.TotalDistributedNetWeightAsString} {_scaleService.SelectedScale.CurrentDisplayUnit.ToTranslatedAbbreviation()}";
            CompanyWheighingViewTitle = $"{Localization.Get(Localization.Key.CompanyLaundrySummary)} {_companyLaundryProcess?.SelectedCompanyLaundry?.SelectedContainer?.Name} {_companyLaundryProcess?.SelectedCompanyLaundry?.SelectedArticleName}";
        }

        private string _companyWheighingViewTitle;

        public string CompanyWheighingViewTitle
        {
            get { return _companyWheighingViewTitle; }
            set
            {
                if (_companyWheighingViewTitle != value)
                {
                    _companyWheighingViewTitle = value;
                    NotifyPropertyChanged();
                }
            }
        }


        private string _selectedContainer;

        public string SelectedContainer
        {
            get { return _selectedContainer; }
            set
            {
                if (_selectedContainer != value)
                {
                    _selectedContainer = value;
                    NotifyPropertyChanged();
                }

            }
        }

        private string _totalDistributedNetWeight;

        public string TotalDistributedNetWeight
        {
            get { return _totalDistributedNetWeight; }
            set
            {
                if (_totalDistributedNetWeight != value)
                {
                    _totalDistributedNetWeight = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private string _lastMeasuredNetWeight;

        public string LastMeasuredNetWeight
        {
            get { return _lastMeasuredNetWeight; }
            set
            {
                if (_lastMeasuredNetWeight != value)
                {
                    _lastMeasuredNetWeight = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private string _currentMeasuredNetWeight;

        public string CurrentMeasuredNetWeight
        {
            get { return _currentMeasuredNetWeight; }
            set
            {
                if (_currentMeasuredNetWeight != value)
                {
                    _currentMeasuredNetWeight = value;
                    NotifyPropertyChanged();
                }
            }
        }
        private SolidColorBrush _weighContainerButtonColor;

        public SolidColorBrush WeighContainerButtonColor
        {
            get { return _weighContainerButtonColor; }
            set
            {
                if (_weighContainerButtonColor != value)
                {
                    _weighContainerButtonColor = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private DelegateCommand _weighContainerCommand;
        public ICommand WeighContainerCommand => _weighContainerCommand = new DelegateCommand(WeighContainerCommandExecute, CanWeighContainerCommandExecute);

        private bool CanWeighContainerCommandExecute()
           
        {
            bool canExecute = _netWeight > 0 || _companyLaundryProcess?.SelectedCompanyLaundry?.LastNetWeight > 0;
            if (canExecute)
            {
                WeighContainerButtonColor = Colors.LightBlueBrush;
            }
            else
            {
                WeighContainerButtonColor = Colors.RedBrush;
            }
            return canExecute;
        }


        private async void WeighContainerCommandExecute()
        {
            _companyLaundryProcess.SelectedCompanyLaundry.CurrentNetWeight = (await _scaleService.SelectedScale.GetStableWeightAsync(UnitType.Display)).NetWeight;
            _companyLaundryProcess.SelectedCompanyLaundry.CalculateTotalNetWeight();
            PopulateProperties();
            WeighContainerButtonColor = Colors.GreenBrush;
        }

        public async Task TareScale()
        {
            if (_companyLaundryProcess.SelectedCompanyLaundry.SelectedContainer != null)
            {

                await _scaleService.SelectedScale.PresetTareAsync(_companyLaundryProcess.SelectedCompanyLaundry.SelectedContainer.TareWeightAsString, MT.Singularity.WellknownWeightUnit.Kilogram);
            }
        }

    }
}
