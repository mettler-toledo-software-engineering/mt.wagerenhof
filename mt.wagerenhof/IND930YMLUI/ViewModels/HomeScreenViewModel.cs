﻿// ReSharper disable once RedundantUsingDirective
using MT.Singularity;
using MT.Singularity.Data;
using MT.Singularity.Platform.CommonUX.Controls.DeltaTrac;
using MT.Singularity.Presentation.Model;

namespace IND930YMLUI.ViewModels
{
    /// <summary>
    /// Home Screen view model implementation
    /// </summary>
    public class HomeScreenViewModel : PropertyChangedBase
    {
        System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
        /// <summary>
        /// Constructor Initializes a new instance of the <see cref="HomeScreenViewModel"/> class.
        /// </summary>
        public HomeScreenViewModel()
        {
            customCulture.NumberFormat.NumberDecimalSeparator = ".";

            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
        }






    }
}
