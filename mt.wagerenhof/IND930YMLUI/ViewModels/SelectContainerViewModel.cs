﻿using System;

using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using DataAccess;
using DevicesLibrary;
using DevicesLibrary.BarcodeReader;
using IND930UI.Library.Enum;
using IND930UI.Library.Models;
using IND930YMLUI.Infrastructure;
using IND930YMLUI.View;
using MT.Singularity;
using MT.Singularity.Composition;
using MT.Singularity.Data;
using MT.Singularity.Expressions;
using MT.Singularity.Logging;
using MT.Singularity.Platform.CommonUX.Translators;
using MT.Singularity.Platform.Devices.Scale;
using MT.Singularity.Platform.Infrastructure;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Presentation.Model;

namespace IND930YMLUI.ViewModels
{
    public class SelectContainerViewModel : ObservableObject
    {

        private IScaleService _scaleService = ApplicationBootstrapperBase.CompositionContainer.Resolve<IScaleService>();
        private IBarcodeReader _barcodeReader;
        private ObservableCollection<ContainerModel> _unmodifiedList;
        private INavigationPage _parentPage;
        private Visual _parentVisual;
        private RegularLaundryModel _regularLaundry;
        private CompanyLaundryProcessModel _companyLaundryProcess;
        public SelectContainerViewModel(INavigationPage parentPage, Visual parentVisual, RegularLaundryModel regularLaundry)
        {
            KeyboardVisibility = Visibility.Collapsed;
            _regularLaundry = regularLaundry;
            _parentPage = parentPage;
            _parentVisual = parentVisual;
            PopulateGrid();
        }

        public SelectContainerViewModel(INavigationPage parentPage, Visual parentVisual, CompanyLaundryProcessModel companyLaundryProcess)
        {
            KeyboardVisibility = Visibility.Collapsed;
            _companyLaundryProcess = companyLaundryProcess;
            _parentPage = parentPage;
            _parentVisual = parentVisual;
            PopulateGrid();
        }

        public void SetProcessStep()
        {
            PopulateGrid();
        }

        public void RegisterEventsForViewModel()
        {
            PropertyChanged += OnPropertyChanged;

            if (_barcodeReader != null)
            {
                _barcodeReader.DataReceivedEvent += BarcodeReaderOnDataReceivedEvent;
            }
        }

        private async void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "SelectedContainer")
            {
                var container = (ContainerModel)SelectedContainer;
                if (container != null)
                {
                    await _scaleService.SelectedScale.PresetTareAsync(container.TareWeightAsString, _scaleService.SelectedScale.CurrentDisplayUnit);
                }
            }
        }

        public void UnregisterEventsForViewModel()
        {
            PropertyChanged -= OnPropertyChanged;

            if (_barcodeReader != null)
            {
                _barcodeReader.DataReceivedEvent -= BarcodeReaderOnDataReceivedEvent;
            }
        }
        public void RegisterBarcodeReader()
        {
            bool success = ApplicationBootstrapperBase.CompositionContainer.TryResolve(out _barcodeReader);
            if (success)
            {
                Log4NetManager.ApplicationLogger.Info($"barcode reader on viewmodel ready");
            }
            else
            {
                Log4NetManager.ApplicationLogger.Error($"barcode reader on viewmodel NULL");
            }
        }
        private void BarcodeReaderOnDataReceivedEvent(object sender, DataReceivedEventArgs e)
        {
            Log4NetManager.ApplicationLogger.Info($"barcode reader on viewmodel data received{e.Data}");
            SearchText = e.Data;
        }

        private void PopulateGrid()
        {
            List<ContainerModel> containerModels = null;
            try
            {
                var csvContainerAccess = ApplicationBootstrapperBase.CompositionContainer.Resolve<IGenericCsvAccess<ContainerModel>>();
                containerModels = csvContainerAccess.GetAllDataEntries().OrderBy(model => model.NameInGrid).ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);

            }

            if (containerModels?.Count > 0)
            {
                ContainerList.Clear();
                containerModels.ForEach(model => ContainerList.Add(model));
                _unmodifiedList = new ObservableCollection<ContainerModel>(ContainerList);
                ContainerListIndex = 0;
                SelectedContainer = ContainerList[ContainerListIndex];
            }
            else
            {
                MessageBox.Show(_parentVisual, "Es wurden keine Daten gefunden\nBitte kontaktieren Sie Ihren Administrator", "Achtung", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

        }

        private string _searchText;

        public string SearchText
        {
            get { return _searchText; }
            set
            {
                if (_searchText != value)
                {
                    _searchText = value;
                    NotifyPropertyChanged();
                    FilterList(_searchText);
                }
            }
        }

        private void FilterList(string searchText)
        {
            var resultList = _unmodifiedList.Where(model => model.NameInGrid.ToLowerInvariant().Contains(searchText.ToLowerInvariant())).ToList();
            ContainerList.Clear();
            resultList.ForEach(model => ContainerList.Add(model));

            if (resultList.Count == 1)
            {
                SelectedContainer = resultList[0];
                ContainerListIndex = 0;
            }
        }

        private Visibility _keyboardVisibility;

        public Visibility KeyboardVisibility
        {
            get { return _keyboardVisibility; }
            set
            {
                if (_keyboardVisibility != value)
                {
                    _keyboardVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }



        private object _selectedContainer;

        public object SelectedContainer
        {
            get { return _selectedContainer; }
            set
            {
                if (_selectedContainer != value)
                {
                    _selectedContainer = value;
                    NotifyPropertyChanged();
                    _confirmSelectionCommand?.NotifyCanExecuteChanged();
                }
            }
        }

        private int _containerListIndex;

        public int ContainerListIndex
        {
            get { return _containerListIndex; }
            set
            {
                if (_containerListIndex != value)
                {
                    _containerListIndex = value;
                    NotifyPropertyChanged();
                    _scrollDownCommand?.NotifyCanExecuteChanged();
                    _scrollUpCommand?.NotifyCanExecuteChanged();
                }
            }
        }

        private ObservableCollection<ContainerModel> _containerList = new ObservableCollection<ContainerModel>();

        public ObservableCollection<ContainerModel> ContainerList
        {
            get { return _containerList; }
            set
            {
                if (_containerList != value)
                {
                    _containerList = value;
                    NotifyPropertyChanged();
                }
            }
        }


        private DelegateCommand _scrollDownCommand;
        public ICommand ScrollDownCommand => _scrollDownCommand = new DelegateCommand(ScrollDownCommandExecute, CanScrollDownCommandExecute);

        private bool CanScrollDownCommandExecute()
        {
            if (ContainerList.Count < 10 || ContainerListIndex == ContainerList.Count - 1)
            {
                return false;
            }
            else return true;
        }

        private void ScrollDownCommandExecute()
        {
            if (ContainerListIndex + 5 >= ContainerList.Count)
            {
                ContainerListIndex = ContainerList.Count - 1;
            }
            else
            {
                ContainerListIndex = ContainerListIndex + 5;
            }
        }

        private DelegateCommand _scrollUpCommand;

        public ICommand ScrollUpCommand => _scrollUpCommand = new DelegateCommand(ScrollUpCommandExecute, CanScrollUpCommandExecute);
        private bool CanScrollUpCommandExecute()
        {
            if (ContainerList.Count < 10 || ContainerListIndex <= 0)
            {
                return false;
            }
            else return true;
        }

        private void ScrollUpCommandExecute()
        {
            if (ContainerListIndex <= 5)
            {
                ContainerListIndex = 0;
            }
            else
            {
                ContainerListIndex = ContainerListIndex - 5;
            }
        }

        private DelegateCommand _confirmSelectionCommand;
        public ICommand ConfirmSelectionCommand => _confirmSelectionCommand = new DelegateCommand(ConfirmSelectionCommandExecute, CanConfirmSelectionCommandExecute);
        private bool CanConfirmSelectionCommandExecute()
        {
            return SelectedContainer != null;
        }

        private void ConfirmSelectionCommandExecute()
        {

            AddContainerToRegularLaundryProcess();

            AddContainerToCompanyLaundryProcess();


        }

        private void AddContainerToRegularLaundryProcess()
        {
            if (_regularLaundry != null)
            {
                _regularLaundry.SelectedContainer = (ContainerModel)SelectedContainer;
                _parentPage.NavigationService.Back();
            }
        }
        private void AddContainerToCompanyLaundryProcess()
        {
            if (_companyLaundryProcess != null)
            {
                var ergebnis = _companyLaundryProcess.AddContainerToProcess((ContainerModel)SelectedContainer, Globals.DefaultDebitor);
                if (ergebnis == AddContainerResult.Duplicate)
                {

                    MessageBox.Show(_parentVisual, "Container wurde bereits ausgewählt", "Achtung", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    _parentPage.NavigationService.Back();
                }

            }
        }
        public void ClearSearchText()
        {
            SearchText = string.Empty;
        }
    }


}
