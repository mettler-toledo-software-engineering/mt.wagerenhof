﻿using System;

using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using DataAccess;
using IND930UI.Library.Enum;
using IND930UI.Library.Models;
using IND930UI.Library.EventManager;
using IND930YMLUI.Infrastructure;
using IND930YMLUI.View;
using MT.Singularity;
using MT.Singularity.Composition;
using MT.Singularity.Data;
using MT.Singularity.Platform.Devices.Scale;
using MT.Singularity.Platform.Infrastructure;
using MT.Singularity.Platform.Translation;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Presentation.Drawing;
using MT.Singularity.Presentation.Model;


namespace IND930YMLUI.ViewModels
{
    public class WeighLaundryViewModel : ObservableObject
    {
        private RegularLaundryModel _regularLaundry;
        private IScaleService _scaleService = ApplicationBootstrapperBase.CompositionContainer.Resolve<IScaleService>();
        private INavigationPage _parent;
        private IWeighingEventProvider _weighingProvider = ApplicationBootstrapperBase.CompositionContainer.Resolve<IWeighingEventProvider>();
        private double _measuredNetWeight = 0;
        public WeighLaundryViewModel(INavigationPage parent, RegularLaundryModel regularLaundry)
        {
            _parent = parent;
            _regularLaundry = regularLaundry;
            _canDoWeighingColor = Colors.RedBrush;

        }

        public string ImagePath { get; set; } = $@"{Globals.GetDataDirectory()}\Images\full_scale_ready_combo.png";

        private async void GetNetWeight()
        {
            _measuredNetWeight = (await _scaleService.SelectedScale.GetStableWeightAsync(UnitType.Display)).NetWeight;
            _doWeighingCommand?.NotifyCanExecuteChanged();
        }

        private void PopulateProperties()
        {
            SelectedDebitor = _regularLaundry.SelectedDebitor?.DebitorName;
            SelectedContainer = _regularLaundry.SelectedContainer?.NameInGrid;
            SelectedArticle = _regularLaundry.SelectedArticle?.Quantity > 0 ? $"{_regularLaundry.SelectedArticle?.ArticleName} ({_regularLaundry.SelectedArticle?.Quantity})" : _regularLaundry.SelectedArticle?.ArticleName;
        }

        public void SetProcessStep()
        {
            PopulateProperties();

            SelectedLaundryInternColor = Colors.LightGrayBrush;
            SelectedLaundryExternColor = Colors.LightGrayBrush;

            _regularLaundry.CurrentProcessStep = RegularLaundryProcessStep.DoWeighing;
            _doWeighingCommand?.NotifyCanExecuteChanged();
            GetNetWeight();
        }

        public void RegisterEventsForViewModel()
        {
            _scaleService.SelectedScale.NewChangedWeightInDisplayUnit += SelectedScaleOnNewChangedWeightInDisplayUnit;
        }

        public void UnregisterEventsForViewModel()
        {
            _scaleService.SelectedScale.NewChangedWeightInDisplayUnit -= SelectedScaleOnNewChangedWeightInDisplayUnit;
        }

        private void SelectedScaleOnNewChangedWeightInDisplayUnit(WeightInformation weight)
        {
            _measuredNetWeight = weight.NetWeight;
            _doWeighingCommand?.NotifyCanExecuteChanged();
        }

        private string _selectedDebitor;

        public string SelectedDebitor
        {
            get { return _selectedDebitor; }
            set
            {
                if (_selectedDebitor != value)
                {
                    _selectedDebitor = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private string _selectedContainer;

        public string SelectedContainer
        {
            get { return _selectedContainer; }
            set
            {
                if (_selectedContainer != value)
                {
                    _selectedContainer = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private string _selectedArticle;

        public string SelectedArticle
        {
            get { return _selectedArticle; }
            set
            {
                if (_selectedArticle != value)
                {
                    _selectedArticle = value;
                    NotifyPropertyChanged();
                }
            }
        }


        private SolidColorBrush _selectedLaundryInternColor;

        public SolidColorBrush SelectedLaundryInternColor
        {
            get { return _selectedLaundryInternColor; }
            set
            {
                if (_selectedLaundryInternColor != value)
                {
                    _selectedLaundryInternColor = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private SolidColorBrush _selectedLaundryExternColor;

        public SolidColorBrush SelectedLaundryExternColor
        {
            get { return _selectedLaundryExternColor; }
            set
            {
                if (_selectedLaundryExternColor != value)
                {
                    _selectedLaundryExternColor = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private SolidColorBrush _canDoWeighingColor;

        public SolidColorBrush CanDoWeighingColor
        {
            get { return _canDoWeighingColor; }
            set
            {
                if (_canDoWeighingColor != value)
                {
                    _canDoWeighingColor = value;
                    NotifyPropertyChanged();
                }
            }
        }





        private DelegateCommand _doWeighingCommand;
        public ICommand DoWeighingCommand => _doWeighingCommand = new DelegateCommand(DoWeighingCommandExecute, CanDoWeighingCommandExecute);

        private bool CanDoWeighingCommandExecute()
        {
            bool canExecute = _measuredNetWeight > 0;

            CanDoWeighingColor = canExecute ? Colors.LightBlueBrush : Colors.RedBrush;

            return canExecute;
        }

        private async void DoWeighingCommandExecute()
        {

            var weight = await _scaleService.SelectedScale.GetStableWeightAsync(UnitType.Display);
            _regularLaundry.MeasuredNetWeight = weight.NetWeight;
            _regularLaundry.Unit = weight.Unit.ToTranslatedAbbreviation();
            var success = SaveMeasureToFile();

            if (success)
            {
                MessageBox.Show((Visual)_parent, "Speichern Erfolgreich", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            _weighingProvider.TriggerWeighingEvent(this, new WeighingEventArgs(weight));
            _parent.NavigationService.Back();
        }

        private bool SaveMeasureToFile()
        {
            var measurement = CreateMeasurementModel();

            var dataaccess = ApplicationBootstrapperBase.CompositionContainer.Resolve<IGenericCsvAccess<MeasurementModel>>();
            dataaccess.FileMode = FileMode.Append;
            dataaccess.HasHeaderRecord = false;
            var result = dataaccess.SaveDataEntry(measurement);

            return result;
        }

        private MeasurementModel CreateMeasurementModel()
        {
            var measurement = new MeasurementModel();
            measurement.NetWeight = $"{_regularLaundry.MeasuredNetWeight:F2}";
            measurement.DebitorNumber = _regularLaundry.SelectedDebitor.DebitorNumber;
            measurement.ArticleNumber = _regularLaundry.SelectedArticle.ArticleNumber;
            measurement.Date = DateTime.Now.ToShortDateString();
            //measurement.GrossWeight = _regularLaundry.MeasuredNetWeight + _regularLaundry.SelectedContainer.TareWeight;
            measurement.Id = "";
            return measurement;
        }

    }
}
