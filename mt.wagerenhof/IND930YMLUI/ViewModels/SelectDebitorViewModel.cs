﻿using System;

using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using DataAccess;
using DevicesLibrary;
using DevicesLibrary.BarcodeReader;
using IND930UI.Library.Enum;
using IND930UI.Library.Models;
using IND930YMLUI.Config;
using MT.Singularity.Composition;
using MT.Singularity.Data;
using MT.Singularity.Logging;
using MT.Singularity.Platform.Infrastructure;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Model;

namespace IND930YMLUI.ViewModels
{
    public class SelectDebitorViewModel : ObservableObject
    {
        private IBarcodeReader _barcodeReader;
        private ObservableCollection<DebitorModel> _unmodifiedList;
        private Visual _parent;
        private RegularLaundryModel _regularLaundry;
        private IComponents _components = ApplicationBootstrapperBase.CompositionContainer.Resolve<IComponents>();
        private Configuration _configuration;
        private string _favorit = "";

        public SelectDebitorViewModel(Visual parent, RegularLaundryModel regularLaundry)
        {
            
            _regularLaundry = regularLaundry;
            KeyboardVisibility = Visibility.Collapsed;
            _parent = parent;
            _components.GetConfigurationAsync().ContinueWith(RegisterEvnts);

        }

        private void RegisterEvnts(Task<Configuration> obj)
        {
            if (obj.IsCompleted)
            {
                _configuration = obj.Result;
                _favorit = _configuration.FavoriteCustomerNumber;
                _configuration.PropertyChanged += ConfigurationOnPropertyChanged;
                
            }
        }

        private void ConfigurationOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "FavoriteCustomerNumber")
            {
                _favorit = _configuration.FavoriteCustomerNumber;
            }
        }

        public void SetProcessStep()
        {
            PopulateGrid();
            _regularLaundry.CurrentProcessStep = RegularLaundryProcessStep.SelectDebitor;
        }

        public void RegisterBarcodeReader()
        {
            bool success = ApplicationBootstrapperBase.CompositionContainer.TryResolve(out _barcodeReader);
            if (success)
            {
                Log4NetManager.ApplicationLogger.Info($"barcode reader on viewmodel ready");
            }
            else
            {
                Log4NetManager.ApplicationLogger.Error($"barcode reader on viewmodel NULL");
            }
        }

        public void RegisterEventsForViewModel()
        {
            if (_barcodeReader != null)
            {
                _barcodeReader.DataReceivedEvent += BarcodeReaderOnDataReceivedEvent;
            }
        }
        public void UnregisterEventsForViewModel()
        {
            if (_barcodeReader != null)
            {
                _barcodeReader.DataReceivedEvent -= BarcodeReaderOnDataReceivedEvent;
            }
        }

        private void BarcodeReaderOnDataReceivedEvent(object sender, DataReceivedEventArgs e)
        {
            Log4NetManager.ApplicationLogger.Info($"barcode reader on viewmodel data received{e.Data}");
            SearchText = e.Data;
        }

        private void PopulateGrid()
        {
            List<DebitorModel> debitorModels = null;
            try
            {
                var csvDebitorAccess = ApplicationBootstrapperBase.CompositionContainer.Resolve<IGenericCsvAccess<DebitorModel>>();
                debitorModels = csvDebitorAccess.GetAllDataEntries().OrderBy(model => model.DebitorName).ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);

            }

            if (debitorModels?.Count > 0)
            {
                DebitorList.Clear();
                debitorModels.ForEach(model => DebitorList.Add(model));
                _unmodifiedList = new ObservableCollection<DebitorModel>(DebitorList);
                DebitorListIndex = 0;
                SelectedDebitor = DebitorList[DebitorListIndex];
            }
            else
            {
                MessageBox.Show(_parent, "Es wurden keine Daten gefunden\nBitte kontaktieren Sie Ihren Administrator", "Achtung", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

        }

        private string _searchText;

        public string SearchText
        {
            get { return _searchText; }
            set
            {
                if (_searchText != value)
                {
                    _searchText = value;
                    NotifyPropertyChanged();
                    FilterList(_searchText);
                }
            }
        }

        private void FilterList(string searchText)
        {
            var resultList = _unmodifiedList.Where(model => model.NameInGrid.ToLowerInvariant().Contains(searchText.ToLowerInvariant())).ToList();
            DebitorList.Clear();
            resultList.ForEach(model => DebitorList.Add(model));

            if (resultList.Count == 1)
            {
                SelectedDebitor = resultList[0];
                DebitorListIndex = 0;
            }
        }

        private Visibility _keyboardVisibility;

        public Visibility KeyboardVisibility
        {
            get { return _keyboardVisibility; }
            set
            {
                if (_keyboardVisibility != value)
                {
                    _keyboardVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }



        private object _selectedDebitor;

        public object SelectedDebitor
        {
            get { return _selectedDebitor; }
            set
            {
                if (_selectedDebitor != value)
                {
                    _selectedDebitor = value;
                    NotifyPropertyChanged();
                    _confirmSelectionCommand?.NotifyCanExecuteChanged();
                }
            }
        }

        private int _debitorListIndex;

        public int DebitorListIndex
        {
            get { return _debitorListIndex; }
            set
            {
                if (_debitorListIndex != value)
                {
                    _debitorListIndex = value;
                    NotifyPropertyChanged();
                    _scrollDownCommand?.NotifyCanExecuteChanged();
                    _scrollUpCommand?.NotifyCanExecuteChanged();
                }
            }
        }

        private ObservableCollection<DebitorModel> _debitorList = new ObservableCollection<DebitorModel>();

        public ObservableCollection<DebitorModel> DebitorList
        {
            get { return _debitorList; }
            set
            {
                if (_debitorList != value)
                {
                    _debitorList = value;
                    NotifyPropertyChanged();
                }
            }
        }


        private DelegateCommand _scrollDownCommand;
        public ICommand ScrollDownCommand => _scrollDownCommand = new DelegateCommand(ScrollDownCommandExecute, CanScrollDownCommandExecute);

        private bool CanScrollDownCommandExecute()
        {
            if (DebitorList.Count < 10 || DebitorListIndex == DebitorList.Count - 1)
            {
                return false;
            }
            else return true;
        }

        private void ScrollDownCommandExecute()
        {
            if (DebitorListIndex + 5 >= DebitorList.Count)
            {
                DebitorListIndex = DebitorList.Count - 1;
            }
            else
            {
                DebitorListIndex = DebitorListIndex + 5;
            }
        }

        private DelegateCommand _scrollUpCommand;

        public ICommand ScrollUpCommand => _scrollUpCommand = new DelegateCommand(ScrollUpCommandExecute, CanScrollUpCommandExecute);
        private bool CanScrollUpCommandExecute()
        {
            if (DebitorList.Count < 10 || DebitorListIndex <= 0)
            {
                return false;
            }
            else return true;
        }

        private void ScrollUpCommandExecute()
        {
            if (DebitorListIndex <= 5)
            {
                DebitorListIndex = 0;
            }
            else
            {
                DebitorListIndex = DebitorListIndex - 5;
            }
        }

        private DelegateCommand _confirmSelectionCommand;
        public ICommand ConfirmSelectionCommand => _confirmSelectionCommand = new DelegateCommand(ConfirmSelectionCommandExecute, CanConfirmSelectionCommandExecute);
        private bool CanConfirmSelectionCommandExecute()
        {
            return SelectedDebitor != null;
        }

        private void ConfirmSelectionCommandExecute()
        {
            _regularLaundry.SelectedDebitor = (DebitorModel) SelectedDebitor;
        }

        public ICommand SelectFavoritDebitorCommand => new DelegateCommand(SelectFavoritDebitorCommandExecute);

        private void SelectFavoritDebitorCommandExecute()
        {
            SearchText = _favorit;
        }

        public void ClearSearchText()
        {
            SearchText = string.Empty;
        }
    }
}
