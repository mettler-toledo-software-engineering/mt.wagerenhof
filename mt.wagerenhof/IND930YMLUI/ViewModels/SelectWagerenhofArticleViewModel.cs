﻿using System;

using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Text;
using IND930UI.Library.Models;
using MT.Singularity.Data;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Presentation.Model;

namespace IND930YMLUI.ViewModels
{
    public class SelectWagerenhofArticleViewModel : ObservableObject
    {
        private RegularLaundryModel _regularLaundry;
        public SelectWagerenhofArticleViewModel(RegularLaundryModel regularLaundry)
        {
            _regularLaundry = regularLaundry;
            FavoriteArticles = new ObservableCollection<FavoriteArticle>(FavoriteData.FavoriteArticles);
        }

        public INavigationPage Parent { get; set; }

        private ObservableCollection<FavoriteArticle> _favoriteArticles;

        public ObservableCollection<FavoriteArticle> FavoriteArticles
        {
            get { return _favoriteArticles; }
            set
            {
                if (_favoriteArticles != value)
                {
                    _favoriteArticles = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private int _selectedQuantity;

        public int SelectedQuantity
        {
            get { return _selectedQuantity; }
            set
            {
                if (_selectedQuantity != value)
                {
                    _selectedQuantity = value;
                    NotifyPropertyChanged();
                }
            }
        }


        public ICommand SelectArticleCommand => new DelegateCommand<FavoriteArticle>(SelectArticleCommandExeceute);

        private void SelectArticleCommandExeceute(FavoriteArticle selectedArticle)
        {
            _regularLaundry.SelectedArticle = selectedArticle;
            _regularLaundry.SelectedArticle.Quantity = SelectedQuantity;

            Parent.NavigationService.Back();
        }
    }
}
