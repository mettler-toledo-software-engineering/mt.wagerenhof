﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using IND930UI.Library.Models;
using IND930YMLUI.View;
using MT.Singularity.Data;
using MT.Singularity.Composition;
using MT.Singularity.Platform.Devices.Scale;
using MT.Singularity.Platform.Infrastructure;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Presentation.Model;
using System.Threading.Tasks;
using DataAccess;
using IND930YMLUI.Infrastructure;
using MT.Singularity.Presentation.Drawing;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Platform.UserManagement;
using MT.Singularity.Platform.Configuration;

namespace IND930YMLUI.ViewModels
{
    public class CompanyWeighingSummaryViewModel : ObservableObject
    {
        private INavigationPage _parent;
        private CompanyLaundryProcessModel _companyLaundryProcess;
        private ISecurityService _securityService;
        private bool _permissionToDeleteContainer;
        private IScaleService _scaleService = ApplicationBootstrapperBase.CompositionContainer.Resolve<IScaleService>();

        public CompanyWeighingSummaryViewModel(INavigationPage parent, CompanyLaundryProcessModel companyLaundryProcess)
        {
            _parent = parent;
            _companyLaundryProcess = companyLaundryProcess;
            _securityService = ApplicationBootstrapperBase.CompositionContainer.Resolve<ISecurityService>();
            CompanyLaundryList = new ObservableCollection<CompanyLaundryModel>();
            SelectContainerView = new SelectContainerView(_companyLaundryProcess);
            CompanyWeighingView = new CompanyWeighingView(_companyLaundryProcess);
            
            SelectArticleView = new SelectArticleView(_companyLaundryProcess);
        }

        private void SecurityServiceOnCurrentUserChanged(User user)
        {
            _permissionToDeleteContainer = Permissions.IsSupervisorOrHigher(user.Permission);
            _deleteSelectedContainerCommand?.NotifyCanExecuteChanged();
            
        }

        public string ArticleImage => $@"{Globals.GetDataDirectory()}\Images\article_small.png";
        public string ContainerImage => $@"{Globals.GetDataDirectory()}\Images\basket_small.png";

        public void SetProcessStep()
        {
        }
        public void RegisterEventsForViewModel()
        {
            _securityService.CurrentUserChanged += SecurityServiceOnCurrentUserChanged;
        }
        public void UnregisterEventsForViewModel()
        {
            _scaleService.SelectedScale.ClearTareAsync();
            _securityService.CurrentUserChanged -= SecurityServiceOnCurrentUserChanged;
        }

        public void PopulateProperties()
        {
            CompanyLaundryList = new ObservableCollection<CompanyLaundryModel>(_companyLaundryProcess.CompanyLaundryList);
            //SelectedCompanyLaundry = _companyLaundryProcess?.SelectedCompanyLaundry;
            CompanyLaundryListIndex = CompanyLaundryList.IndexOf(_companyLaundryProcess?.SelectedCompanyLaundry);
            _weighContainerCommand?.NotifyCanExecuteChanged();
            _finishSelectedContainerCommand?.NotifyCanExecuteChanged();
            _goToSelectArticleCommand?.NotifyCanExecuteChanged();
            _deleteSelectedContainerCommand?.NotifyCanExecuteChanged();
            TareScale();
        }
        private SolidColorBrush _weighContainerButtonColor;

        public SolidColorBrush WeighContainerButtonColor
        {
            get { return _weighContainerButtonColor; }
            set
            {
                if (_weighContainerButtonColor != value)
                {
                    _weighContainerButtonColor = value;
                    NotifyPropertyChanged();
                }

            }
        }

        private SolidColorBrush _finishSelectedContainerButtonColor;

        public SolidColorBrush FinishSelectedContainerButtonColor
        {
            get { return _finishSelectedContainerButtonColor; }
            set
            {
                if (_finishSelectedContainerButtonColor != value)
                {
                    _finishSelectedContainerButtonColor = value;
                    NotifyPropertyChanged();
                }
            }
        }


        private ObservableCollection<CompanyLaundryModel> _companyLaundryList;

        public ObservableCollection<CompanyLaundryModel> CompanyLaundryList
        {
            get { return _companyLaundryList; }
            set
            {
                if (_companyLaundryList != value)
                {
                    _companyLaundryList = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private int _companyLaundryListIndex;

        public int CompanyLaundryListIndex
        {
            get { return _companyLaundryListIndex; }
            set
            {
                if (_companyLaundryListIndex != value)
                {
                    _companyLaundryListIndex = value;
                    NotifyPropertyChanged();
                    _scrollDownCommand?.NotifyCanExecuteChanged();
                    _scrollUpCommand?.NotifyCanExecuteChanged();
                }
            }
        }

        private object _selectedCompanyLaundry;

        public object SelectedCompanyLaundry
        {
            get { return _selectedCompanyLaundry; }
            set
            {
                if (_selectedCompanyLaundry != value)
                {
                    _selectedCompanyLaundry = value;
                    NotifyPropertyChanged();
                    _weighContainerCommand?.NotifyCanExecuteChanged();
                    _finishSelectedContainerCommand?.NotifyCanExecuteChanged();
                    _goToSelectArticleCommand?.NotifyCanExecuteChanged();
                    _deleteSelectedContainerCommand?.NotifyCanExecuteChanged();
                }
            }
        }

        private DelegateCommand _scrollDownCommand;
        public ICommand ScrollDownCommand => _scrollDownCommand = new DelegateCommand(ScrollDownCommandExecute, CanScrollDownCommandExecute);

        private bool CanScrollDownCommandExecute()
        {
            if (CompanyLaundryList.Count < 10 || CompanyLaundryListIndex == CompanyLaundryList.Count - 1)
            {
                return false;
            }
            else return true;
        }

        private void ScrollDownCommandExecute()
        {
            if (CompanyLaundryListIndex + 5 >= CompanyLaundryList.Count)
            {
                CompanyLaundryListIndex = CompanyLaundryList.Count - 1;
            }
            else
            {
                CompanyLaundryListIndex = CompanyLaundryListIndex + 5;
            }
        }

        private DelegateCommand _scrollUpCommand;

        public ICommand ScrollUpCommand => _scrollUpCommand = new DelegateCommand(ScrollUpCommandExecute, CanScrollUpCommandExecute);
        private bool CanScrollUpCommandExecute()
        {
            if (CompanyLaundryList.Count < 10 || CompanyLaundryListIndex <= 0)
            {
                return false;
            }
            return true;
        }

        private void ScrollUpCommandExecute()
        {
            if (CompanyLaundryListIndex <= 5)
            {
                CompanyLaundryListIndex = 0;
            }
            else
            {
                CompanyLaundryListIndex = CompanyLaundryListIndex - 5;
            }
        }

        private SelectContainerView _selectContainerView;

        public SelectContainerView SelectContainerView
        {
            get { return _selectContainerView; }
            set
            {
                if (_selectContainerView != value)
                {
                    _selectContainerView = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private CompanyWeighingView _companyWeighingView;

        public CompanyWeighingView CompanyWeighingView
        {
            get { return _companyWeighingView; }
            set
            {
                if (_companyWeighingView != value)
                {
                    _companyWeighingView = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private SelectArticleView _selectArticleView;

        public SelectArticleView SelectArticleView
        {
            get { return _selectArticleView; }
            set
            {
                if (_selectArticleView != value)
                {
                    _selectArticleView = value;
                    NotifyPropertyChanged();
                }
            }
        }


        public ICommand GoToSelectContainerCommand => new DelegateCommand(GoToSelectContainerCommandExecute);

        private void GoToSelectContainerCommandExecute()
        {
            _parent.NavigationService.NavigateTo(SelectContainerView);
        }
        private DelegateCommand _finishSelectedContainerCommand;
        public ICommand FinishSelectedContainerCommand => _finishSelectedContainerCommand = new DelegateCommand(FinishSelectedContainerCommandExecute, CanFinishSelectedContainerCommandExecute);

        private bool CanFinishSelectedContainerCommandExecute()
        {
            bool containerSelected = ((CompanyLaundryModel) SelectedCompanyLaundry)?.SelectedArticle != null && ((CompanyLaundryModel)SelectedCompanyLaundry)?.TotalDistributedNetWeight > 0;
            FinishSelectedContainerButtonColor = containerSelected ? Colors.LightBlueBrush : Colors.LightGrayBrush;
            return containerSelected;
        }

        private void FinishSelectedContainerCommandExecute()
        {
            MessageBox.Show((Visual) _parent, "Wollen Sie die Erfassung für diesen Container und Artikel beenden?", "Achtung", MessageBoxButtons.YesNo, MessageBoxIcon.Question, result =>
            {
                if (result == DialogResult.Yes)
                {
                    var laundryModel = (CompanyLaundryModel) SelectedCompanyLaundry;
                    _companyLaundryProcess.CompanyLaundryList.Remove(laundryModel);
                    CompanyLaundryList.Remove(laundryModel);
                    var measurement = CreateMeasurementModel(laundryModel);

                    var dataaccess = ApplicationBootstrapperBase.CompositionContainer.Resolve<IGenericCsvAccess<MeasurementModel>>();
                    dataaccess.FileMode = FileMode.Append;
                    dataaccess.HasHeaderRecord = false;
                    var success = dataaccess.SaveDataEntry(measurement);

                    CompanyLaundryListIndex = 0;
                    if (CompanyLaundryList.Count == 0)
                    {
                        _scaleService.SelectedScale.ClearTareAsync();
                    }
                }

            });
        }

        private DelegateCommand _deleteSelectedContainerCommand;
        public ICommand DeleteSelectedContainerCommand => _deleteSelectedContainerCommand = new DelegateCommand(DeleteSelectedContainerCommandExecute, CanDeleteSelectedContainerCommandExecute);

        private bool CanDeleteSelectedContainerCommandExecute()
        {
            bool canDelete = ((CompanyLaundryModel)SelectedCompanyLaundry) != null && _permissionToDeleteContainer;
            return canDelete;
        }

        private void DeleteSelectedContainerCommandExecute()
        {
            MessageBox.Show((Visual)_parent, "Wollen Sie die ausgewählten Container, ohne zu speichern,\naus der Erfassung entfernen?", "Achtung", MessageBoxButtons.YesNo, MessageBoxIcon.Question, result =>
            {
                if (result == DialogResult.Yes)
                {
                    var laundryModel = (CompanyLaundryModel)SelectedCompanyLaundry;
                    _companyLaundryProcess.CompanyLaundryList.Remove(laundryModel);
                    CompanyLaundryList.Remove(laundryModel);

                    CompanyLaundryListIndex = 0;
                    if (CompanyLaundryList.Count == 0)
                    {
                        _scaleService.SelectedScale.ClearTareAsync();
                    }
                }
            });
        }

        private MeasurementModel CreateMeasurementModel(CompanyLaundryModel selectedCompanyLaundry)
        {
            var measurement = new MeasurementModel();
            measurement.NetWeight = $"{selectedCompanyLaundry.TotalDistributedNetWeight:F2}";
            measurement.DebitorNumber = selectedCompanyLaundry.DefaultDebitor.DebitorNumber;
            measurement.ArticleNumber = selectedCompanyLaundry.SelectedArticle.ArticleNumber;
            measurement.Date = DateTime.Now.ToShortDateString();
            //measurement.GrossWeight = selectedCompanyLaundry.TotalDistributedNetWeight + selectedCompanyLaundry.SelectedContainer.TareWeight;
            measurement.Id = "";
            return measurement;
        }

        private DelegateCommand _weighContainerCommand;
        public ICommand WeighContainerCommand => _weighContainerCommand = new DelegateCommand(WeighContainerCommandExecute, CanWeighContainerCommandExecute);

        private bool CanWeighContainerCommandExecute()
        {
            var laundryModel = (CompanyLaundryModel) SelectedCompanyLaundry;
            bool containerSelected = laundryModel?.SelectedContainer != null && laundryModel.SelectedArticle != null;
            if (containerSelected)
            {
                WeighContainerButtonColor = Colors.LightBlueBrush;
                TareScale();
                return true;
            }
            WeighContainerButtonColor = Colors.RedBrush;
            return false;
        }

        private void WeighContainerCommandExecute()
        {
            _companyLaundryProcess.SelectedCompanyLaundry = (CompanyLaundryModel)SelectedCompanyLaundry;
            _parent.NavigationService.NavigateTo(CompanyWeighingView);
        }

        private async Task TareScale()
        {
            if (SelectedCompanyLaundry != null)
            {
                var laundry = (CompanyLaundryModel)SelectedCompanyLaundry;

                await _scaleService.SelectedScale.PresetTareAsync(laundry.SelectedContainer.TareWeightAsString, MT.Singularity.WellknownWeightUnit.Kilogram);
            }
            else
            {
                await _scaleService.SelectedScale.PresetTareAsync(_companyLaundryProcess.SelectedCompanyLaundry.SelectedContainer.TareWeightAsString, MT.Singularity.WellknownWeightUnit.Kilogram);

            }
        }

        private DelegateCommand _goToSelectArticleCommand;
        public ICommand GoToSelectArticleCommand => _goToSelectArticleCommand = new DelegateCommand(GoToSelectArticleCommandExecute, CanGoToSelectArticleCommand);

        private bool CanGoToSelectArticleCommand()
        {
            bool canExecute = ((CompanyLaundryModel) SelectedCompanyLaundry)?.SelectedContainer != null;

            return canExecute;
        }

        private void GoToSelectArticleCommandExecute()
        {
            _companyLaundryProcess.SelectedCompanyLaundry = (CompanyLaundryModel)SelectedCompanyLaundry;

            if (_companyLaundryProcess.SelectedCompanyLaundry.SelectedArticle != null)
            {
                MessageBox.Show((Visual) _parent, "Für den ausgewählten Container wurde bereits ein Artikel erfasst.\nWollen Sie alle Messungen verwerfen und einen neuen Artikel auswählen?", "Achtung", MessageBoxButtons.YesNo, MessageBoxIcon.Question, result =>
                {
                    if (result == DialogResult.Yes)
                    {
                        _companyLaundryProcess.SelectedCompanyLaundry.TotalDistributedNetWeight = 0;
                        _companyLaundryProcess.SelectedCompanyLaundry.LastNetWeight = 0;
                        _companyLaundryProcess.SelectedCompanyLaundry.CurrentNetWeight = 0;
                        _parent.NavigationService.NavigateTo(SelectArticleView);
                    }
                });
            }
            else
            {
                _parent.NavigationService.NavigateTo(SelectArticleView);
            }

            
        }
    }
}
