﻿using System;
using System.ComponentModel;
using IND930UI.Library.Enum;
using IND930UI.Library.Models;
using IND930YMLUI.Config;
using IND930YMLUI.Infrastructure;
using IND930YMLUI.View;
using MT.Singularity.Composition;
using MT.Singularity.Data;
using MT.Singularity.Platform.Configuration;
using MT.Singularity.Platform.Infrastructure;
using MT.Singularity.Platform.UserManagement;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Presentation.Model;
using MT.Singularity.Platform.Devices.Scale;


namespace IND930YMLUI.ViewModels
{
    public class IdleScreenViewModel : ObservableObject
    {
        private ISecurityService _securityService;
        private bool _permissionToChangeSettings;
        private RegularLaundryModel _regularLaundry = new RegularLaundryModel();
        private CompanyLaundryProcessModel _companyLaundryProcess = new CompanyLaundryProcessModel();
        private Configuration _configuration;
        private INavigationPage _parent;

        private static readonly string CleanLaundryImage = $@"{Globals.GetDataDirectory()}\Images\basket_big.png";
        private static readonly string DirtyLaundryImage = $@"{Globals.GetDataDirectory()}\Images\dirty_laundry_big.png";
        private static readonly string companyLaundryImage = $@"{Globals.GetDataDirectory()}\Images\article_big.png";

        public IdleScreenViewModel(INavigationPage parent)
        {
            _securityService = ApplicationBootstrapperBase.CompositionContainer.Resolve<ISecurityService>();
            _permissionToChangeSettings = Permissions.IsSupervisorOrHigher(_securityService.CurrentPermission);
            SettingsButtonVisibility = _permissionToChangeSettings ? Visibility.Visible : Visibility.Collapsed;
            CompanyWeighingSummaryView = new CompanyWeighingSummaryView(_companyLaundryProcess);
            
            _parent = parent;
            InitializeViewModel();
        }

        public async void InitializeViewModel()
        {
            var customerComponent = ApplicationBootstrapperBase.CompositionContainer.Resolve<IComponents>();
            _configuration = await customerComponent.GetConfigurationAsync();
            var scaleService = ApplicationBootstrapperBase.CompositionContainer.Resolve<IScaleService>();
            await scaleService.SelectedScale.ClearTareAsync();
            CleanLaundryVisibility = _configuration.SelectedWorkingEnvironment == WorkingEnvironment.CleanLaundry.ToFriendlyName() ? Visibility.Visible : Visibility.Hidden;
            LaundryImage = _configuration.SelectedWorkingEnvironment == WorkingEnvironment.CleanLaundry.ToFriendlyName() ? CleanLaundryImage : DirtyLaundryImage;
            CompanyLaundryImage = companyLaundryImage;
            _configuration.PropertyChanged += ConfigurationOnPropertyChanged;
        }

        public void SetProcessStep()
        {
            _regularLaundry.CurrentProcessStep = RegularLaundryProcessStep.Idle;
        }

        public void RegisterEventsForViewModel()
        {
            _securityService.CurrentUserChanged += SecurityServiceOnCurrentUserChanged;
            
        }

        private void ConfigurationOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "SelectedWorkingEnvironment")
            {
                if (_configuration.SelectedWorkingEnvironment == WorkingEnvironment.CleanLaundry.ToFriendlyName())
                {
                    SetupCleanLaundryElements();
                }
                if (_configuration.SelectedWorkingEnvironment == WorkingEnvironment.DirtyLaundry.ToFriendlyName())
                {
                    SetupDirtyLaundryElements();
                }
            }
        }

        private void SetupCleanLaundryElements()
        {
            CleanLaundryVisibility = Visibility.Visible;
            DirtyLaundryVisibility = Visibility.Hidden;
            LaundryImage = CleanLaundryImage;
        }

        private void SetupDirtyLaundryElements()
        {
            CleanLaundryVisibility = Visibility.Hidden;
            DirtyLaundryVisibility = Visibility.Visible;
            LaundryImage = DirtyLaundryImage;
        }

        private string _laundryImage;

        public string LaundryImage
        {
            get { return _laundryImage; }
            set
            {
                if (_laundryImage != value)
                {
                    _laundryImage = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private string _companyLaundryImage;

        public string CompanyLaundryImage
        {
            get { return _companyLaundryImage; }
            set
            {
                if (_companyLaundryImage != value)
                {
                    _companyLaundryImage = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private Visibility _cleanLaundryVisibility;

        public Visibility CleanLaundryVisibility
        {
            get { return _cleanLaundryVisibility; }
            set
            {
                if (_cleanLaundryVisibility != value)
                {
                    _cleanLaundryVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private Visibility _dirtyLaundryVisibility;

        public Visibility DirtyLaundryVisibility
        {
            get { return _dirtyLaundryVisibility; }
            set
            {
                if (_dirtyLaundryVisibility != value)
                {
                    _dirtyLaundryVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }


        public void UnregisterEventsForViewModel()
        {
            _securityService.CurrentUserChanged -= SecurityServiceOnCurrentUserChanged;
            
        }

        private void SecurityServiceOnCurrentUserChanged(User user)
        {
            _permissionToChangeSettings = Permissions.IsSupervisorOrHigher(user.Permission);

            SettingsButtonVisibility = _permissionToChangeSettings ? Visibility.Visible : Visibility.Collapsed;
        }

        private Visibility _settingsButtonVisibility;

        public Visibility SettingsButtonVisibility
        {
            get { return _settingsButtonVisibility; }
            set
            {
                if (_settingsButtonVisibility != value)
                {
                    _settingsButtonVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }


        private SettingsView _settingsView = new SettingsView();

        public SettingsView SettingsView
        {
            get { return _settingsView; }
            set
            {
                if (_settingsView != value)
                {
                    _settingsView = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private RegularWeighingSummaryView _regularWeighingSummaryView;

        public RegularWeighingSummaryView RegularWeighingSummaryView
        {
            get { return _regularWeighingSummaryView; }
            set
            {
                if (_regularWeighingSummaryView != value)
                {
                    _regularWeighingSummaryView = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public ICommand GoToRegularWeighingSummaryViewCommand => new DelegateCommand(GoToRegularWeighingSummaryViewCommandExecute);

        private void GoToRegularWeighingSummaryViewCommandExecute()
        {
            RegularWeighingSummaryView = new RegularWeighingSummaryView(new RegularLaundryModel());
            _parent.NavigationService.NavigateTo(RegularWeighingSummaryView);
        }

        private CompanyWeighingSummaryView _companyWeighingSummaryView;

        public CompanyWeighingSummaryView CompanyWeighingSummaryView 
        {
            get { return _companyWeighingSummaryView; }
            set
            {
                if (_companyWeighingSummaryView != value)
                {
                    _companyWeighingSummaryView = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public ICommand GoToCompanyWeighingSummaryViewCommand => new DelegateCommand(GoToCompanyWeighingSummaryViewCommandExecute);

        private void GoToCompanyWeighingSummaryViewCommandExecute()
        {
            _parent.NavigationService.NavigateTo(CompanyWeighingSummaryView);
        }
    }
}
