﻿using System;

using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess;
using DevicesLibrary.BarcodeReader;
using IND930UI.Library.Models;
using IND930YMLUI.View;
using MT.Singularity.Composition;

using MT.Singularity.Data;
using MT.Singularity.Logging;
using MT.Singularity.Platform.Devices.Scale;
using MT.Singularity.Platform.Infrastructure;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Model;
using DataReceivedEventArgs = DevicesLibrary.DataReceivedEventArgs;


namespace IND930YMLUI.ViewModels
{
    public class ManageContainerViewModel : ObservableObject
    {

        private IBarcodeReader _barcodeReader;
        private IScaleService _scaleService = ApplicationBootstrapperBase.CompositionContainer.Resolve<IScaleService>();
        private ObservableCollection<ContainerModel> _unmodifiedList;
        private Visual _parent;
        public ManageContainerViewModel(Visual parent)
        {
            
            KeyboardVisibility = Visibility.Collapsed;
            _parent = parent;
            
            PopulateGrid();


        }

        public void RegisterBarcodeReader()
        {
            bool success = ApplicationBootstrapperBase.CompositionContainer.TryResolve(out _barcodeReader);
            if (success)
            {
                Log4NetManager.ApplicationLogger.Info($"barcode reader on viewmodel ready");
            }
            else
            {
                Log4NetManager.ApplicationLogger.Error($"barcode reader on viewmodel NULL");
            }
        }
        public void RegisterEventsForViewModel()
        {
            if (_barcodeReader != null)
            {
                _barcodeReader.DataReceivedEvent += BarcodeReaderOnDataReceivedEvent;
            }
        }

        public void UnregisterEventsForViewModel()
        {
            if (_barcodeReader != null)
            {
                _barcodeReader.DataReceivedEvent -= BarcodeReaderOnDataReceivedEvent;
            }
        }

        private async void BarcodeReaderOnDataReceivedEvent(object sender, DataReceivedEventArgs e)
        {
            Log4NetManager.ApplicationLogger.Info($"barcode reader on viewmodel data received{e.Data}");
            AddContainer(e.Data, await GetNetWeight());
        }

        private async Task<double> GetNetWeight()
        {
            var weight = await _scaleService.SelectedScale.GetStableWeightAsync(UnitType.Display);
            return weight.NetWeight;
        }

        private void PopulateGrid()
        {
            List<ContainerModel> containermodels = null;
            try
            {
                var csvContainerAccess = ApplicationBootstrapperBase.CompositionContainer.Resolve<IGenericCsvAccess<ContainerModel>>();
                containermodels = csvContainerAccess.GetAllDataEntries().OrderBy(model => model.Name).ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                
            }
            
            if (containermodels?.Count > 0)
            {
                ContainerList = new ObservableCollection<ContainerModel>(containermodels);
                _unmodifiedList = new ObservableCollection<ContainerModel>(containermodels);
                ContainerListIndex = 0;
                SelectedContainer = ContainerList[ContainerListIndex];

                //ContainerList.Clear();
                //containermodels.ForEach(model => ContainerList.Add(model));
                //_unmodifiedList = new ObservableCollection<ContainerModel>(ContainerList);
                //ContainerListIndex = 0;
                //SelectedContainer = ContainerList[ContainerListIndex];
            }
            else
            {
                MessageBox.Show(_parent, "Es wurden keine Daten gefunden\nBitte kontaktieren Sie Ihren Administrator", "Achtung", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

        }

        private string _searchText;

        public string SearchText
        {
            get { return _searchText; }
            set
            {
                if (_searchText != value)
                {
                    _searchText = value;
                    NotifyPropertyChanged();
                    FilterList(_searchText);
                }
            }
        }

        private void FilterList(string searchText)
        {
            var resultList = _unmodifiedList.Where(model => model.Name.ToLowerInvariant().Contains(searchText.ToLowerInvariant())).ToList();
            ContainerList.Clear();
            resultList.ForEach(model => ContainerList.Add(model));

            if (resultList.Count == 1)
            {
                SelectedContainer = resultList[0];
                ContainerListIndex = 0;
            }
        }

        private Visibility _keyboardVisibility;

        public Visibility KeyboardVisibility
        {
            get { return _keyboardVisibility; }
            set
            {
                if (_keyboardVisibility != value)
                {
                    _keyboardVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }



        private object _selectedContainer;

        public object SelectedContainer
        {
            get { return _selectedContainer; }
            set
            {
                if (_selectedContainer != value)
                {
                    _selectedContainer = value;
                    
                    NotifyPropertyChanged();
                    _editContainerCommand?.NotifyCanExecuteChanged();
                    _deleteContainerCommand?.NotifyCanExecuteChanged();
                }
            }
        }
        
        private int _containerListIndex;

        public int ContainerListIndex
        {
            get { return _containerListIndex; }
            set
            {
                if (_containerListIndex != value)
                {
                    _containerListIndex = value;
                    NotifyPropertyChanged();
                    _scrollDownCommand?.NotifyCanExecuteChanged();
                    _scrollUpCommand?.NotifyCanExecuteChanged();
                }
            }
        }

        private ObservableCollection<ContainerModel> _containerList = new ObservableCollection<ContainerModel>();

        public ObservableCollection<ContainerModel> ContainerList
        {
            get { return _containerList; }
            set
            {
                if (_containerList != value)
                {
                    _containerList = value;
                    NotifyPropertyChanged();
                }
            }
        }


        private DelegateCommand _scrollDownCommand;
        public ICommand ScrollDownCommand => _scrollDownCommand = new DelegateCommand(ScrollDownCommandExecute, CanScrollDownCommandExecute);

        private bool CanScrollDownCommandExecute()
        {
            if (ContainerList.Count < 10 || ContainerListIndex == ContainerList.Count - 1)
            {
                return false;
            }
            else return true;
        }

        private void ScrollDownCommandExecute()
        {
            if (ContainerListIndex + 5 >= ContainerList.Count)
            {
                ContainerListIndex = ContainerList.Count - 1;
            }
            else
            {
                ContainerListIndex = ContainerListIndex + 5;
            }
        }

        private DelegateCommand _scrollUpCommand;

        public ICommand ScrollUpCommand => _scrollUpCommand = new DelegateCommand(ScrollUpCommandExecute, CanScrollUpCommandExecute);
        private bool CanScrollUpCommandExecute()
        {
            if (ContainerList.Count < 10 || ContainerListIndex <= 0)
            {
                return false;
            }
            else return true;
        }

        private void ScrollUpCommandExecute()
        {
            if (ContainerListIndex <= 5)
            {
                ContainerListIndex = 0;
            }
            else
            {
                ContainerListIndex = ContainerListIndex - 5;
            }
        }

        public ICommand AddContainerCommand =>  new DelegateCommand(AddContainerCommandExecute);
        
        private async void AddContainerCommandExecute()
        {
            AddContainer("", await GetNetWeight());
        }

        private async void AddContainer(string containerName, double containerWeight)
        {
            var view = new AddContainerView(containerName, containerWeight);
            var result = await view.ShowAsync(_parent);

            if (result == DialogResult.OK)
            {
                PopulateGrid();
            }
        }

        private DelegateCommand _editContainerCommand;
        public ICommand EditContainerCommand => _editContainerCommand = new DelegateCommand(EditContainerCommandExecute, CanEditContainerCommandExecute);

        private bool CanEditContainerCommandExecute()
        {
           return SelectedContainer != null;
        }

        private async void EditContainerCommandExecute()
        {
            var container = (ContainerModel) SelectedContainer;
            var view = new AddContainerView(container);
            var result = await view.ShowAsync(_parent);

            if (result == DialogResult.OK)
            {
                PopulateGrid();
            }
        }

        private DelegateCommand _deleteContainerCommand;
        public ICommand DeleteContainerCommand => _deleteContainerCommand = new DelegateCommand(DeleteContainerCommandExecute, CanDeleteContainerCommandExecute);

        private bool CanDeleteContainerCommandExecute()
        {
            return SelectedContainer != null;
        }

        private void DeleteContainerCommandExecute()
        {
            var container = (ContainerModel)SelectedContainer;

            MessageBox.Show(_parent, $"Wollen Sie den Container: {container.Name}\nWirklich löschen?", "Achtung!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, dialogResult =>
            {
                if (dialogResult == DialogResult.Yes)
                {
                    var csvContainerAccess = ApplicationBootstrapperBase.CompositionContainer.Resolve<IGenericCsvAccess<ContainerModel>>();
                    var result = csvContainerAccess.DeleteDataEntry(container.Id);
                    Debug.WriteLine(result);
                    PopulateGrid();
                }
            } );

        }


    }
}
