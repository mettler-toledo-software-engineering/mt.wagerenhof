﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

using System.Threading.Tasks;
using DataAccess;
using DataAccess.Network;
using DevicesLibrary;
using DevicesLibrary.BarcodeReader;
using DevicesLibrary.Connections;
using DevicesLibrary.USB;
using IND930YMLUI.Config;
using IND930UI.Library.Models;
using IND930UI.Library.EventManager;
// ReSharper disable once RedundantUsingDirective

using MT.Singularity.Composition;
using MT.Singularity.Logging;

using MT.Singularity.Platform.CommonUX.Infrastructure;
using MT.Singularity.Platform.Configuration;
using MT.Singularity.Platform.Devices;
using MT.Singularity.Platform.Infrastructure;


namespace IND930YMLUI.Infrastructure
{
    /// <summary>
    /// Initializes and runs the application.
    /// </summary>
    internal class Bootstrapper : ClientBootstrapperBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Bootstrapper"/> class.
        /// </summary>
        public Bootstrapper()
            : base(Assembly.GetExecutingAssembly())
        {
        }

        /// <summary>
        /// Gets a value indicating whether it need to create customer database.
        /// </summary>
        protected override bool NeedToCreateCustomerDatabase
        {
            get { return true; }
        }
        /// <summary>
        /// Initializes the application in this method. This method is called after all
        /// services have been started and before the <see cref="T:MT.Singularity.Platform.Infrastructure.IShell" /> is shown.
        /// </summary>
        protected override async void InitializeApplication()
        {
            base.InitializeApplication();
            await InitializeCustomerService();
        }

        private async Task InitializeCustomerService()
        {
            try
            {
                var engine = CompositionContainer.Resolve<IPlatformEngine>();
                var securityService = await engine.GetSecurityServiceAsync();
                var configurationStore = CompositionContainer.Resolve<IConfigurationStore>();
                var customerComponent = new Components(configurationStore, securityService, CompositionContainer);
                
                CompositionContainer.AddInstance<IComponents>(customerComponent);
                var configuration = await customerComponent.GetConfigurationAsync();

                RegisterClassesToContainer(configuration);
                SetupUsb();
                await SetupBarcodeReader(configuration, engine);


            }
            catch (Exception ex)
            {
                Log4NetManager.ApplicationLogger.Error("Error Bootstrapper", ex);
                //throw;
            }
        }



        private void RegisterClassesToContainer(Configuration configuration)
        {
            INetworkUser user = new NetworkUser(configuration.AdminName, configuration.AdminPassword, configuration.RemoteHost);
            string encoding = "windows-1252";
            string delimiter = ";";

            CompositionContainer.AddInstance<IGenericCsvAccess<ContainerModel>>(new GenericCsvAccess<ContainerModel>(Globals.GetDataDirectory(), "container.csv", delimiter, encoding));
            CompositionContainer.AddInstance<IGenericCsvAccess<ArticleModel>>(new GenericCsvNetworkAccess<ArticleModel>(configuration.DataSource, configuration.CsvArticle, delimiter, user, encoding, Globals.GetDataDirectory()));
            CompositionContainer.AddInstance<IGenericCsvAccess<DebitorModel>>(new GenericCsvNetworkAccess<DebitorModel>(configuration.DataSource, configuration.CsvDebitor, delimiter, user, encoding, Globals.GetDataDirectory()));
            CompositionContainer.AddInstance<IGenericCsvAccess<MeasurementModel>>(new GenericCsvNetworkAccess<MeasurementModel>(configuration.WriteDirectory, configuration.CsvMeasurements, delimiter, user, encoding, Globals.GetDataDirectory()));
            CompositionContainer.AddInstance<IWeighingEventProvider>(new WeighingEventProvider());
        }

        private async Task SetupBarcodeReader(Configuration configuration, IPlatformEngine engine)
        {
            IBarcodeReader barcodereader = null;
            try
            {
                var connection = new SerialDeviceConnection(configuration.BarcodeReaderComPort).GetDeviceConnection();
                barcodereader = new SerialBarcodeReader(connection, EndOfDataCharacter.CR);
                if (connection != null && barcodereader.Initialized)
                {
                    CompositionContainer.AddInstance(barcodereader);
                    Log4NetManager.ApplicationLogger.Info("barcode reader added to composition container");
                }
            }
            catch (Exception ex)
            {
                Log4NetManager.ApplicationLogger.Error("Setup BarcodeReader Failed", ex);
            }

            if (barcodereader == null || barcodereader.Initialized == false)
            {
                try
                {
                    var interfaceservice = await engine.GetInterfaceServiceAsync();
                    var allinterfaces = await interfaceservice.GetAllInterfacesAsync();
                    var allserialinterface = allinterfaces.OfType<ISerialInterface>();
                    var serialinterface = allserialinterface.FirstOrDefault();

                    if (serialinterface != null)
                    {
                        Log4NetManager.ApplicationLogger.Info($"interface: {serialinterface.FriendlyName} with port: {serialinterface.PortName} found");
                        var serialconnection = await serialinterface.CreateConnectionChannelAsync();
                        try
                        {
                            barcodereader = new SerialBarcodeReader(serialconnection);

                            CompositionContainer.AddInstance(barcodereader);
                            Log4NetManager.ApplicationLogger.Info("barcode reader added to composition container");
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e);
                            Log4NetManager.ApplicationLogger.Error("Error SetupBarcodeReader", e);
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }

        }

        private void SetupUsb()
        {
            var usbHandler = new UsbHandler(Globals.IND930USBDrive);
            CompositionContainer.AddInstance<IUsbHandler>(usbHandler);

            usbHandler.DeviceInsertedEvent += UsbHandlerOnDeviceInsertedEvent;
            usbHandler.DeviceRemovedEvent += UsbHandlerOnDeviceRemovedEvent;
        }

        private void UsbHandlerOnDeviceRemovedEvent(object sender, EventArgs e)
        {

        }

        private void UsbHandlerOnDeviceInsertedEvent(object sender, EventArgs e)
        {
        }

    }
}
