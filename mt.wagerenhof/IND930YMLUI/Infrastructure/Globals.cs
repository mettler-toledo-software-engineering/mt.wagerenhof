﻿using System;

using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using IND930UI.Library.Models;
using MT.Singularity.Platform;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Drawing;

namespace IND930YMLUI.Infrastructure
{
    public static class Globals
    {
        public const string IND930USBDrive = "D:\\";

        public const int ScreenWidth = 1280;
        public const int ScreenHeight = 800;

        public static readonly SingularityEnvironment Environment = new SingularityEnvironment("IND930YMLUI");

        public static DebitorModel DefaultDebitor = new DebitorModel { DebitorName = "Wagerenhof (Intern)", DebitorNumber = "20657", Id = 0 };

        public static string ProgVersionStr(bool withRevision)
        {
            Version version = Assembly.GetExecutingAssembly().GetName().Version;
            string vers = $"V {version.Major}.{version.Minor}.{version.Build}";
            if (withRevision)
            {
                vers += "." + version.Revision.ToString();
            }

            return vers;
        }

        public static SolidColorBrush TransparantBlueBrush = new SolidColorBrush(new Color(1, 248, 252, 248));
        public static SolidColorBrush LightGray = Colors.LightGrayBrush;
        public static string GetDataDirectory()
        {
            return Environment.DataDirectory;
        }
    }
}