﻿using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Presentation.Controls.Grid;
using MT.Singularity.Platform.CommonUX.Controls;
using MT.Singularity.Platform.CommonUX.Controls.MeasurementDisplay.View;
namespace IND930YMLUI.View
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.1.0.0")]
    public partial class SettingsView : MT.Singularity.Presentation.Controls.Navigation.NavigationPage
    {
        public void Update_manageContainerButtonTarget()
        {
            _manageContainerButton.Target = _settingsViewModel.ManageContainerView;
        }
        public void Update_manageCustomersButtonTarget()
        {
            _manageCustomersButton.Target = _settingsViewModel.ManageCustomersView;
        }
        public void Update_manageArticleButtonTarget()
        {
            _manageArticleButton.Target = _settingsViewModel.ManageArticlesView;
        }
        private MT.Singularity.Presentation.Controls.ContentControl Title;
        private MT.Singularity.Presentation.Controls.Navigation.NavigationButton _backButton;
        private MT.Singularity.Presentation.Controls.Navigation.NavigationButton _manageContainerButton;
        private MT.Singularity.Presentation.Controls.Navigation.NavigationButton _manageCustomersButton;
        private MT.Singularity.Presentation.Controls.Navigation.NavigationButton _manageArticleButton;
        private MT.Singularity.Presentation.Controls.StackPanel scalebuttonstack;
        private MT.Singularity.Presentation.Controls.StackPanel separationLine;
        private MT.Singularity.Platform.CommonUX.Controls.MeasurementDisplay.View.WeightDisplayWindow myWeightWindow;
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.Grid.Grid internal1;
            MT.Singularity.Presentation.Controls.TextBlock internal2;
            MT.Singularity.Presentation.Controls.DockPanel internal3;
            MT.Singularity.Presentation.Controls.GroupPanel internal4;
            MT.Singularity.Presentation.Controls.Image internal5;
            MT.Singularity.Presentation.Controls.GroupPanel internal6;
            MT.Singularity.Presentation.Controls.Image internal7;
            MT.Singularity.Presentation.Controls.GroupPanel internal8;
            MT.Singularity.Presentation.Controls.Image internal9;
            MT.Singularity.Presentation.Controls.GroupPanel internal10;
            MT.Singularity.Presentation.Controls.Image internal11;
            MT.Singularity.Platform.CommonUX.Controls.ZeroSoftkey internal12;
            MT.Singularity.Platform.CommonUX.Controls.ClearSoftkey internal13;
            MT.Singularity.Platform.CommonUX.Controls.TareSoftkey internal14;
            internal2 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal2.Text = IND930YMLUI.Localization.Get(IND930YMLUI.Localization.Key.Settings);
            internal2.AddTranslationAction(() => {
                internal2.Text = IND930YMLUI.Localization.Get(IND930YMLUI.Localization.Key.Settings);
            });
            internal2.FontSize = ((System.Nullable<System.Int32>)30);
            internal2.Foreground = new MT.Singularity.Presentation.Color(4294967295u);
            Title = new MT.Singularity.Presentation.Controls.ContentControl();
            Title.Content = internal2;
            Title.Height = 65;
            Title.GridRow = 0;
            Title.GridColumn = 0;
            Title.Margin = new MT.Singularity.Presentation.Thickness(5);
            Title.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            Title.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            Title.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4279383126u));
            _backButton = new MT.Singularity.Presentation.Controls.Navigation.NavigationButton();
            _backButton.Margin = new MT.Singularity.Presentation.Thickness(0, 0, 0, 5);
            _backButton.Height = 90;
            _backButton.Width = 100;
            _backButton.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            _backButton.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            _backButton.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            _backButton.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal5 = new MT.Singularity.Presentation.Controls.Image();
            internal5.Source = "embedded://IND930YMLUI/IND930YMLUI.images.ArrowLeft.al8";
            internal5.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal5.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal4 = new MT.Singularity.Presentation.Controls.GroupPanel(internal5);
            _backButton.Content = internal4;
            _backButton.Back = true;
            _manageContainerButton = new MT.Singularity.Presentation.Controls.Navigation.NavigationButton();
            _manageContainerButton.Margin = new MT.Singularity.Presentation.Thickness(0, 0, 0, 5);
            _manageContainerButton.Height = 90;
            _manageContainerButton.Width = 100;
            _manageContainerButton.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            _manageContainerButton.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            _manageContainerButton.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            _manageContainerButton.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal7 = new MT.Singularity.Presentation.Controls.Image();
            internal7.Source = "embedded://IND930YMLUI/IND930YMLUI.images.Container.al8";
            internal7.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal7.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal6 = new MT.Singularity.Presentation.Controls.GroupPanel(internal7);
            _manageContainerButton.Content = internal6;
            this.Update_manageContainerButtonTarget();
            _manageCustomersButton = new MT.Singularity.Presentation.Controls.Navigation.NavigationButton();
            _manageCustomersButton.Margin = new MT.Singularity.Presentation.Thickness(0, 0, 0, 5);
            _manageCustomersButton.Height = 90;
            _manageCustomersButton.Width = 100;
            _manageCustomersButton.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            _manageCustomersButton.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            _manageCustomersButton.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            _manageCustomersButton.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal9 = new MT.Singularity.Presentation.Controls.Image();
            internal9.Source = "embedded://IND930YMLUI/IND930YMLUI.images.Customer.al8";
            internal9.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal9.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal8 = new MT.Singularity.Presentation.Controls.GroupPanel(internal9);
            _manageCustomersButton.Content = internal8;
            this.Update_manageCustomersButtonTarget();
            _manageArticleButton = new MT.Singularity.Presentation.Controls.Navigation.NavigationButton();
            _manageArticleButton.Margin = new MT.Singularity.Presentation.Thickness(0, 0, 0, 5);
            _manageArticleButton.Height = 90;
            _manageArticleButton.Width = 100;
            _manageArticleButton.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            _manageArticleButton.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            _manageArticleButton.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            _manageArticleButton.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal11 = new MT.Singularity.Presentation.Controls.Image();
            internal11.Source = "embedded://IND930YMLUI/IND930YMLUI.images.Clipboard.al8";
            internal11.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal11.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal10 = new MT.Singularity.Presentation.Controls.GroupPanel(internal11);
            _manageArticleButton.Content = internal10;
            this.Update_manageArticleButtonTarget();
            internal3 = new MT.Singularity.Presentation.Controls.DockPanel(_backButton, _manageContainerButton, _manageCustomersButton, _manageArticleButton);
            internal3.GridColumn = 2;
            internal3.GridRow = 1;
            internal3.Width = 110;
            internal3.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal3.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal12 = new MT.Singularity.Platform.CommonUX.Controls.ZeroSoftkey();
            internal12.Margin = new MT.Singularity.Presentation.Thickness(0, 5, 0, 0);
            internal12.Width = 100;
            internal12.Height = 90;
            internal12.ScaleNumber = 0;
            internal12.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal12.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal13 = new MT.Singularity.Platform.CommonUX.Controls.ClearSoftkey();
            internal13.Margin = new MT.Singularity.Presentation.Thickness(0, 5, 0, 0);
            internal13.Width = 100;
            internal13.Height = 90;
            internal13.ScaleNumber = 0;
            internal13.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal13.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal14 = new MT.Singularity.Platform.CommonUX.Controls.TareSoftkey();
            internal14.Margin = new MT.Singularity.Presentation.Thickness(0, 5, 0, 0);
            internal14.Width = 100;
            internal14.Height = 90;
            internal14.ScaleNumber = 0;
            internal14.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal14.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            scalebuttonstack = new MT.Singularity.Presentation.Controls.StackPanel(internal12, internal13, internal14);
            scalebuttonstack.GridColumn = 2;
            scalebuttonstack.GridRow = 0;
            scalebuttonstack.GridRowSpan = 2;
            scalebuttonstack.Width = 110;
            scalebuttonstack.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            scalebuttonstack.Orientation = MT.Singularity.Presentation.Orientation.Vertical;
            scalebuttonstack.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            separationLine = new MT.Singularity.Presentation.Controls.StackPanel();
            separationLine.Margin = new MT.Singularity.Presentation.Thickness(0, 5, 0, 5);
            separationLine.GridColumn = 1;
            separationLine.GridRow = 0;
            separationLine.GridRowSpan = 2;
            separationLine.Orientation = MT.Singularity.Presentation.Orientation.Vertical;
            separationLine.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            separationLine.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            separationLine.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4279383126u));
            myWeightWindow = new MT.Singularity.Platform.CommonUX.Controls.MeasurementDisplay.View.WeightDisplayWindow();
            myWeightWindow.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4294507768u));
            myWeightWindow.Margin = new MT.Singularity.Presentation.Thickness(0, 5, 5, 0);
            myWeightWindow.GridColumn = 0;
            myWeightWindow.GridRow = 1;
            myWeightWindow.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            myWeightWindow.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Right;
            myWeightWindow.Width = 400;
            myWeightWindow.Height = 200;
            internal1 = new MT.Singularity.Presentation.Controls.Grid.Grid(Title, internal3, scalebuttonstack, separationLine, myWeightWindow);
            internal1.RowDefinitions = GridDefinitions.Create("65","1*");
            internal1.ColumnDefinitions = GridDefinitions.Create("1*","1","110");
            this.Content = internal1;
            this.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4294507768u));
        }
    }
}
