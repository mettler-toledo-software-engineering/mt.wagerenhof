﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using IND930UI.Library.Models;
using IND930YMLUI.ViewModels;
using MT.Singularity;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;

namespace IND930YMLUI.View
{
    /// <summary>
    /// Interaction logic for SelectArticleView
    /// </summary>
    public partial class SelectArticleView
    {
        private SelectArticleViewModel _selectArticleViewModel;
        public SelectArticleView(RegularLaundryModel regularLaundry)
        {


            _selectArticleViewModel = new SelectArticleViewModel(this, regularLaundry);
            InitializeComponents();
            _backButton.Background = Colors.LightGrayBrush;
            _confirmSelectionButton.Background = Colors.LightGrayBrush;

        }

        public SelectArticleView(CompanyLaundryProcessModel companyLaundry)
        {
            _selectArticleViewModel = new SelectArticleViewModel(this, companyLaundry);
            InitializeComponents();
            _backButton.Background = Colors.LightGrayBrush;
            _confirmSelectionButton.Background = Colors.LightGrayBrush;
        }

        public INavigationPage SelectArticlePage
        {
            get
            {
                return this;
            }
        }
        /// <summary>
        /// This method is called before the home screen is shown.
        /// </summary>
        public void BeforeStart(IRootVisualProvider rootVisualProvider)
        {
        }

        /// <summary>
        /// Called when a page is being navigated to for the first time.
        /// </summary>
        protected override void OnFirstNavigation()
        {
            myWeightWindow.Activate();
            _selectArticleViewModel.RegisterBarcodeReader();
            _selectArticleViewModel.RegisterEventsForViewModel();
            _selectArticleViewModel.SetProcessStep();
            base.OnFirstNavigation();
        }

        /// <summary>
        /// Called when the user is navigating away from the current page to <paramref name="nextPage" />.
        /// </summary>
        /// <param name="nextPage">The next page the user is navigating to.</param>
        /// <returns>
        /// A value how the navigation framework should proceed with the navigation request.
        /// </returns>
        protected override NavigationResult OnNavigatingAway(INavigationPage nextPage)
        {
            var result = base.OnNavigatingAway(nextPage);
            
            _selectArticleViewModel.UnregisterEventsForViewModel();
            if (result == NavigationResult.Proceed)
            {
                myWeightWindow.Deactivate();
            }
            return result;
        }

        /// <summary>
        /// Called when a page is reactivated when returning from another page.
        /// </summary>
        /// <param name="previousPage">The page that the user is returning from.</param>
        protected override void OnNavigationReturning(INavigationPage previousPage)
        {
            base.OnNavigationReturning(previousPage);
            _selectArticleViewModel.RegisterBarcodeReader();
            _selectArticleViewModel.RegisterEventsForViewModel();
            _selectArticleViewModel.SetProcessStep();
            myWeightWindow.Activate();
        }

        protected override NavigationResult OnNavigatingBack(INavigationPage nextPage)
        {
            _selectArticleViewModel.ClearSearchText();
            _selectArticleViewModel.UnregisterEventsForViewModel();
            return base.OnNavigatingBack(nextPage);
        }

        private void ShowKeyboardRequested(object sender, EventArgs e)
        {
            _selectArticleViewModel.KeyboardVisibility = Visibility.Visible;
        }


        private void CollapseKeyboardRequested(object sender, EventArgs e)
        {
            _selectArticleViewModel.KeyboardVisibility = Visibility.Collapsed;
        }
    }
}
