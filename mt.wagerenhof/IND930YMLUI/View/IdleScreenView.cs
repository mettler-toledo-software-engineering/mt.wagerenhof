﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using IND930YMLUI.ViewModels;
using MT.Singularity;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Presentation.Input;

namespace IND930YMLUI.View
{
    /// <summary>
    /// Interaction logic for IdleScreenView
    /// </summary>
    public partial class IdleScreenView
    {
        private IdleScreenViewModel _idleScreenViewModel;
        public IdleScreenView()
        {
            _idleScreenViewModel = new IdleScreenViewModel(this);
            InitializeComponents();
            _settingsButton.Background = Colors.LightGrayBrush;

        }

        public INavigationPage IdlePage
        {
            get
            {
                return this;
            }
        }



        /// <summary>
        /// Called when a page is being navigated to for the first time.
        /// </summary>
        protected override void OnFirstNavigation()
        {
            myWeightWindow.Activate();
            _idleScreenViewModel.RegisterEventsForViewModel();
            _idleScreenViewModel.SetProcessStep();
            base.OnFirstNavigation();
        }

        /// <summary>
        /// Called when the user is navigating away from the current page to <paramref name="nextPage" />.
        /// </summary>
        /// <param name="nextPage">The next page the user is navigating to.</param>
        /// <returns>
        /// A value how the navigation framework should proceed with the navigation request.
        /// </returns>
        protected override NavigationResult OnNavigatingAway(INavigationPage nextPage)
        {
            var result = base.OnNavigatingAway(nextPage);
            _idleScreenViewModel.UnregisterEventsForViewModel();
            if (result == NavigationResult.Proceed)
            {
                myWeightWindow.Deactivate();
            }
            return result;
        }

        /// <summary>
        /// Called when a page is reactivated when returning from another page.
        /// </summary>
        /// <param name="previousPage">The page that the user is returning from.</param>
        protected override void OnNavigationReturning(INavigationPage previousPage)
        {
            
            base.OnNavigationReturning(previousPage);
            _idleScreenViewModel.RegisterEventsForViewModel();
            _idleScreenViewModel.SetProcessStep();
            myWeightWindow.Activate();
        }

        protected override NavigationResult OnNavigatingBack(INavigationPage nextPage)
        {
            _idleScreenViewModel.UnregisterEventsForViewModel();
            return base.OnNavigatingBack(nextPage);
        }

        
    }
}
