﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using IND930UI.Library.Models;
using IND930YMLUI.ViewModels;
using MT.Singularity;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;

namespace IND930YMLUI.View
{
    /// <summary>
    /// Interaction logic for CompanyWeighingView
    /// </summary>
    public partial class CompanyWeighingView
    {
        private CompanyWeighingViewModel _companyWeighingViewModel;
        private CompanyLaundryProcessModel _companyLaundryProcess;
        public CompanyWeighingView(CompanyLaundryProcessModel companyLaundryProcess)
        {
            _companyLaundryProcess = companyLaundryProcess;
            _companyWeighingViewModel = new CompanyWeighingViewModel(this, _companyLaundryProcess);
            InitializeComponents();

            _backButton.Background = Colors.LightGrayBrush;

        }

        public INavigationPage CompanyWeighingPage
        {
            get
            {
                return this;
            }
        }
        /// <summary>
        /// This method is called before the home screen is shown.
        /// </summary>
        public void BeforeStart(IRootVisualProvider rootVisualProvider)
        {
        }

        /// <summary>
        /// Called when a page is being navigated to for the first time.
        /// </summary>
        protected override async void OnFirstNavigation()
        {
            myWeightWindow.Activate();
            _companyWeighingViewModel.RegisterEventsForViewModel();
            await _companyWeighingViewModel.TareScale();
            _companyWeighingViewModel.PopulateProperties();
            base.OnFirstNavigation();
        }

        /// <summary>
        /// Called when the user is navigating away from the current page to <paramref name="nextPage" />.
        /// </summary>
        /// <param name="nextPage">The next page the user is navigating to.</param>
        /// <returns>
        /// A value how the navigation framework should proceed with the navigation request.
        /// </returns>
        protected override NavigationResult OnNavigatingAway(INavigationPage nextPage)
        {

            var result = base.OnNavigatingAway(nextPage);
            _companyWeighingViewModel.UnregisterEventsForViewModel();
            if (result == NavigationResult.Proceed)
            {
                myWeightWindow.Deactivate();
            }
            return result;
        }

        /// <summary>
        /// Called when a page is reactivated when returning from another page.
        /// </summary>
        /// <param name="previousPage">The page that the user is returning from.</param>
        protected override async void OnNavigationReturning(INavigationPage previousPage)
        {
            base.OnNavigationReturning(previousPage);
            _companyWeighingViewModel.RegisterEventsForViewModel();
            
            _companyWeighingViewModel.PopulateProperties();
            myWeightWindow.Activate();
            await _companyWeighingViewModel.TareScale();
        }

        protected override NavigationResult OnNavigatingBack(INavigationPage nextPage)
        {
            _companyWeighingViewModel.UnregisterEventsForViewModel();
            return base.OnNavigatingBack(nextPage);
        }
    }
}
