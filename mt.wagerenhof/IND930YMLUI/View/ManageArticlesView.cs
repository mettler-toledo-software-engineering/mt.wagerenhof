﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using IND930YMLUI.ViewModels;
using MT.Singularity;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;

namespace IND930YMLUI.View
{
    /// <summary>
    /// Interaction logic for ManageArticlesView
    /// </summary>
    public partial class ManageArticlesView
    {
        private ManageArticlesViewModel _manageArticlesViewModel;
        public ManageArticlesView()
        {
            _manageArticlesViewModel = new ManageArticlesViewModel(this);
            InitializeComponents();
            _backButton.Background = Colors.LightGrayBrush;
        }

        public INavigationPage ManageArticlePage
        {
            get
            {
                return this;
            }
        }
        /// <summary>
        /// This method is called before the home screen is shown.
        /// </summary>
        public void BeforeStart(IRootVisualProvider rootVisualProvider)
        {
        }

        /// <summary>
        /// Called when a page is being navigated to for the first time.
        /// </summary>
        protected override void OnFirstNavigation()
        {
            myWeightWindow.Activate();
            _manageArticlesViewModel.RegisterEventsForViewModel();
            base.OnFirstNavigation();
        }

        /// <summary>
        /// Called when the user is navigating away from the current page to <paramref name="nextPage" />.
        /// </summary>
        /// <param name="nextPage">The next page the user is navigating to.</param>
        /// <returns>
        /// A value how the navigation framework should proceed with the navigation request.
        /// </returns>
        protected override NavigationResult OnNavigatingAway(INavigationPage nextPage)
        {
            var result = base.OnNavigatingAway(nextPage);
            _manageArticlesViewModel.UnregisterEventsForViewModel();
            if (result == NavigationResult.Proceed)
            {
                myWeightWindow.Deactivate();
            }
            return result;
        }

        /// <summary>
        /// Called when a page is reactivated when returning from another page.
        /// </summary>
        /// <param name="previousPage">The page that the user is returning from.</param>
        protected override void OnNavigationReturning(INavigationPage previousPage)
        {
            base.OnNavigationReturning(previousPage);

            _manageArticlesViewModel.RegisterEventsForViewModel();
            myWeightWindow.Activate();
        }

        protected override NavigationResult OnNavigatingBack(INavigationPage nextPage)
        {
            _manageArticlesViewModel.UnregisterEventsForViewModel();
            return base.OnNavigatingBack(nextPage);
        }

        private void ShowKeyboardRequested(object sender, EventArgs e)
        {
            _manageArticlesViewModel.KeyboardVisibility = Visibility.Visible;
        }


        private void CollapseKeyboardRequested(object sender, EventArgs e)
        {
            _manageArticlesViewModel.KeyboardVisibility = Visibility.Collapsed;
        }
    }
}
