﻿using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Grid;
using MT.Singularity.Presentation.Controls.DataGrid;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Platform.CommonUX.Controls;
using MT.Singularity.Platform.CommonUX.Controls.MeasurementDisplay.View;
namespace IND930YMLUI.View
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.1.0.0")]
    public partial class HomeScreen : MT.Singularity.Presentation.Controls.Navigation.NavigationPage
    {
        private MT.Singularity.Presentation.Controls.Navigation.NavigationFrame _navigationFrame;
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.Grid.Grid internal1;
            _navigationFrame = new MT.Singularity.Presentation.Controls.Navigation.NavigationFrame();
            _navigationFrame.IsEnabled = true;
            _navigationFrame.GridRow = 0;
            _navigationFrame.GridColumn = 0;
            _navigationFrame.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            _navigationFrame.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal1 = new MT.Singularity.Presentation.Controls.Grid.Grid(_navigationFrame);
            internal1.RowDefinitions = GridDefinitions.Create("1*");
            internal1.ColumnDefinitions = GridDefinitions.Create("1*");
            this.Content = internal1;
            this.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4294507768u));
        }
    }
}
