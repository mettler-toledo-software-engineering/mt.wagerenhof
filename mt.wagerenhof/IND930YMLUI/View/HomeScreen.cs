﻿using System;
using IND930YMLUI.ViewModels;
using MT.Singularity.Composition;
using MT.Singularity.Presentation;
using MT.Singularity.Platform.CommonUX.Infrastructure;
using MT.Singularity.Presentation.Controls.Navigation;

namespace IND930YMLUI.View
{
    /// <summary>
    /// Interaction logic for HomeScreen
    /// </summary>
    [Export(typeof(IHomeScreenFactoryService))]
    public partial class HomeScreen : IHomeScreenFactoryService
    {
        private readonly HomeScreenViewModel _homeScreenViewModel = new HomeScreenViewModel();

        /// <summary>
        /// Initializes a new instance of the <see cref="HomeScreen"/> class.
        /// </summary>
        public HomeScreen()
        {
            InitializeComponents();
        }

        /// <summary>
        /// Gets the home screen page.
        /// </summary>
        /// <value>
        /// The home screen page.
        /// </value>
        public INavigationPage HomeScreenPage
        {
            get { return this; }
        }

        /// <summary>
        /// This method is called before the home screen is shown.
        /// </summary>
        public void BeforeStart(IRootVisualProvider rootVisualProvider)
        {
        }

        /// <summary>
        /// Called when a page is being navigated to for the first time.
        /// </summary>
        protected override void OnFirstNavigation()
        {
            base.OnFirstNavigation();
            var idlescreen = new IdleScreenView();
            _navigationFrame.NavigateTo(idlescreen);
            _navigationFrame.ActivePageChanged += NavigationFrameOnActivePageChanged;
        }

        private void NavigationFrameOnActivePageChanged(object sender, EventArgs e)
        {

            Console.WriteLine(_navigationFrame.Count);
        }


        /// <summary>
        /// Gets a value indicating whether the cursor should be hidden.
        /// </summary>
        /// <value>
        ///   <c>true</c> to hide the cursor; otherwise, <c>false</c>.
        /// </value>
        public bool HideCursor
        {
#if DEBUG
            get { return false; }
#else
            get { return true; }
#endif
        }



    }
}