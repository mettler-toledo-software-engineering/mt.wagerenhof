﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using DataAccess;
using IND930UI.Library.Models;
using IND930YMLUI.ViewModels;
using HandledEventArgs = MT.Singularity.Presentation.Input.HandledEventArgs;

namespace IND930YMLUI.View
{
    /// <summary>
    /// Interaction logic for AddContainerView
    /// </summary>
    public partial class AddContainerView
    {

        private AddContainerViewModel _addContainerViewModel;

        public AddContainerView(string containerName, double containerWeight)
        {

            _addContainerViewModel = new AddContainerViewModel(this, containerName, containerWeight);
            InitializeComponents();

            _cancelButton.Click += CancelButtonOnClick;
        }


        public AddContainerView(ContainerModel selectedContainer = null)
        {

            _addContainerViewModel = new AddContainerViewModel(this, selectedContainer);
            InitializeComponents();

            _cancelButton.Click += CancelButtonOnClick;
        }

        private void CancelButtonOnClick(object sender, HandledEventArgs e)
        {
            Close();
        }
    }
}
