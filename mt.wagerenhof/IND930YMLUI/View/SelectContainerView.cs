﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using IND930UI.Library.Models;
using IND930YMLUI.ViewModels;
using MT.Singularity;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;

namespace IND930YMLUI.View
{
    /// <summary>
    /// Interaction logic for SelectContainerView
    /// </summary>
    public partial class SelectContainerView
    {
        private RegularLaundryModel _regularLaundry;
        private SelectContainerViewModel _selectContainerViewModel;
        private CompanyLaundryProcessModel _companyLaundryProcess;
        public SelectContainerView(RegularLaundryModel regularLaundry)
        {
            _regularLaundry = regularLaundry;

            _selectContainerViewModel = new SelectContainerViewModel(this,this,_regularLaundry);
            InitializeComponents();

            _backButton.Background = Colors.LightGrayBrush;
            _confirmSelectionButton.Background = Colors.LightGrayBrush;
        }

        public SelectContainerView(CompanyLaundryProcessModel companyLaundryProcess)
        {
            _companyLaundryProcess = companyLaundryProcess;

            _selectContainerViewModel = new SelectContainerViewModel(this, this, _companyLaundryProcess);

            InitializeComponents();

            _backButton.Background = Colors.LightGrayBrush;
            _confirmSelectionButton.Background = Colors.LightGrayBrush;
        }

        public INavigationPage SelectContainerPage
        {
            get
            {
                return this;
            }
        }
        /// <summary>
        /// This method is called before the home screen is shown.
        /// </summary>
        public void BeforeStart(IRootVisualProvider rootVisualProvider)
        {
        }

        /// <summary>
        /// Called when a page is being navigated to for the first time.
        /// </summary>
        protected override void OnFirstNavigation()
        {
            myWeightWindow.Activate();
            _selectContainerViewModel.RegisterBarcodeReader();
            _selectContainerViewModel.RegisterEventsForViewModel();
            _selectContainerViewModel.SetProcessStep();
            base.OnFirstNavigation();
        }

        /// <summary>
        /// Called when the user is navigating away from the current page to <paramref name="nextPage" />.
        /// </summary>
        /// <param name="nextPage">The next page the user is navigating to.</param>
        /// <returns>
        /// A value how the navigation framework should proceed with the navigation request.
        /// </returns>
        protected override NavigationResult OnNavigatingAway(INavigationPage nextPage)
        {
            var result = base.OnNavigatingAway(nextPage);
            
            _selectContainerViewModel.UnregisterEventsForViewModel();
            if (result == NavigationResult.Proceed)
            {
                myWeightWindow.Deactivate();
            }
            return result;
        }

        /// <summary>
        /// Called when a page is reactivated when returning from another page.
        /// </summary>
        /// <param name="previousPage">The page that the user is returning from.</param>
        protected override void OnNavigationReturning(INavigationPage previousPage)
        {
            base.OnNavigationReturning(previousPage);
            _selectContainerViewModel.RegisterBarcodeReader();
            _selectContainerViewModel.RegisterEventsForViewModel();
            _selectContainerViewModel.SetProcessStep();
            myWeightWindow.Activate();
        }

        protected override NavigationResult OnNavigatingBack(INavigationPage nextPage)
        {
            _selectContainerViewModel.UnregisterEventsForViewModel();
            _selectContainerViewModel.ClearSearchText();
            return base.OnNavigatingBack(nextPage);
        }

        private void ShowKeyboardRequested(object sender, EventArgs e)
        {
            _selectContainerViewModel.KeyboardVisibility = Visibility.Visible;
        }


        private void CollapseKeyboardRequested(object sender, EventArgs e)
        {
            _selectContainerViewModel.KeyboardVisibility = Visibility.Collapsed;
        }
    }
}
