﻿using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Presentation.Controls.Grid;
using MT.Singularity.Presentation.Controls.DataGrid;
using MT.Singularity.Platform.CommonUX.Controls;
using MT.Singularity.Platform.CommonUX.Controls.MeasurementDisplay.View;
namespace IND930YMLUI.View
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.1.0.0")]
    public partial class SelectContainerView : MT.Singularity.Presentation.Controls.Navigation.NavigationPage
    {
        private MT.Singularity.Presentation.Controls.ContentControl Title;
        private MT.Singularity.Presentation.Controls.DataGrid.DataGrid _containerGrid;
        private MT.Singularity.Presentation.Controls.Button _listUpButton;
        private MT.Singularity.Presentation.Controls.Button _listDownButton;
        private MT.Singularity.Presentation.Controls.Navigation.NavigationButton _confirmSelectionButton;
        private MT.Singularity.Presentation.Controls.Navigation.NavigationButton _backButton;
        private MT.Singularity.Presentation.Controls.StackPanel scalebuttonstack;
        private MT.Singularity.Presentation.Controls.StackPanel separationLine;
        private MT.Singularity.Platform.CommonUX.Controls.MeasurementDisplay.View.WeightDisplayWindow myWeightWindow;
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.Grid.Grid internal1;
            MT.Singularity.Presentation.Controls.TextBlock internal2;
            MT.Singularity.Presentation.Controls.DataGrid.DataGridTextColumn internal3;
            MT.Singularity.Presentation.Controls.GroupPanel internal4;
            MT.Singularity.Presentation.Controls.Image internal5;
            MT.Singularity.Presentation.Controls.GroupPanel internal6;
            MT.Singularity.Presentation.Controls.Image internal7;
            MT.Singularity.Presentation.Controls.StackPanel internal8;
            MT.Singularity.Presentation.Controls.TextBlock internal9;
            MT.Singularity.Presentation.Controls.TextBox internal10;
            MT.Singularity.Presentation.Controls.Keyboard internal11;
            MT.Singularity.Presentation.Controls.TextBlock internal12;
            MT.Singularity.Presentation.Controls.DockPanel internal13;
            MT.Singularity.Presentation.Controls.Button internal14;
            MT.Singularity.Presentation.Controls.GroupPanel internal15;
            MT.Singularity.Presentation.Controls.Image internal16;
            MT.Singularity.Presentation.Controls.GroupPanel internal17;
            MT.Singularity.Presentation.Controls.Image internal18;
            MT.Singularity.Presentation.Controls.TextBlock internal19;
            MT.Singularity.Presentation.Controls.DockPanel internal20;
            MT.Singularity.Presentation.Controls.GroupPanel internal21;
            MT.Singularity.Presentation.Controls.Image internal22;
            MT.Singularity.Platform.CommonUX.Controls.ZeroSoftkey internal23;
            MT.Singularity.Platform.CommonUX.Controls.ClearSoftkey internal24;
            MT.Singularity.Platform.CommonUX.Controls.TareSoftkey internal25;
            internal2 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal2.Text = IND930YMLUI.Localization.Get(IND930YMLUI.Localization.Key.SelectContainer);
            internal2.AddTranslationAction(() => {
                internal2.Text = IND930YMLUI.Localization.Get(IND930YMLUI.Localization.Key.SelectContainer);
            });
            internal2.FontSize = ((System.Nullable<System.Int32>)30);
            internal2.Foreground = new MT.Singularity.Presentation.Color(4294967295u);
            Title = new MT.Singularity.Presentation.Controls.ContentControl();
            Title.Content = internal2;
            Title.Height = 65;
            Title.GridRow = 0;
            Title.GridColumn = 0;
            Title.GridColumnSpan = 4;
            Title.Margin = new MT.Singularity.Presentation.Thickness(5);
            Title.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            Title.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            Title.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4279383126u));
            internal3 = new MT.Singularity.Presentation.Controls.DataGrid.DataGridTextColumn();
            internal3.Header = "Bezeichnung";
            internal3.ColumnName = "NameInGrid";
            internal3.Width = 410;
            _containerGrid = new MT.Singularity.Presentation.Controls.DataGrid.DataGrid();
            _containerGrid.Columns.Add(internal3);
            _containerGrid.Margin = new MT.Singularity.Presentation.Thickness(5, 5, 5, 5);
            _containerGrid.GridColumn = 0;
            _containerGrid.GridColumnSpan = 2;
            _containerGrid.GridRow = 1;
            _containerGrid.GridRowSpan = 2;
            _containerGrid.RowHeight = 42;
            _containerGrid.ColumnHeaderHeight = 42;
            _containerGrid.FontSize = ((System.Nullable<System.Int32>)24);
            this.bindings[0] = MT.Singularity.Expressions.ExpressionBinding.Create(() => _containerGrid.ItemsSource,() =>  _selectContainerViewModel.ContainerList,MT.Singularity.Expressions.BindingMode.OneWay,false);
            this.bindings[1] = MT.Singularity.Expressions.ExpressionBinding.Create(() => _containerGrid.SelectedIndex,() =>  _selectContainerViewModel.ContainerListIndex,MT.Singularity.Expressions.BindingMode.TwoWay,false);
            this.bindings[2] = MT.Singularity.Expressions.ExpressionBinding.Create(() => _containerGrid.SelectedItem,() =>  _selectContainerViewModel.SelectedContainer,MT.Singularity.Expressions.BindingMode.TwoWay,false);
            _listUpButton = new MT.Singularity.Presentation.Controls.Button();
            _listUpButton.Margin = new MT.Singularity.Presentation.Thickness(5, 5, 10, 5);
            _listUpButton.GridColumn = 0;
            _listUpButton.GridRow = 3;
            _listUpButton.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            _listUpButton.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal5 = new MT.Singularity.Presentation.Controls.Image();
            internal5.Source = "embedded://IND930YMLUI/IND930YMLUI.images.ArrowUp.al8";
            internal5.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal5.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal4 = new MT.Singularity.Presentation.Controls.GroupPanel(internal5);
            _listUpButton.Content = internal4;
            this.bindings[3] = MT.Singularity.Expressions.ExpressionBinding.Create(() => _listUpButton.Command,() =>  _selectContainerViewModel.ScrollUpCommand,MT.Singularity.Expressions.BindingMode.OneWay,false);
            _listDownButton = new MT.Singularity.Presentation.Controls.Button();
            _listDownButton.Margin = new MT.Singularity.Presentation.Thickness(10, 5, 5, 5);
            _listDownButton.GridColumn = 1;
            _listDownButton.GridRow = 3;
            _listDownButton.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            _listDownButton.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal7 = new MT.Singularity.Presentation.Controls.Image();
            internal7.Source = "embedded://IND930YMLUI/IND930YMLUI.images.ArrowDown.al8";
            internal7.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal7.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal6 = new MT.Singularity.Presentation.Controls.GroupPanel(internal7);
            _listDownButton.Content = internal6;
            this.bindings[4] = MT.Singularity.Expressions.ExpressionBinding.Create(() => _listDownButton.Command,() =>  _selectContainerViewModel.ScrollDownCommand,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal9 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal9.Text = IND930YMLUI.Localization.Get(IND930YMLUI.Localization.Key.SearchFilter);
            internal9.AddTranslationAction(() => {
                internal9.Text = IND930YMLUI.Localization.Get(IND930YMLUI.Localization.Key.SearchFilter);
            });
            internal9.FontSize = ((System.Nullable<System.Int32>)24);
            internal9.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal9.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal10 = new MT.Singularity.Presentation.Controls.TextBox();
            internal10.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal10.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal10.ShowKeyboardOnFocus = false;
            internal10.GotFocus += ShowKeyboardRequested;
            internal10.LostFocus += CollapseKeyboardRequested;
            this.bindings[5] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal10.Text,() => _selectContainerViewModel.SearchText,MT.Singularity.Expressions.BindingMode.TwoWay,false);
            internal10.Width = 500;
            internal10.Height = 50;
            internal10.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4290039284u));
            internal11 = new MT.Singularity.Presentation.Controls.Keyboard();
            internal11.Margin = new MT.Singularity.Presentation.Thickness(0, 5, 0, 5);
            internal11.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal11.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            this.bindings[6] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal11.Visibility,() => _selectContainerViewModel.KeyboardVisibility,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal11.SaveRequested += CollapseKeyboardRequested;
            internal11.CancelRequested += CollapseKeyboardRequested;
            internal11.CloseRequested += CollapseKeyboardRequested;
            internal11.Width = 695;
            internal12 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal12.FontSize = ((System.Nullable<System.Int32>)24);
            internal12.Text = "Bitte wähle einen Container für die Wägung aus";
            internal12.Margin = new MT.Singularity.Presentation.Thickness(0, 0, 0, 20);
            internal14 = new MT.Singularity.Presentation.Controls.Button();
            internal14.Margin = new MT.Singularity.Presentation.Thickness(0, 5, 5, 5);
            internal14.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal16 = new MT.Singularity.Presentation.Controls.Image();
            internal16.Source = "C:\\Packages\\IND930YMLUI\\Data\\Images\\basket_big.png";
            internal16.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal16.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal15 = new MT.Singularity.Presentation.Controls.GroupPanel(internal16);
            internal14.Content = internal15;
            internal14.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4294507768u));
            internal14.Width = 300;
            _confirmSelectionButton = new MT.Singularity.Presentation.Controls.Navigation.NavigationButton();
            _confirmSelectionButton.Margin = new MT.Singularity.Presentation.Thickness(0, 5, 5, 5);
            _confirmSelectionButton.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Right;
            _confirmSelectionButton.Width = 300;
            _confirmSelectionButton.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            _confirmSelectionButton.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal18 = new MT.Singularity.Presentation.Controls.Image();
            internal18.Source = "embedded://IND930YMLUI/IND930YMLUI.images.ok.al8";
            internal18.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal18.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal19 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal19.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal19.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal19.Text = "Bestätigen";
            internal19.FontSize = ((System.Nullable<System.Int32>)16);
            internal19.Margin = new MT.Singularity.Presentation.Thickness(1);
            internal17 = new MT.Singularity.Presentation.Controls.GroupPanel(internal18, internal19);
            _confirmSelectionButton.Content = internal17;
            this.bindings[7] = MT.Singularity.Expressions.ExpressionBinding.Create(() => _confirmSelectionButton.Command,() => _selectContainerViewModel.ConfirmSelectionCommand,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal13 = new MT.Singularity.Presentation.Controls.DockPanel(internal14, _confirmSelectionButton);
            internal8 = new MT.Singularity.Presentation.Controls.StackPanel(internal9, internal10, internal11, internal12, internal13);
            internal8.Margin = new MT.Singularity.Presentation.Thickness(10, 0, 10, 0);
            internal8.GridColumn = 2;
            internal8.GridColumnSpan = 2;
            internal8.GridRow = 2;
            internal8.GridRowSpan = 2;
            internal8.Orientation = MT.Singularity.Presentation.Orientation.Vertical;
            _backButton = new MT.Singularity.Presentation.Controls.Navigation.NavigationButton();
            _backButton.Margin = new MT.Singularity.Presentation.Thickness(0, 0, 0, 5);
            _backButton.Height = 90;
            _backButton.Width = 100;
            _backButton.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            _backButton.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            _backButton.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            _backButton.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal22 = new MT.Singularity.Presentation.Controls.Image();
            internal22.Source = "embedded://IND930YMLUI/IND930YMLUI.images.ArrowLeft.al8";
            internal22.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal22.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal21 = new MT.Singularity.Presentation.Controls.GroupPanel(internal22);
            _backButton.Content = internal21;
            _backButton.Back = true;
            internal20 = new MT.Singularity.Presentation.Controls.DockPanel(_backButton);
            internal20.GridColumn = 5;
            internal20.GridRow = 2;
            internal20.GridRowSpan = 2;
            internal20.Width = 110;
            internal20.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal20.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal23 = new MT.Singularity.Platform.CommonUX.Controls.ZeroSoftkey();
            internal23.Margin = new MT.Singularity.Presentation.Thickness(0, 5, 0, 0);
            internal23.Width = 100;
            internal23.Height = 90;
            internal23.ScaleNumber = 0;
            internal23.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal23.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal24 = new MT.Singularity.Platform.CommonUX.Controls.ClearSoftkey();
            internal24.Margin = new MT.Singularity.Presentation.Thickness(0, 5, 0, 0);
            internal24.Width = 100;
            internal24.Height = 90;
            internal24.ScaleNumber = 0;
            internal24.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal24.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal25 = new MT.Singularity.Platform.CommonUX.Controls.TareSoftkey();
            internal25.Margin = new MT.Singularity.Presentation.Thickness(0, 5, 0, 0);
            internal25.Width = 100;
            internal25.Height = 90;
            internal25.ScaleNumber = 0;
            internal25.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal25.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            scalebuttonstack = new MT.Singularity.Presentation.Controls.StackPanel(internal23, internal24, internal25);
            scalebuttonstack.GridColumn = 5;
            scalebuttonstack.GridRow = 0;
            scalebuttonstack.GridRowSpan = 2;
            scalebuttonstack.Width = 110;
            scalebuttonstack.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            scalebuttonstack.Orientation = MT.Singularity.Presentation.Orientation.Vertical;
            scalebuttonstack.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            separationLine = new MT.Singularity.Presentation.Controls.StackPanel();
            separationLine.Margin = new MT.Singularity.Presentation.Thickness(0, 5, 0, 5);
            separationLine.GridColumn = 4;
            separationLine.GridRow = 0;
            separationLine.GridRowSpan = 4;
            separationLine.Orientation = MT.Singularity.Presentation.Orientation.Vertical;
            separationLine.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            separationLine.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            separationLine.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4279383126u));
            myWeightWindow = new MT.Singularity.Platform.CommonUX.Controls.MeasurementDisplay.View.WeightDisplayWindow();
            myWeightWindow.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4294507768u));
            myWeightWindow.Margin = new MT.Singularity.Presentation.Thickness(0, 5, 5, 0);
            myWeightWindow.GridColumn = 2;
            myWeightWindow.GridColumnSpan = 2;
            myWeightWindow.GridRow = 1;
            myWeightWindow.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            myWeightWindow.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Right;
            myWeightWindow.Width = 400;
            myWeightWindow.Height = 200;
            internal1 = new MT.Singularity.Presentation.Controls.Grid.Grid(Title, _containerGrid, _listUpButton, _listDownButton, internal8, internal20, scalebuttonstack, separationLine, myWeightWindow);
            internal1.RowDefinitions = GridDefinitions.Create("65","240","1*","100");
            internal1.ColumnDefinitions = GridDefinitions.Create("1*","1*","350","350","1","110");
            this.Content = internal1;
            this.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4294507768u));
        }
        private readonly MT.Singularity.Expressions.IDataBinding[] bindings = new MT.Singularity.Expressions.IDataBinding[8];
    }
}
