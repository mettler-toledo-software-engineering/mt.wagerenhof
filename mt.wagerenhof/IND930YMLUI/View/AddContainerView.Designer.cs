﻿using MT.Singularity.Presentation.Controls;
namespace IND930YMLUI.View
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.1.0.0")]
    public partial class AddContainerView : MT.Singularity.Presentation.Controls.ChildWindow
    {
        private MT.Singularity.Presentation.Controls.Button _cancelButton;
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.StackPanel internal1;
            MT.Singularity.Presentation.Controls.StackPanel internal2;
            MT.Singularity.Presentation.Controls.TextBlock internal3;
            MT.Singularity.Presentation.Controls.TextBox internal4;
            MT.Singularity.Presentation.Controls.TextBlock internal5;
            MT.Singularity.Presentation.Controls.TextBox internal6;
            MT.Singularity.Presentation.Controls.StackPanel internal7;
            MT.Singularity.Presentation.Controls.Button internal8;
            MT.Singularity.Presentation.Controls.GroupPanel internal9;
            MT.Singularity.Presentation.Controls.Image internal10;
            MT.Singularity.Presentation.Controls.TextBlock internal11;
            MT.Singularity.Presentation.Controls.GroupPanel internal12;
            MT.Singularity.Presentation.Controls.Image internal13;
            MT.Singularity.Presentation.Controls.TextBlock internal14;
            internal3 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal3.Text = IND930YMLUI.Localization.Get(IND930YMLUI.Localization.Key.ContainerName);
            internal3.AddTranslationAction(() => {
                internal3.Text = IND930YMLUI.Localization.Get(IND930YMLUI.Localization.Key.ContainerName);
            });
            internal3.FontSize = ((System.Nullable<System.Int32>)16);
            internal4 = new MT.Singularity.Presentation.Controls.TextBox();
            internal4.Padding = new MT.Singularity.Presentation.Thickness(5);
            this.bindings[0] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal4.Text,() =>  _addContainerViewModel.ContainerName,MT.Singularity.Expressions.BindingMode.TwoWay,false);
            internal4.FontSize = ((System.Nullable<System.Int32>)24);
            internal4.Width = 500;
            internal4.Margin = new MT.Singularity.Presentation.Thickness(3);
            internal5 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal5.Margin = new MT.Singularity.Presentation.Thickness(0, 10, 0, 0);
            internal5.Text = IND930YMLUI.Localization.Get(IND930YMLUI.Localization.Key.Weight);
            internal5.AddTranslationAction(() => {
                internal5.Text = IND930YMLUI.Localization.Get(IND930YMLUI.Localization.Key.Weight);
            });
            internal5.FontSize = ((System.Nullable<System.Int32>)16);
            internal6 = new MT.Singularity.Presentation.Controls.TextBox();
            internal6.Padding = new MT.Singularity.Presentation.Thickness(5);
            this.bindings[1] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal6.Text,() =>  _addContainerViewModel.ContainerWeight,MT.Singularity.Expressions.BindingMode.TwoWay,false);
            internal6.KeyboardLayout = MT.Singularity.Presentation.Input.KeyboardLayout.Numeric;
            internal6.FontSize = ((System.Nullable<System.Int32>)24);
            internal6.Width = 500;
            internal6.Margin = new MT.Singularity.Presentation.Thickness(3);
            internal2 = new MT.Singularity.Presentation.Controls.StackPanel(internal3, internal4, internal5, internal6);
            internal2.Orientation = MT.Singularity.Presentation.Orientation.Vertical;
            internal8 = new MT.Singularity.Presentation.Controls.Button();
            internal8.Width = 190;
            internal8.Height = 70;
            internal8.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal8.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal10 = new MT.Singularity.Presentation.Controls.Image();
            internal10.Source = "embedded://IND930YMLUI/IND930YMLUI.images.ok.al8";
            internal10.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal10.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal11 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal11.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal11.Text = IND930YMLUI.Localization.Get(IND930YMLUI.Localization.Key.Ok);
            internal11.AddTranslationAction(() => {
                internal11.Text = IND930YMLUI.Localization.Get(IND930YMLUI.Localization.Key.Ok);
            });
            internal11.FontSize = ((System.Nullable<System.Int32>)20);
            internal11.Margin = new MT.Singularity.Presentation.Thickness(1);
            internal9 = new MT.Singularity.Presentation.Controls.GroupPanel(internal10, internal11);
            internal8.Content = internal9;
            this.bindings[2] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal8.Command,() => _addContainerViewModel.OkCommand,MT.Singularity.Expressions.BindingMode.OneWay,false);
            _cancelButton = new MT.Singularity.Presentation.Controls.Button();
            _cancelButton.Margin = new MT.Singularity.Presentation.Thickness(0, 20, 0, 0);
            _cancelButton.Width = 190;
            _cancelButton.Height = 70;
            _cancelButton.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            _cancelButton.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal13 = new MT.Singularity.Presentation.Controls.Image();
            internal13.Source = "embedded://IND930YMLUI/IND930YMLUI.images.Cancel.al8";
            internal13.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal13.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal14 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal14.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal14.Text = IND930YMLUI.Localization.Get(IND930YMLUI.Localization.Key.Abbrechen);
            internal14.AddTranslationAction(() => {
                internal14.Text = IND930YMLUI.Localization.Get(IND930YMLUI.Localization.Key.Abbrechen);
            });
            internal14.FontSize = ((System.Nullable<System.Int32>)20);
            internal14.Margin = new MT.Singularity.Presentation.Thickness(1);
            internal12 = new MT.Singularity.Presentation.Controls.GroupPanel(internal13, internal14);
            _cancelButton.Content = internal12;
            internal7 = new MT.Singularity.Presentation.Controls.StackPanel(internal8, _cancelButton);
            internal7.Orientation = MT.Singularity.Presentation.Orientation.Vertical;
            internal7.Margin = new MT.Singularity.Presentation.Thickness(20, 20, 0, 10);
            internal1 = new MT.Singularity.Presentation.Controls.StackPanel(internal2, internal7);
            internal1.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal1.Margin = new MT.Singularity.Presentation.Thickness(30);
            this.Content = internal1;
        }
        private readonly MT.Singularity.Expressions.IDataBinding[] bindings = new MT.Singularity.Expressions.IDataBinding[3];
    }
}
