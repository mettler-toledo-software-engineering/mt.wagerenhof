﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using IND930UI.Library.Models;
using IND930YMLUI.ViewModels;
using MT.Singularity;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;

namespace IND930YMLUI.View
{
    /// <summary>
    /// Interaction logic for SelectWagerenhofArticleView
    /// </summary>
    public partial class SelectWagerenhofArticleView
    {
        private SelectWagerenhofArticleViewModel _selectWagerenhofArticleViewModel;
        public SelectWagerenhofArticleView(RegularLaundryModel regularLaundry)
        {
            _selectWagerenhofArticleViewModel = new SelectWagerenhofArticleViewModel(regularLaundry);
            _selectWagerenhofArticleViewModel.Parent = this;
            
            InitializeComponents();
            _backButton.Background = Colors.LightGrayBrush;
        }
    }
}
