﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevicesLibrary
{
    public class DataReceivedEventArgs
    {
        public string Data { get; set; }

        public DataReceivedEventArgs(string data)
        {
            Data = data;
        }
    }
}
