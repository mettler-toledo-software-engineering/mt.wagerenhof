﻿using System;
using System.Diagnostics;
using System.IO.Ports;

namespace DevicesLibrary.BarcodeReader
{
    public interface IBarcodeReader : IDisposable
    {

        event EventHandler<DataReceivedEventArgs> DataReceivedEvent;
        bool Initialized { get; }

    }
}