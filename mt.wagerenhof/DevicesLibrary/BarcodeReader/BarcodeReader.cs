﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MT.Singularity.IO;
using MT.Singularity.Logging;
using SerialPort = System.IO.Ports.SerialPort;

namespace DevicesLibrary.BarcodeReader
{
    public abstract class BarcodeReader : IBarcodeReader
    {

        protected SerialPort _usedSerialPort;
        private string _dataline;
        private string _endOfDataCharakter;
        public event EventHandler<DataReceivedEventArgs> DataReceivedEvent;
        private const string SourceClass = nameof(BarcodeReader);

        public bool Initialized { get; private set; }

        protected BarcodeReader(SerialPort connection, EndOfDataCharacter character)
        {

            _endOfDataCharakter = character.TranslateCharakterToString();
            //Initialized = InitBarcodeReader(connection);
        }

        protected BarcodeReader(IConnectionChannel<DataSegment> connectionChannel)
        {

        }

        public bool InitBarcodeReader(SerialPort connection)
        {
            _usedSerialPort = connection;
            _usedSerialPort.DataReceived -= UsedSerialPortOnDataReceived;

            if (_usedSerialPort != null)
            {
                if (_usedSerialPort.IsOpen)
                {
                    _usedSerialPort.DataReceived -= UsedSerialPortOnDataReceived;
                    _usedSerialPort.Close();
                }
                _usedSerialPort.Open();
                _usedSerialPort.DataReceived += UsedSerialPortOnDataReceived;

                return true;
            }

            Log4NetManager.ApplicationLogger.ErrorEx($"Barcode Reader coud not be initialized ", SourceClass);
            return false;

        }

        private void UsedSerialPortOnDataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            var data = _usedSerialPort.ReadExisting();
            _dataline += data;
            if (string.IsNullOrEmpty(_dataline) == false && char.IsLetterOrDigit(_dataline, 0) && _dataline.EndsWith(_endOfDataCharakter))
            {
                _dataline = _dataline.Replace(_endOfDataCharakter, "");
                DataReceivedEvent?.Invoke(null, new DataReceivedEventArgs(_dataline));
                _dataline = "";
            }

        }

        public void Dispose()
        {
            _usedSerialPort.DataReceived -= UsedSerialPortOnDataReceived;
            _usedSerialPort.Close();
            Initialized = false;
            _dataline = "";
        }

    }
}
