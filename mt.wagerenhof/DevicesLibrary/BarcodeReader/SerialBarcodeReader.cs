﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MT.Singularity.Composition;
using MT.Singularity.IO;
using MT.Singularity.Logging;
using MT.Singularity.Serialization;
using SerialPort = System.IO.Ports.SerialPort;

namespace DevicesLibrary.BarcodeReader
{
    [Export(typeof(IBarcodeReader))]
    [InjectionBehavior(IsSingleton = true)]
    public class SerialBarcodeReader : IBarcodeReader
    {
        public event EventHandler<DataReceivedEventArgs> DataReceivedEvent;

        private IConnectionChannel<DataSegment> _connectionChannel;
        private StringSerializer _stringSerializer;

        public bool Initialized { get; private set; }
        private SerialPort _usedSerialPort;
        private string _dataline;
        private string _endOfDataCharacter;

        public SerialBarcodeReader(IConnectionChannel<DataSegment> connectionChannel) 
        {
            _connectionChannel = connectionChannel;
            InitSerialBarcodeReader();
        }
        public async void InitSerialBarcodeReader()
        {
            try
            {
                Log4NetManager.ApplicationLogger.Info($"init barcode reader");
                var delimiterSerializer = new DelimiterSerializer(_connectionChannel, CommonDataSegments.Cr);
                _stringSerializer = new StringSerializer(delimiterSerializer);

                if (_stringSerializer != null)
                {
                    await _stringSerializer.OpenAsync();
                    Log4NetManager.ApplicationLogger.Info($"serial channel opened");
                    var eventEmitter = new ChannelEventEmitter<string>(_stringSerializer);
                    Initialized = true;
                    eventEmitter.MessageRead += EventEmitterOnMessageRead;
                }
                Initialized = false;
            }
            catch (Exception e)
            {
                Initialized = false;
                Log4NetManager.ApplicationLogger.Error( $"init failed ", e);
                
            }
        }

        private void EventEmitterOnMessageRead(object sender, MessageReadEventArgs<string> e)
        {
            Log4NetManager.ApplicationLogger.Info($"event triggered with data {e.Message}");
            DataReceivedEvent?.Invoke(null, new DataReceivedEventArgs(e.Message));
        }


        public SerialBarcodeReader(SerialPort serialPort, EndOfDataCharacter endOfDataCharacter)
        {
            Initialized = InitBarcodeReader(serialPort);
            _endOfDataCharacter = endOfDataCharacter.TranslateCharakterToString();
        }

        private bool InitBarcodeReader(SerialPort connection)
        {
            _usedSerialPort = connection;
            _usedSerialPort.DataReceived -= UsedSerialPortOnDataReceived;

            if (_usedSerialPort != null)
            {
                if (_usedSerialPort.IsOpen)
                {
                    _usedSerialPort.DataReceived -= UsedSerialPortOnDataReceived;
                    _usedSerialPort.Close();
                }
                _usedSerialPort.Open();
                _usedSerialPort.DataReceived += UsedSerialPortOnDataReceived;

                return true;
            }

            Log4NetManager.ApplicationLogger.Error($"Barcode Reader coud not be initialized ");
            return false;

        }

        private void UsedSerialPortOnDataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            var data = _usedSerialPort.ReadExisting();
            _dataline += data;
            if (string.IsNullOrEmpty(_dataline) == false && char.IsLetterOrDigit(_dataline, 0) && _dataline.EndsWith(_endOfDataCharacter))
            {
                _dataline = _dataline.Replace(_endOfDataCharacter, "");
                DataReceivedEvent?.Invoke(null, new DataReceivedEventArgs(_dataline));
                _dataline = "";
            }

        }

        public void Dispose()
        {
            Initialized = false;
            _stringSerializer.Close();
            _stringSerializer.Dispose();
        }
    }
}
