﻿using System.IO.Ports;

namespace DevicesLibrary.Connections
{
    public interface IDeviceConnection
    {
        SerialPort GetDeviceConnection();
    }
}