﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevicesLibrary.USB
{
    public interface IUsbHandler
    {
        bool UsbDriveAvailable { get; }

        event EventHandler DeviceInsertedEvent;
        event EventHandler DeviceRemovedEvent;
    }
}
