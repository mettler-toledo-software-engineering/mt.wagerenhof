﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;
using MT.Singularity.Logging;

namespace DevicesLibrary.USB
{
    public class UsbDevice
    {
        private string _name;
        private const string SourceClass = nameof(UsbDevice);

        public UsbDevice(string name)
        {
            _name = name;
        }

        public string GetPortName()
        {
            if (_name.ToLowerInvariant().StartsWith("com"))
            {
                return _name;
            }
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_PnPEntity");
            foreach (var o in searcher.Get())
            {
                var queryObj = (ManagementObject)o;
                string devicename = (string)queryObj["Name"];


                if (string.IsNullOrEmpty(devicename) == false && devicename.Contains(_name))
                {
                    //bsp: "Zebra CDC Scanner (COM5)"
                    int portstart = devicename.IndexOf("(", StringComparison.Ordinal) + 1;
                    int portWordLength = devicename.Length - portstart - 1;

                    var portname = devicename.Substring(portstart, portWordLength);

                    return portname;
                }
            }
            Log4NetManager.ApplicationLogger.ErrorEx($"No Device with name {_name} found ", SourceClass);
            return "";
        }
    }
}
