﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;
using MT.Singularity.Composition;

namespace DevicesLibrary.USB
{
    [Export(typeof(IUsbHandler))]
    [InjectionBehavior(IsSingleton = true)]
    public class UsbHandler : IUsbHandler
    {
        public bool UsbDriveAvailable { get; private set; }

        public event EventHandler DeviceInsertedEvent;
        public event EventHandler DeviceRemovedEvent;

        private string _usbDrivePath;

        public UsbHandler(string usbdrivePath)
        {
            _usbDrivePath = usbdrivePath;
            UsbDriveAvailable = CheckUSBDirectory();
            RegisterForUsbEvents();
        }

        private bool CheckUSBDirectory()
        {
            return (Directory.Exists(_usbDrivePath));
        }

        private void RegisterForUsbEvents()
        {
            WqlEventQuery insertQuery = new WqlEventQuery("SELECT * FROM __InstanceCreationEvent WITHIN 2 WHERE TargetInstance ISA 'Win32_PnPEntity'");

            ManagementEventWatcher insertWatcher = new ManagementEventWatcher(insertQuery);
            insertWatcher.EventArrived += DeviceInserted;

            insertWatcher.Start();

            //Win32_USBHub als target reagiert nur auf usb drives
            //Win32_PnPEntity als target reagiert auf alle geräte
            WqlEventQuery removeQuery = new WqlEventQuery("SELECT * FROM __InstanceDeletionEvent WITHIN 2 WHERE TargetInstance ISA 'Win32_PnPEntity'");
            ManagementEventWatcher removeWatcher = new ManagementEventWatcher(removeQuery);
            removeWatcher.EventArrived += DeviceRemoved;
            removeWatcher.Start();
        }

        private void DeviceRemoved(object sender, EventArrivedEventArgs e)
        {
            UsbDriveAvailable = CheckUSBDirectory();
            DeviceRemovedEvent?.Invoke(this, EventArgs.Empty);
        }

        private void DeviceInserted(object sender, EventArrivedEventArgs e)
        {
            UsbDriveAvailable = CheckUSBDirectory();
            DeviceInsertedEvent?.Invoke(this, EventArgs.Empty);
        }
    }
}
