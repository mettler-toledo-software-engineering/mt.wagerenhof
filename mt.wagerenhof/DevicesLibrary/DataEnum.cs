﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevicesLibrary
{
    public enum EndOfDataCharacter
    {
        CR,
        LF,
        CRLF,
        ZPLCRLF
    }
}
