﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MT.Singularity.Platform.Devices.Scale;

namespace IND930UI.Library.EventManager
{
    public class WeighingEventArgs: EventArgs
    {
        public WeighingEventArgs(WeightInformation measuredWeight)
        {
            MeasuredWeight = measuredWeight;
        }
        public WeightInformation MeasuredWeight { get; private set; }
    }
}
