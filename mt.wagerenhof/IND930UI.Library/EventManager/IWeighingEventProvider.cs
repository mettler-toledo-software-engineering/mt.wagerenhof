﻿using System;

namespace IND930UI.Library.EventManager
{
    public interface IWeighingEventProvider
    {
        event EventHandler<WeighingEventArgs> WeighingEvent;

        void TriggerWeighingEvent(object sender, WeighingEventArgs eventArgs);
    }
}