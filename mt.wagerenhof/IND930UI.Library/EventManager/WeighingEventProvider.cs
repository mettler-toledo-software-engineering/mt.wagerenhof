﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MT.Singularity.Composition;

namespace IND930UI.Library.EventManager
{
    [Export(typeof(IWeighingEventProvider))]
    [InjectionBehavior(IsSingleton = true)]
    public class WeighingEventProvider : IWeighingEventProvider
    {
        public event EventHandler<WeighingEventArgs> WeighingEvent;
        public void TriggerWeighingEvent(object sender, WeighingEventArgs eventArgs)
        {
            WeighingEvent?.Invoke(sender, eventArgs);
        }
    }

}
