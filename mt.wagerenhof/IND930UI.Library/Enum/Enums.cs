﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IND930UI.Library.Enum
{
    public enum RegularLaundryProcessStep
    {
        Idle,
        SelectDebitor,
        SelectContainer,
        SelectArticle,
        DoWeighing,
        FinishedWeighing
    }


    public enum AddContainerResult
    {
        Ok,
        NOk,
        Duplicate,
        Undefined
    }

    public enum WorkingEnvironment
    {
        CleanLaundry,
        DirtyLaundry
    }

}
