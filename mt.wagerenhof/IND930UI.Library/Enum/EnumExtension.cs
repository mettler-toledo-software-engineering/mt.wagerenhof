﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IND930UI.Library.Enum
{
    public static class EnumExtension
    {

        public static string ToFriendlyName(this WorkingEnvironment workingEnvironment)
        {
            switch (workingEnvironment)
            {
                case WorkingEnvironment.CleanLaundry: return "Saubere Wäsche";
                case WorkingEnvironment.DirtyLaundry: return "Schmutzige Wäsche";
                default: return "";
            }
        }
    }
}
