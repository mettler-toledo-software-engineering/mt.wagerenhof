﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IND930UI.Library.Models;

namespace IND930UI.Library.Extensions
{
    public static class CompanyLaundryExtension
    {

        public static CompanyLaundryModel CalculateTotalNetWeight(this CompanyLaundryModel companyLaundry)
        {
            double lastNetto = companyLaundry.LastNetWeight;
            double totalNetto = companyLaundry.TotalDistributedNetWeight;
            double currentNetto = companyLaundry.CurrentNetWeight;

            companyLaundry.LastNetWeight = currentNetto;


            if (lastNetto > currentNetto)
            {
                companyLaundry.TotalDistributedNetWeight = totalNetto + lastNetto - currentNetto;
            }


            return companyLaundry;

        }
    }
}
