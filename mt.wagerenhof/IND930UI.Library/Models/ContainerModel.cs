﻿using System;

using System.Collections.Generic;
using System.Globalization;
using System.Text;
using CsvHelper.Configuration.Attributes;

//using CsvHelper.Configuration.Attributes;

namespace IND930UI.Library.Models
{
    public class ContainerModel : DomainObject
    {

        [Index(1)]
        public string Name { get; set; }
        [Index(2)]
        public double TareWeight { get; set; }
        [Ignore]
        public string TareWeightAsString => $"{TareWeight:F1}";
        [Ignore]
        public string NameInGrid
        {
            get
            {
                return $"{Name} ({TareWeightAsString} {Unit})";
            }
        }
        [Index(3)]
        public string Unit { get; set; }

        public override string ToString()
        {
            return $"{Name} ({TareWeightAsString})";
        }
    }
}
