﻿using System;

using System.Collections.Generic;
using System.Text;
using CsvHelper.Configuration.Attributes;

//using CsvHelper.Configuration.Attributes;

namespace IND930UI.Library.Models
{
    public class DebitorModel : DomainObject
    {
        [Index(0)]
        public string DebitorNumber { get; set; }
        [Index(1)]
        public string DebitorName { get; set; }

        [Ignore]
        public string NameInGrid
        {
            get
            {
                return $"{DebitorNumber} : {DebitorName}";
            }
        }

        public override string ToString()
        {
            return $"{DebitorNumber} : {DebitorName}";
        }
    }
}
