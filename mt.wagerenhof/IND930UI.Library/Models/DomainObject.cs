﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsvHelper.Configuration.Attributes;
using DataAccess;

namespace IND930UI.Library.Models
{
    public class DomainObject : IDomainObject
    {
        [Index(0)]
        public int Id { get; set; }
    }
}
