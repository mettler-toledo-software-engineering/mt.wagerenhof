﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsvHelper.Configuration.Attributes;

namespace IND930UI.Library.Models
{
    public class MeasurementModel : DomainObject
    {
        [Index(0)]
        //[Name("Wiege-Nr")]
        public new string Id { get; set; }
        [Index(1)]
        //[Name("Datum")]
        public string Date { get; set; }

        [Index(2)]
        //[Name("Zeit")]
        public string Time => "";

        [Index(3)]
        //[Name("unbenutzt")]
        public string Placeholder1 => "";
        [Index(4)]
        //[Name("KundenNr")]
        public string DebitorNumber { get; set; }

        [Index(5)]
        //[Name("unbenutzt")]
        public string Placeholder2 => "";
        [Index(6)]
        //[Name("ArtikelNr")]
        public string ArticleNumber { get; set; }
        [Index(7)]
        //[Name("Kilo")]
        public string NetWeight { get; set; }

        [Index(8)]
        //[Name("Brutto")]
        public string GrossWeight => "";

        [Index(9)]
        //[Name("unbenutzt")]
        public string AnotherTime => "";


    }
}
