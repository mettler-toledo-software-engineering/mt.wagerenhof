﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IND930UI.Library.Models
{
    public class FavoriteArticle : ArticleModel
    {
        public string Picture { get; set; }
    }
}
