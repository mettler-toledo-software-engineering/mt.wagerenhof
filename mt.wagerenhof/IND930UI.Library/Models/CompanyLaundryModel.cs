﻿using System;

using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace IND930UI.Library.Models
{
    public class CompanyLaundryModel
    {
        public CompanyLaundryModel(ContainerModel selectedContainer, DebitorModel defaultDebitor)
        {
            SelectedContainer = selectedContainer;
            DefaultDebitor = defaultDebitor;
        }

        public int MeasurementsPerDay { get; set; }
        public string NameInGrid => SelectedContainer?.Name;
        public DebitorModel DefaultDebitor { get; }
        public ContainerModel SelectedContainer { get;}
        public ArticleModel SelectedArticle { get; set; }
        public string SelectedArticleName => SelectedArticle?.ArticleName;
        public double LastNetWeight { get; set; } = 0;
        public string LastNetWeightAsString => $"{LastNetWeight:F2}";
        public double CurrentNetWeight { get; set; } = 0;
        public string CurrentNetWeightAsString => $"{CurrentNetWeight:F2}";
        public double TotalDistributedNetWeight { get; set; } = 0;

        public string TotalDistributedNetWeightAsString => $"{TotalDistributedNetWeight:F2}";

        public override string ToString()
        {
            return $"{SelectedContainer.Name} ({SelectedContainer.TareWeightAsString}) ";
        }
    }
}
