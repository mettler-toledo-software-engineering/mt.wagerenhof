﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IND930UI.Library.Enum;
using IND930UI.Library.Models;
using MT.Singularity.Composition;

namespace IND930UI.Library.Models
{
    
    public class CompanyLaundryProcessModel
    {
        public List<CompanyLaundryModel> CompanyLaundryList { get; set; } = new List<CompanyLaundryModel>();
        public CompanyLaundryModel SelectedCompanyLaundry { get; set; }

        public AddContainerResult AddContainerToProcess(ContainerModel container, DebitorModel defaultDebitor)
        {
            AddContainerResult result;

            bool modelIsAllreadyInList = CompanyLaundryList.Any(model => model.SelectedContainer.Name == container.Name);
            if (modelIsAllreadyInList)
            {
                result = AddContainerResult.Duplicate;
  
            }
            else
            {
                var companyLaundry = new CompanyLaundryModel(container, defaultDebitor);
                CompanyLaundryList.Add(companyLaundry);
                SelectedCompanyLaundry = companyLaundry;
                result = AddContainerResult.Ok;
            }
            return result;

        }

        public void ResetCompanyLaundryProcess()
        {
            CompanyLaundryList.Clear();
            SelectedCompanyLaundry = null;
        }
    }
}
