﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IND930UI.Library.Enum;

namespace IND930UI.Library.Models
{

    public class RegularLaundryModel
    {
        public ArticleModel SelectedArticle { get; set; }
        public DebitorModel SelectedDebitor { get; set; }
        public ContainerModel SelectedContainer { get; set; }

        private double _measuredNetWeight = 0;

        public double MeasuredNetWeight
        {
            get { return _measuredNetWeight; }
            set
            {
                _measuredNetWeight = Math.Round(value,2);

            }
        }

        public string Unit { get; set; }


        public RegularLaundryProcessStep CurrentProcessStep { get; set; }

    }
}
