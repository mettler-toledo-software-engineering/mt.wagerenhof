﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IND930UI.Library.Models
{
    public static class FavoriteData
    {
        private static string image1 = @"c:\packages\IND930YMLUI\Data\Images\42004.png";
        private static string image2 = @"c:\packages\IND930YMLUI\Data\Images\42006.png";
        private static string image3 = @"c:\packages\IND930YMLUI\Data\Images\42008.png";
        private static string image4 = @"c:\packages\IND930YMLUI\Data\Images\42010.png";
        private static string image5 = @"c:\packages\IND930YMLUI\Data\Images\42011.png";
        private static string image6 = @"c:\packages\IND930YMLUI\Data\Images\42012.jpg";
        private static string image7 = @"c:\packages\IND930YMLUI\Data\Images\42014.png";
        private static string image8 = @"c:\packages\IND930YMLUI\Data\Images\42030.png";
        private static string image9 = @"c:\packages\IND930YMLUI\Data\Images\40811.png";
        private static string image10 = @"c:\packages\IND930YMLUI\Data\Images\42020.png";

        public static List<FavoriteArticle> FavoriteArticles = new List<FavoriteArticle>
        {
            new FavoriteArticle
            {
                ArticleName = "Decken  Mischmasch", Quantity = 0, ArticleNumber = "42004", ExpectedUnit = 1, Picture = image1
            },
            new FavoriteArticle
            {
                ArticleName = "Frotte + Badeteppich", Quantity = 0, ArticleNumber = "42006", ExpectedUnit = 1, Picture = image2
            },
            new FavoriteArticle
            {
                ArticleName = "Bewohnerkleider", Quantity = 0, ArticleNumber = "42008", ExpectedUnit = 1, Picture = image3
            },
            new FavoriteArticle
            {
                ArticleName = "Unterwäsche / Socken / Lappen", Quantity = 0, ArticleNumber = "42010", ExpectedUnit = 1, Picture = image4
            },
            new FavoriteArticle
            {
                ArticleName = "Microfaser / Jon Master", Quantity = 0, ArticleNumber = "42011", ExpectedUnit = 1, Picture = image5
            },
            new FavoriteArticle
            {
                ArticleName = "Duvet / Pfulmen", Quantity = 0, ArticleNumber = "42012", ExpectedUnit = 1, Picture = image6
            },
            new FavoriteArticle
            {
                ArticleName = "Hemden / Blusen / Hosen", Quantity = 0, ArticleNumber = "42014", ExpectedUnit = 1, Picture = image7
            },
            new FavoriteArticle
            {
                ArticleName = "Flickwäsche", Quantity = 0, ArticleNumber = "42030", ExpectedUnit = 1, Picture = image8
            },
            new FavoriteArticle
            {
                ArticleName = "Berufskleider", Quantity = 0, ArticleNumber = "40811", ExpectedUnit = 1, Picture = image9
            },
            new FavoriteArticle
            {
                ArticleName = " Duschvorhänge / Vorhänge", Quantity = 0, ArticleNumber = "42020", ExpectedUnit = 1, Picture = image10
            }
        };
    }
}
