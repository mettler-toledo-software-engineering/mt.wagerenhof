﻿using System;

using System.Collections.Generic;
using System.Text;
using CsvHelper.Configuration.Attributes;

//using CsvHelper.Configuration.Attributes;

namespace IND930UI.Library.Models
{
    public class ArticleModel : DomainObject
    {
        [Index(0)]
        public string ArticleNumber { get; set; }
        [Index(1)]
        public string ArticleName { get; set; }
        [Index(2)]
        //1 = kg, 2 = pieces
        public int ExpectedUnit { get; set; }

        [Ignore]
        public int Quantity { get; set; } = 0;

        [Ignore]
        public string NameInGrid
        {
            get
            {
                return $"{ArticleNumber} : {ArticleName}";
            }
        }

        public override string ToString()
        {
            return $"{ArticleNumber} - {ArticleName}";
        }
    }
}
