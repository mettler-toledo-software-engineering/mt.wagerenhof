﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace DataAccess
{
    public interface IGenericCsvAccess<T> : IDataAccess<T> where T : IDomainObject
    {
        string FileDelimiter { get; set; }
        FileMode FileMode { get; set; }
        string FileName { get; set; }
        string FilePath { get; set; }
        bool HasHeaderRecord { get; set; }


    }
}