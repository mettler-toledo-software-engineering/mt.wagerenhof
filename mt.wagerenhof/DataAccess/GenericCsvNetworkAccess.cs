﻿using System;
using DataAccess.Network;
using System.Collections.Generic;
using System.IO;

namespace DataAccess
{
    public class GenericCsvNetworkAccess<T> : GenericCsvAccess<T> where T : IDomainObject
    {
        private readonly INetworkAccess _networkAccess;
        private readonly string _backupDestination;
        public GenericCsvNetworkAccess(string filePath, string fileName, string fileDelimiter, INetworkUser user, string encoding, string alternatePath) : base(filePath, fileName, fileDelimiter,  encoding)
        {
            _networkAccess = new NetworkAccess(user);
            _backupDestination = Path.Combine(alternatePath, fileName);
        }

        public override bool SaveDataEntry(T dataEntry)
        {
            _networkAccess.EnterImpersonationContext();
            var success = base.SaveDataEntry(dataEntry);


            if (success)
            {
                CopyFileToBackupDestination();
            }
            else
            {
                success = SaveEntry(_backupDestination, dataEntry);
            }
            _networkAccess.LeaveImpersonationContext();
            return success;
        }


        public override bool SaveDataEntries(List<T> dataEntries)
        {
            _networkAccess.EnterImpersonationContext();
            var success = base.SaveDataEntries(dataEntries);

            if (success)
            {
                CopyFileToBackupDestination();
            }
            else
            {
                success = SaveEntries(_backupDestination, dataEntries);
            }
            _networkAccess.LeaveImpersonationContext();
            return success;
        }

        public override List<T> GetAllDataEntries()
        {
            _networkAccess.EnterImpersonationContext();
            var result = base.GetAllDataEntries();
            if (result != null)
            {
                CopyFileToBackupDestination();
            }
            else
            {
                result = GetAllEntries(_backupDestination);
            }
            _networkAccess.LeaveImpersonationContext();
            return result;
        }

        private void CopyFileToBackupDestination()
        {
            try
            {
                File.Copy(MainDestination, _backupDestination, true);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            
        }
    }
}
