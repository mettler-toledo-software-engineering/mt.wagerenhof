﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    interface IGenericDataBaseAccess<T>
    {
        Task<int> SaveDataEntryAsync(T dataEntry);
        Task<bool> UpdateDataEntryAsnyc(T dataEntry);
        Task<bool> DeleteDatEntryaAsync(T dataEntry);

        Task<List<T>> GetAllDataEntriesAsync();

        Task<T> GetDataEntryByIdAsync(int id);

        Task<List<T>> GetEntriesByParameterAsync(string parameterName, object parameterValue);

        bool UpdateDataEntry(T dataEntry);
        bool DeleteDataEntry(T dataEntry);

        T GetDataEntryById(int id);
        List<T> GetDataEntriesByParameter(string parameterName, object parameterValue);

    }
}
