﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using CsvHelper;
using System.Globalization;
using CsvHelper.Configuration;

namespace DataAccess
{

    public class GenericCsvAccess<T> : IGenericCsvAccess<T> where T : IDomainObject
    {

        public string FilePath { get; set; }
        public string FileName { get; set; }
        public string FileDelimiter { get; set; }
        public FileMode FileMode { get; set; }
        public bool HasHeaderRecord { get; set; }


        private string _csvEncoding { get; set; }
        private readonly CsvConfiguration _config;

        protected readonly string MainDestination;


        public GenericCsvAccess(string filePath, string fileName, string fileDelimiter,  string encoding)
        {
            FilePath = filePath;
            FileName = fileName;
            FileDelimiter = fileDelimiter;

            _csvEncoding = encoding;

            MainDestination = Path.Combine(FilePath, fileName);
            

            _config = new CsvConfiguration(CultureInfo.CurrentUICulture)
            {
                PrepareHeaderForMatch = args => args.Header.ToLower(),

                Delimiter = FileDelimiter,
                HasHeaderRecord = HasHeaderRecord

            };

        }

        protected List<T> GetAllEntries(string destination)
        {
            try
            {
                using (var reader = new StreamReader(destination, Encoding.GetEncoding(_csvEncoding)))
                using (var csv = new CsvReader(reader, _config))
                {
                    var records = csv.GetRecords<T>().ToList();
                    return records;
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                return null;
            }
        }

        public virtual List<T> GetAllDataEntries()
        {

            var result = GetAllEntries(MainDestination);
            return result;
        }

        protected bool SaveEntry(string destination, T dataEntry)
        {
            try
            {
                using (var stream = File.Open(destination, FileMode.Append))
                using (var writer = new StreamWriter(stream, Encoding.GetEncoding(_csvEncoding)))
                using (var csv = new CsvWriter(writer, _config))
                {
                    csv.NextRecord();
                    csv.WriteRecord<T>(dataEntry);
                    return true;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }

        public virtual bool SaveDataEntry(T dataEntry)
        {
            var result = SaveEntry(MainDestination, dataEntry);
            return result;

        }

        protected bool SaveEntries(string destination, List<T> dataEntries)
        {
            try
            {
                using (var stream = File.Open(destination, FileMode.Create))
                using (var writer = new StreamWriter(stream, Encoding.GetEncoding(_csvEncoding)))
                using (var csv = new CsvWriter(writer, _config))
                {
                    csv.WriteRecords(dataEntries);
                    return true;
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                return false;
            }
        }

        public virtual bool SaveDataEntries(List<T> dataEntries)
        {
            var result = SaveEntries(MainDestination, dataEntries);
            return result;
        }

        public virtual bool DeleteDataEntry(int id)
        {
            try
            {

                List<T> records;
                using (var reader = new StreamReader(MainDestination, Encoding.GetEncoding(_csvEncoding)))
                using (var csv = new CsvReader(reader, _config))
                {
                    records = csv.GetRecords<T>().ToList();
                    records.RemoveAll(entry => entry.Id == id);
                }

                using (var stream = File.Open(MainDestination, FileMode.Create))
                using (var writer = new StreamWriter(stream, Encoding.GetEncoding(_csvEncoding)))
                using (var csv = new CsvWriter(writer, _config))
                {

                    csv.WriteRecords(records);

                }

                return true;

            }
            catch (Exception mainException)
            {
                Console.WriteLine(mainException);
                return false;
            }

        }


        public int GenerateId()
        {
            List<T> records = new List<T>();

            try
            {
                using (var reader = new StreamReader(MainDestination, Encoding.GetEncoding(_csvEncoding)))
                using (var csv = new CsvReader(reader, _config))
                {
                    records = csv.GetRecords<T>().ToList();

                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                try
                {
                    using (var reader = new StreamReader(MainDestination, Encoding.GetEncoding(_csvEncoding)))
                    using (var csv = new CsvReader(reader, _config))
                    {
                        records = csv.GetRecords<T>().ToList();

                    }
                }
                catch (Exception exception)
                {
                    Console.WriteLine(exception);

                }


            }


            int? id = records.OrderByDescending(entry => entry.Id).FirstOrDefault()?.Id;

            if (id == null)
            {
                return 1;
            }
            else
            {
                return id.Value + 1;
            }
        }

    }
}
