﻿namespace DataAccess.Network
{
    public interface INetworkUser : IUser
    {
        
        string TargetComputerName { get; set; }
        
    }
}