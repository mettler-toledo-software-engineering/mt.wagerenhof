﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Network
{
    public class NetworkUser : INetworkUser
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string TargetComputerName { get; set; }


        public NetworkUser(string userName, string password, string targetComputerName)
        {
            UserName = userName;
            Password = password;
            TargetComputerName = targetComputerName;
        }
    }
}
