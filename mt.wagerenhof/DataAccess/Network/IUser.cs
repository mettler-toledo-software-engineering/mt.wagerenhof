﻿namespace DataAccess.Network
{
    public interface IUser
    {
        string Password { get; set; }
        string UserName { get; set; }
    }
}