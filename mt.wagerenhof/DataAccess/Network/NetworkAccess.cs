﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DataAccess.Network
{
    public class NetworkAccess : INetworkAccess
    {
        private readonly WrappedImpersonationContext _context;

        public NetworkAccess(INetworkUser user)
        {
            var networkUser = user;
            _context = new WrappedImpersonationContext(networkUser.TargetComputerName, networkUser.UserName, networkUser.Password);
        }

        

        public void EnterImpersonationContext()
        {
            _context.Enter();
        }

        public void LeaveImpersonationContext()
        {
            _context.Leave();
        }

    }
}
