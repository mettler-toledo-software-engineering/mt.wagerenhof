﻿namespace DataAccess.Network
{
    public interface INetworkAccess
    {
        void EnterImpersonationContext();
        void LeaveImpersonationContext();
    }
}